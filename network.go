package customlib

import (
	"bytes"
	"crypto/tls"
	"customlib/vars"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"regexp"
	"strings"
)

// SendHTTPRequest : generic for sending http request and retrieve the body
func SendHTTPRequest(method string, url string, body []byte,
	headers map[string]string, insecureSkipVerify bool) (int, []byte) {
	// check the url, must match
	if url == "" {
		return 10, []byte("no url given")
	}
	// re-init the method
	var methodToSend string
	switch method {
	case "POST", "post":
		methodToSend = "POST"
	case "PUT", "put":
		methodToSend = "PUT"
	case "DELETE", "delete", "del":
		methodToSend = "DELETE"
	default:
		methodToSend = "GET"
	}
	// create the request handler
	req, _ := http.NewRequest(methodToSend, url, bytes.NewBuffer(body))
	// adding headers
	for k, v := range headers {
		req.Header.Set(k, v)
	}
	// create client - default without options.
	// option may include for example
	// 	MaxIdleConns:       10,
	// 	IdleConnTimeout:    30 * time.Second,
	// 	DisableCompression: true,
	client := &http.Client{}
	if insecureSkipVerify {
		// create the client with the ignore cert option
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		client = &http.Client{Transport: tr}
	}

	// launch request
	resp, err := client.Do(req)
	if err != nil {
		return 20, []byte(err.Error())
	}
	// read the body to return it
	defer resp.Body.Close()
	responseStatusCode := resp.StatusCode
	// responseHeader := resp.Header
	responseBody, _ := ioutil.ReadAll(resp.Body)
	// return it
	return responseStatusCode, responseBody
}

// SendRESTRequest : generic for sending http request and retrieve the body
func SendRESTRequest(method string, url string, body []byte,
	headers map[string]string, cookie *http.Cookie, insecureSkipVerify bool) (int, map[string][]string, []*http.Cookie, []byte) {
	// check the url, must match
	if url == "" {
		return 10, nil, nil, []byte("no url given")
	}
	// re-init the method
	var methodToSend string
	switch method {
	case "POST", "post":
		methodToSend = "POST"
	case "PUT", "put":
		methodToSend = "PUT"
	case "DELETE", "delete", "del":
		methodToSend = "DELETE"
	default:
		methodToSend = "GET"
	}
	// create the request handler
	req, _ := http.NewRequest(methodToSend, url, bytes.NewBuffer(body))
	// adding headers
	for k, v := range headers {
		req.Header.Set(k, v)
	}
	// adding cookie if applicable
	if cookie != nil {
		req.AddCookie(cookie)
	}
	// create client - default without options.
	// option may include for example
	// 	MaxIdleConns:       10,
	// 	IdleConnTimeout:    30 * time.Second,
	// 	DisableCompression: true,
	client := &http.Client{}
	if insecureSkipVerify {
		// create the client with the ignore cert option
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		client = &http.Client{Transport: tr}
	}

	// launch request
	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
		log.Println(url)
		log.Println(req.Body)
		return 20, nil, nil, []byte(err.Error())
	}
	// read the body to return it
	defer resp.Body.Close()
	// responseStatusCode := resp.StatusCode
	// responseHeader := resp.Header
	responseBody, _ := ioutil.ReadAll(resp.Body)
	// return it
	return resp.StatusCode, resp.Header, resp.Cookies(), responseBody
}

// ExtractRouteIDFromURI : extracts the last field from uri, the "id" or name
func ExtractRouteIDFromURI(uri string) string {
	uriSplit := strings.Split(vars.RemoveTrailingSlash(uri), "/")
	return uriSplit[len(uriSplit)-1]
}

// GetLocalIPAddresses : get the local IP addresses
func GetLocalIPAddresses() ([]string, []string, error) {
	ifaces, err := net.Interfaces()
	if err != nil {
		return nil, nil, err
	}
	var ips4 []string
	var ips6 []string
	for _, iface := range ifaces {
		// avoid the vmnet brides/nat, loop, docker...
		if regexp.MustCompile(`^vmnet.*|^lo|^docker`).MatchString(iface.Name) {
			continue
		}
		// get the addresses
		addresses, err := iface.Addrs()
		if err != nil {
			continue
		}
		// for each address on this interface
		for _, address := range addresses {
			switch v := address.(type) {
			case *net.IPNet:
				if v.IP.To4() != nil {
					ips4 = append(ips4, v.IP.String())
				}
				if strings.Contains(v.IP.String(), ":") {
					ips6 = append(ips6, v.IP.String())
				}
			case *net.IPAddr:
				if v.IP.To4() != nil {
					ips4 = append(ips4, v.IP.String())
				}
				if strings.Contains(v.IP.String(), ":") {
					ips6 = append(ips6, v.IP.String())
				}
			}
		}
	}
	return ips4, ips6, nil
}

// GetLocalIPv4Addresses : get the local IP addresses
func GetLocalIPv4Addresses() ([]string, error) {
	ips4, _, err := GetLocalIPAddresses()
	return ips4, err
}

// GetLocalIPv6Addresses : get the local IP addresses
func GetLocalIPv6Addresses() ([]string, error) {
	_, ips6, err := GetLocalIPAddresses()
	return ips6, err
}
