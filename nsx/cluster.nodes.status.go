package nsx

import (
	"customlib"
	"encoding/json"
	"errors"
	"strconv"
)

// NodeStatusManager : the status and version of the manager's node status
type NodeStatusManager struct {
	Status struct {
		Status string `json:"mgmt_cluster_status,omitempty"` // could be CONNECTED, DISCONNECTED, UNKNOWN
	} `json:"mgmt_cluster_status,omitempty"`
	Version string `json:"version,omitempty"`
}

// NodeStatusController : the status and version of the controller's node status
type NodeStatusController struct {
	Status struct {
		Status     string `json:"control_cluster_status,omitempty"` // could be CONNECTED, DISCONNECTED, UNKNOWN
		MgmtStatus struct {
			ConnectivityStatus string `json:"connectivity_status,omitempty"` // could be CONNECTED, DISCONNECTED, UNKNOWN
		} `json:"mgmt_connection_status,omitempty"`
	} `json:"control_cluster_status,omitempty"`
	Version string `json:"version,omitempty"`
}

// FileSystem : the filesystem type found in properties
type FileSystem struct {
	FileSystem string `json:"file_system,omitempty"`
	Type       string `json:"type,omitempty"`
	Mount      string `json:"mount,omitempty"`
	Total      int64  `json:"total,omitempty"`
	Used       int64  `json:"used,omitempty"`
}

// NodeStatusProperties : the properties of the retrieved/archieved metrics of node
type NodeStatusProperties struct {
	CPUCores    int64        `json:"cpu_cores,omitempty"`
	SystemTime  int64        `json:"system_time,omitempty"`
	Uptime      int64        `json:"uptime,omitempty"`
	FileSystems []FileSystem `json:"file_systems,omitempty"`
	LoadAverage []float64    `json:"load_average,omitempty"`
	Source      string       `json:"source,omitempty"`
	MemTotal    int64        `json:"mem_total,omitempty"`
	MemUsed     int64        `json:"mem_used,omitempty"`
	MemCache    int64        `json:"mem_cache,omitempty"`
	SwapTotal   int64        `json:"swap_total,omitempty"`
	SwapUsed    int64        `json:"swap_used,omitempty"`
}

// NodeInterfaceProperties : properties for ethernet interfaces of nodes
type NodeInterfaceProperties struct {
	ID             string `json:"interface_id"`
	InterfaceAlias []struct {
		PhysicalAddress  string `json:"physical_address,omitempty"`
		Netmask          string `json:"netmask,omitempty"`
		IPAddress        string `json:"ip_address,omitempty"`
		IPConfiguration  string `json:"ip_configuration,omitempty"`
		BroadcastAddress string `json:"broadcast_address,omitempty"`
	} `json:"interface_alias,omitempty"`
	AdminStatus string `json:"admin_status,omitempty"`
	LinkStatus  string `json:"link_status,omitempty"`
	Source      string `json:"source,omitempty"`
	MTU         int    `json:"mtu,omitempty"`
}

// RuleConfig : the properties assigned to services/processes out of the nodes (api, communications, cluster comm...)
type RuleConfig struct {
	Type                  string `json:"type,omitempty"` // could be ControllerClusterRoleConfig or ManagementClusterRoleConfig
	MgmtClusterListenAddr struct {
		Port                        int    `json:"port"`
		IPAddress                   string `json:"ip_address"`
		CertificateSHA256Thumbprint string `json:"certificate_sha256_thumbprint"`
		Certificate                 string `json:"certificate"`
	} `json:"mgmt_cluster_listen_addr,omitempty"`
	MgmtPlaneListenAddr struct {
		Port                        int    `json:"port"`
		IPAddress                   string `json:"ip_address"`
		CertificateSHA256Thumbprint string `json:"certificate_sha256_thumbprint"`
		Certificate                 string `json:"certificate"`
	} `json:"mgmt_plane_listen_addr,omitempty"`
	APIListenAddr struct {
		Port                        int    `json:"port"`
		IPAddress                   string `json:"ip_address"`
		CertificateSHA256Thumbprint string `json:"certificate_sha256_thumbprint"`
		Certificate                 string `json:"certificate"`
	} `json:"api_listen_addr,omitempty"`
	ControlClusterListenAddr struct {
		Port      int    `json:"port"`
		IPAddress string `json:"ip_address"`
	} `json:"control_cluster_listen_addr,omitempty"`
	ControlPlaneListenAddr struct {
		Port                        int    `json:"port"`
		IPAddress                   string `json:"ip_address"`
		CertificateSHA256Thumbprint string `json:"certificate_sha256_thumbprint"`
		Certificate                 string `json:"certificate"`
	} `json:"control_plane_listen_addr,omitempty"`
	HostMsgClientInfo struct {
		AccountName string `json:"account_name"`
	} `json:"host_msg_client_info,omitempty"`

	MpaMsgClientInfo struct {
		AccountName string `json:"account_name"`
	} `json:"mpa_msg_client_info,omitempty"` // common for mp and ccp
}

// ClusterNodeStatusManager : the status of the manager nodes
type ClusterNodeStatusManager struct {
	NodeStatus              *NodeStatusManager        `json:"node_status,omitempty"`
	NodeStatusProperties    []NodeStatusProperties    `json:"node_status_properties,omitempty"`
	NodeInterfaceProperties []NodeInterfaceProperties `json:"node_interface_properties,omitempty"`
	RoleConfig              *RuleConfig               `json:"role_config,omitempty"`
	TransportNodesConnected int64                     `json:"transport_nodes_connected,omitempty"` // only for management cluster
}

// ClusterNodeStatusController : the status of the controller nodes
type ClusterNodeStatusController struct {
	NodeStatus              *NodeStatusController     `json:"node_status,omitempty"`
	NodeStatusProperties    []NodeStatusProperties    `json:"node_status_properties,omitempty"`
	NodeInterfaceProperties []NodeInterfaceProperties `json:"node_interface_properties,omitempty"`
	RoleConfig              *RuleConfig               `json:"role_config,omitempty"`
}

// ClusterNodesStatus : the status of all nodes
type ClusterNodesStatus struct {
	ManagementCluster []ClusterNodeStatusManager    `json:"management_cluster,omitempty"`
	ControllerCluster []ClusterNodeStatusController `json:"controller_cluster,omitempty"`
}

// ===== Starting funcs =====

// GetClusterNodesStatus : gets the mp+ccp nodes status as object
func (n *NSX) GetClusterNodesStatus() (ClusterNodesStatus, error) {
	var clusterNodesStatus ClusterNodesStatus
	// cursor
	if n.Address == "" || n.Base64Login == "" {
		return clusterNodesStatus, errors.New("Invalid parameters")
	}
	uri := "https://" +
		n.Address + ":" +
		strconv.Itoa(n.Port) +
		uriClusterManagementNodesStatus
	// in case of multiple pages
	headers := make(map[string]string)
	headers["Content-Type"] = customlib.APPJSONHeader
	headers["Authorization"] = "Basic " + n.Base64Login
	responseCode, responseBody := customlib.SendHTTPRequest(
		"GET",
		uri,
		nil,
		headers,
		n.InsecureSkipVerify)
	if responseCode < 200 || responseCode > 208 {
		return clusterNodesStatus, errors.New("failed the get request, responseCode=" +
			strconv.Itoa(responseCode) + "\nresponseBody=" + string(responseBody))
	}
	json.Unmarshal(responseBody, &clusterNodesStatus)
	return clusterNodesStatus, nil
}

// IsHealthy : return true if all nodes are connected and ccp<>mp is connected
func (s *ClusterNodesStatus) IsHealthy() bool {
	return s.IsControllerHealthy() && s.IsControllerToMgmtHealthy() && s.IsManagementHealthy()
}

// IsManagementHealthy : return true if all management nodes are connected
func (s *ClusterNodesStatus) IsManagementHealthy() bool {
	for _, mp := range s.ManagementCluster {
		if mp.NodeStatus.Status.Status != "CONNECTED" {
			return false
		}
	}
	return true
}

// IsControllerHealthy : return true if all controller nodes are connected
func (s *ClusterNodesStatus) IsControllerHealthy() bool {
	for _, ccp := range s.ControllerCluster {
		if ccp.NodeStatus.Status.Status != "CONNECTED" {
			return false
		}
	}
	return true
}

// IsControllerToMgmtHealthy : return true if all ccp<>mp is connected
func (s *ClusterNodesStatus) IsControllerToMgmtHealthy() bool {
	for _, ccp := range s.ControllerCluster {
		if ccp.NodeStatus.Status.MgmtStatus.ConnectivityStatus != "CONNECTED" {
			return false
		}
	}
	return true
}

// GetMgmtNodeIPs : get ip of the given # of management node
func (s *ClusterNodesStatus) GetMgmtNodeIPs(nodeIndex int) string {
	if nodeIndex < 0 || nodeIndex > 2 {
		// not supposed to have more than 3 nodes in MP cluster
		return ""
	}
	return s.ManagementCluster[nodeIndex].RoleConfig.APIListenAddr.IPAddress
}

// GetMgmtNodesIPs : get ip of all management nodes
func (s *ClusterNodesStatus) GetMgmtNodesIPs() []string {
	var ips []string
	for i := range s.ManagementCluster {
		ips = append(ips, s.ManagementCluster[i].RoleConfig.APIListenAddr.IPAddress)
	}
	return ips
}

// GetMgmtCertificateSHA256Thumbprint : Get the certificate thumbprint
func (s *ClusterNodesStatus) GetMgmtCertificateSHA256Thumbprint() string {
	if len(s.ManagementCluster) > 0 {
		return s.ManagementCluster[0].RoleConfig.APIListenAddr.CertificateSHA256Thumbprint
	}
	return ""
}

// GetThumbprint : shortcut for GetMgmtCertificateSHA256Thumbprint
func (s *ClusterNodesStatus) GetThumbprint() string {
	return s.GetMgmtCertificateSHA256Thumbprint()
}

// GetMgmtCertificate : return the management certificate in pem format
func (s *ClusterNodesStatus) GetMgmtCertificate() string {
	return s.ManagementCluster[0].RoleConfig.APIListenAddr.Certificate
}

// GetControllerNodeIPs : get ip of the given # of controller node
func (s *ClusterNodesStatus) GetControllerNodeIPs(nodeIndex int) string {
	if nodeIndex < 0 || nodeIndex > 2 {
		// not supposed to have more than 3 nodes in MP cluster
		return ""
	}
	return s.ControllerCluster[nodeIndex].RoleConfig.ControlPlaneListenAddr.IPAddress
}

// GetControllerNodesIPs : get ip of all controller nodes
func (s *ClusterNodesStatus) GetControllerNodesIPs() []string {
	var ips []string
	for i := range s.ControllerCluster {
		ips = append(ips, s.ControllerCluster[i].RoleConfig.ControlPlaneListenAddr.IPAddress)
	}
	return ips
}
