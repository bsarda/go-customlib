package nsx

import (
	"customlib"
	"customlib/hashicorpvault"
	"encoding/base64"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

// =============================================================================
// ======================== basic operations and strucs ========================

// NSX : NSX-T manager configuration
type NSX struct {
	Address            string
	Port               int
	InsecureSkipVerify bool
	Base64Login        string
	Login              string
	Password           string
	Cookie             http.Cookie
	XCSRFToken         string
	AuthType           string
}

// =============================================================================
// =================  NSX-T: create the object for future ops  =================

// CreateNSX : creates the nsxt object with id and secret
// valid inputs for authType : basic|session
func CreateNSX(address string, port int, insecureSkipVerify bool, login string, password string, authType string) (*NSX, error) {
	// check inputs
	if address == "" || login == "" || password == "" {
		return nil, errors.New("Invalid inputs given")
	}
	// reformat
	portLocal := 443
	if port > 10 && port < 65535 {
		portLocal = port
	}
	if strings.Contains(strings.ToLower(authType), "ss") {
		authType = "session"
	} else {
		authType = "basic"
	}

	nsxt := NSX{
		Address:            address,
		Port:               portLocal,
		InsecureSkipVerify: insecureSkipVerify,
		Login:              login,
		Password:           password,
		AuthType:           authType,
		Base64Login: base64.StdEncoding.EncodeToString(
			[]byte(login + ":" + password)),
	}
	return &nsxt, nil
}

// CreateNSXFromConfigfile : creates idm from a config file
func CreateNSXFromConfigfile(filename string) (*NSX, error) {
	config, err := customlib.ReadConfig(filename)
	if err != nil {
		return nil, err
	}
	address := config["address"]
	port, _ := strconv.Atoi(config["port"])
	login := config["login"]
	password := config["password"]
	insecureSkipVerify := false
	if config["insecureSkipVerify"] == "true" {
		insecureSkipVerify = true
	}
	authType := config["authType"]
	// checking
	if len(address) == 0 || len(login) == 0 || len(password) == 0 {
		return nil, errors.New("failed to get the parameters from config file")
	}
	return CreateNSX(address, port, insecureSkipVerify, login, password, authType)
}

// CreateNSXFromVaultKVStore : creates nsx from a vault kvstore. takes ~25ms
func CreateNSXFromVaultKVStore(vault *hashicorpvault.Vault, keyPath string) (*NSX, error) {
	// keyPath = "config/endpoint/nsx/"+endpoint
	valueAddress, err := vault.GetValue(keyPath+`/address`, "")
	if err != nil {
		return nil, errors.New("cannot found address, error: " + err.Error())
	}
	valuePort, err := vault.GetValue(keyPath+`/port`, "")
	if err != nil {
		return nil, errors.New("cannot found port, error: " + err.Error())
	}
	valueInsecureSkipVerify, err := vault.GetValue(keyPath+`/insecureSkipVerify`, "")
	if err != nil {
		return nil, errors.New("cannot found insecureSkipVerify, error: " + err.Error())
	}
	valueLogin, err := vault.GetValue(keyPath+`/login`, "")
	if err != nil {
		return nil, errors.New("cannot found login, error: " + err.Error())
	}
	valuePassword, err := vault.GetValue(keyPath+`/password`, "")
	if err != nil {
		return nil, errors.New("cannot found password, error: " + err.Error())
	}
	valueAuthType, err := vault.GetValue(keyPath+`/authType`, "")
	if err != nil {
		valueAuthType = "basic"
		// return nil, errors.New("cannot found authType, error: " + err.Error())
	}
	return CreateNSX(valueAddress.(string), int(valuePort.(float64)),
		valueInsecureSkipVerify.(bool), valueLogin.(string), valuePassword.(string), valueAuthType.(string))
}

// DestroySessionBased : destroys the session
func (n *NSX) DestroySessionBased() error {
	body := []byte(n.XCSRFToken)
	uri := "https://" +
		n.Address + ":" +
		strconv.Itoa(n.Port) +
		uriSessionDestroy
	headers := make(map[string]string)
	headers["Content-Type"] = "application/x-www-form-urlencoded"
	responseCode, _, _, responseBody := customlib.SendRESTRequest(
		"POST",
		uri,
		body,
		headers,
		nil,
		n.InsecureSkipVerify)
	if responseCode != 200 {
		return errors.New("failed the request, responseCode=" +
			strconv.Itoa(responseCode) + "\nresponseBody=" + string(responseBody))
	}
	// Successfully destroyed
	var nilcookie http.Cookie
	n.Cookie = nilcookie
	n.XCSRFToken = ""
	return nil
}

// InitSessionBased : init a cookie-based session, to be reused afterwards
func (n *NSX) InitSessionBased() error {
	body := []byte("j_username=" + n.Login + "&j_password=" + n.Password)
	uri := "https://" +
		n.Address + ":" +
		strconv.Itoa(n.Port) +
		uriSessionCreate
	headers := make(map[string]string)
	headers["Content-Type"] = "application/x-www-form-urlencoded"
	responseCode, responseHeader, cookies, responseBody := customlib.SendRESTRequest(
		"POST",
		uri,
		body,
		headers,
		nil,
		n.InsecureSkipVerify)
	if responseCode != 200 {
		return errors.New("failed the request, responseCode=" +
			strconv.Itoa(responseCode) + "\nresponseBody=" + string(responseBody))
	}
	// put vars in the structure
	n.XCSRFToken = responseHeader["X-Xsrf-Token"][0]
	n.Cookie = *cookies[0]
	// n.Cookie = regexp.MustCompile("JSESSIONID=(.*?);").FindStringSubmatch(responseHeader["Set-Cookie"][0])[1]
	return nil
}

// SendRequest : generic handler for sending request. type of req to be specified during nsx object init
func (n *NSX) SendRequest(method string, uri string, body []byte) (int, map[string][]string, []byte, error) {
	if n.AuthType == "basic" {
		return n.SendRequestBasicAuth(method, uri, body)
	} else if n.AuthType == "session" {
		return n.SendRequestSessionBased(method, uri, body)
	}
	return 0, nil, nil, errors.New("Unknow authType to use: " + n.AuthType)
}

// SendRequestBasicAuth : send a request using basic auth
func (n *NSX) SendRequestBasicAuth(method string, uri string, body []byte) (int, map[string][]string, []byte, error) {
	if n.Address == "" {
		return 0, nil, nil, errors.New("NSX Object does not have address")
	}
	// get, put, post, delete
	method = strings.ToUpper(method)
	if method != "GET" && method != "POST" && method != "PUT" && method != "DELETE" {
		return 0, nil, nil, errors.New("Invalid method: " + method)
	}
	headers := make(map[string]string)
	headers["Content-Type"] = customlib.APPJSONHeader
	headers["Authorization"] = "Basic " + n.Base64Login
	responseCode, responseHeaders, _, responseBody := customlib.SendRESTRequest(
		method,
		"https://"+n.Address+":"+strconv.Itoa(n.Port)+uri,
		body,
		headers,
		nil,
		n.InsecureSkipVerify)
	if responseCode < 200 || responseCode > 208 {
		fmt.Printf("Response Body: %s\n", responseBody)
		return responseCode, responseHeaders, responseBody, errors.New(
			"failed the " + method + " request, code " + strconv.Itoa(responseCode) + ". See the body for details")
	}
	return responseCode, responseHeaders, responseBody, nil
}

// SendRequestSessionBased : sends a request by using a session-based cookie
func (n *NSX) SendRequestSessionBased(method string, uri string, body []byte) (int, map[string][]string, []byte, error) {
	if n.Address == "" {
		return 0, nil, nil, errors.New("NSX Object does not have address")
	}
	// get, put, post, delete
	method = strings.ToUpper(method)
	if method != "GET" && method != "POST" && method != "PUT" && method != "DELETE" {
		return 0, nil, nil, errors.New("Invalid method: " + method)
	}
	// not correctly opened, renewing
	if n.XCSRFToken != "" && n.Cookie.String() == "" {
		n.DestroySessionBased()
		err := n.InitSessionBased()
		if err != nil {
			return 0, nil, nil, errors.New("Failed to open session, error is: " + err.Error())
		}
	}
	if n.XCSRFToken == "" {
		err := n.InitSessionBased()
		if err != nil {
			return 0, nil, nil, errors.New("Failed to open session, error is: " + err.Error())
		}
	}
	// create the request
	headers := make(map[string]string)
	headers["Content-Type"] = customlib.APPJSONHeader
	headers["X-XSRF-TOKEN"] = n.XCSRFToken
	responseCode, responseHeaders, _, responseBody := customlib.SendRESTRequest(
		method,
		"https://"+n.Address+":"+strconv.Itoa(n.Port)+uri,
		body,
		headers,
		&n.Cookie,
		n.InsecureSkipVerify)
	if responseCode < 200 || responseCode > 208 {
		fmt.Printf("Response Body: %s\n", responseBody)
		return responseCode, responseHeaders, responseBody, errors.New(
			"failed the " + method + " request, code " + strconv.Itoa(responseCode) + ". See the body for details")
	}
	return responseCode, responseHeaders, responseBody, nil
}

// Close : closing properly the session by destroying the cookie and token
func (n *NSX) Close() error {
	if n.XCSRFToken != "" {
		n.DestroySessionBased()
	}
	return nil
}
