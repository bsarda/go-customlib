package nsx

// GET /api/v1/logical-router-ports
// POST /api/v1/logical-router-ports
// DELETE /api/v1/logical-router-ports/<logical-router-port-id>
// GET /api/v1/logical-router-ports/<logical-router-port-id>
// PUT /api/v1/logical-router-ports/<logical-router-port-id>
// GET /api/v1/logical-router-ports/<logical-router-port-id>/arp-table?format=csv
// GET /api/v1/logical-router-ports/<logical-router-port-id>/arp-table
// GET /api/v1/logical-router-ports/<logical-router-port-id>/statistics
// GET /api/v1/logical-router-ports/<logical-router-port-id>/statistics/summary

// ===== Starting funcs =====
