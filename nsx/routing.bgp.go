package nsx

import (
	"encoding/json"
	"errors"
	"strings"

	"github.com/google/go-cmp/cmp"
)

// BGPConfig : the bgp configuration for a T0 router
type BGPConfig struct {
	ID               string             `json:"id"`
	Name             string             `json:"display_name,omitempty"`
	Description      string             `json:"description,omitempty"`
	CreateTime       int64              `json:"_create_time,omitempty"`
	CreateUser       string             `json:"_create_user,omitempty"`
	LastModifiedTime int64              `json:"_last_modified_time,omitempty"`
	LastModifiedUser string             `json:"_last_modified_user,omitempty"`
	Protection       string             `json:"_protection,omitempty"`
	Revision         int                `json:"_revision"`
	Schema           string             `json:"_schema,omitempty"`
	SystemOwned      bool               `json:"_system_owned"`
	LogicalRouterID  string             `json:"logical_router_id,omitempty"`
	ResourceType     string             `json:"resource_type,omitempty"`
	Tags             []Tag              `json:"tags,omitempty"`
	AS               string             `json:"as_num,omitempty"`
	ECMP             bool               `json:"ecmp"`
	Enabled          bool               `json:"enabled"`
	GracefulRestart  bool               `json:"graceful_restart"`
	RouteAggregation []RouteAggregation `json:"route_aggregation,omitempty"`
	LogicalRouter    *LogicalRouter     `json:"-"`
	NSX              *NSX               `json:"-"`
}

// RouteAggregation : the routes to include in BGP config
type RouteAggregation struct {
	Prefix      string `json:"prefix,omitempty"`
	SummaryOnly bool   `json:"summary_only"`
}

// ===== Starting funcs =====

// GetBGPConfig : gets the bgp configuration
func (r *LogicalRouter) GetBGPConfig() (BGPConfig, error) {
	var bgpConfig BGPConfig
	if r.RouterType == Tier1 {
		return bgpConfig, errors.New("Not available for Tier1 routers")
	}
	if len(r.AdvancedConfig.HAVIPConfigs) > 0 {
		return bgpConfig, errors.New("HA VIP is configured. HA VIP and BGP are mutually exclusive")
	}
	uri := strings.Replace(uriRoutingBGP, "<logical-router-id>", r.ID, 1)
	_, _, responseBody, err := r.NSX.SendRequest("GET", uri, nil)
	if err != nil {
		return bgpConfig, err
	}
	json.Unmarshal(responseBody, &bgpConfig)
	bgpConfig.NSX = r.NSX
	bgpConfig.LogicalRouter = r
	return bgpConfig, nil
}

// SetBGPConfig : set the BGP configuration
func (r *LogicalRouter) SetBGPConfig(bgpConfig BGPConfig) error {
	if r.RouterType == Tier1 {
		return errors.New("Not available for Tier1 routers")
	}
	bgpConfig.ResourceType = "BgpConfig"
	body, err := json.Marshal(bgpConfig)
	if err != nil {
		return err
	}
	uri := strings.Replace(uriRoutingBGP, "<logical-router-id>", r.ID, 1)
	_, _, _, err = r.NSX.SendRequest("PUT", uri, body)
	if err != nil {
		return err
	}
	// refresh
	newRouter, _ := r.NSX.GetRouter(r.ID)
	*r = newRouter
	return nil
}

// EnableBGP : enable the BGP
func (r *LogicalRouter) EnableBGP() error {
	bgpConfig, err := r.GetBGPConfig()
	if err != nil {
		return err
	}
	bgpConfig.Enabled = true
	return r.SetBGPConfig(bgpConfig)
}

// DisableBGP : disable the BGP
func (r *LogicalRouter) DisableBGP() error {
	bgpConfig, err := r.GetBGPConfig()
	if err != nil {
		return err
	}
	bgpConfig.Enabled = false
	return r.SetBGPConfig(bgpConfig)
}

// EnableECMP : enable ECMP for BGP
func (r *LogicalRouter) EnableECMP() error {
	bgpConfig, err := r.GetBGPConfig()
	if err != nil {
		return err
	}
	bgpConfig.ECMP = true
	return r.SetBGPConfig(bgpConfig)
}

// DisableECMP : disable ECMP for BGP
func (r *LogicalRouter) DisableECMP() error {
	bgpConfig, err := r.GetBGPConfig()
	if err != nil {
		return err
	}
	bgpConfig.ECMP = false
	return r.SetBGPConfig(bgpConfig)
}

// EnableBGPGracefulRestart : enable the BGP graceful restart
func (r *LogicalRouter) EnableBGPGracefulRestart() error {
	bgpConfig, err := r.GetBGPConfig()
	if err != nil {
		return err
	}
	bgpConfig.GracefulRestart = true
	return r.SetBGPConfig(bgpConfig)
}

// DisableBGPGracefulRestart : disable the BGP graceful restart
func (r *LogicalRouter) DisableBGPGracefulRestart() error {
	bgpConfig, err := r.GetBGPConfig()
	if err != nil {
		return err
	}
	bgpConfig.GracefulRestart = false
	return r.SetBGPConfig(bgpConfig)
}

// SetAS : set the as number (as string because of dot notation)
func (r *LogicalRouter) SetAS(as string) error {
	bgpConfig, err := r.GetBGPConfig()
	if err != nil {
		return err
	}
	bgpConfig.AS = as
	return r.SetBGPConfig(bgpConfig)
}

// SetRouteAggregation : set the routes aggregations
func (r *LogicalRouter) SetRouteAggregation(routeAggregation []RouteAggregation) error {
	bgpConfig, err := r.GetBGPConfig()
	if err != nil {
		return err
	}
	bgpConfig.RouteAggregation = routeAggregation
	return r.SetBGPConfig(bgpConfig)
}

// ClearRouteAggregation : clear the route aggregations
func (r *LogicalRouter) ClearRouteAggregation() error {
	bgpConfig, err := r.GetBGPConfig()
	if err != nil {
		return err
	}
	bgpConfig.RouteAggregation = nil
	return r.SetBGPConfig(bgpConfig)
}

// AddRouteAggregations : add route aggregations to existing
func (r *LogicalRouter) AddRouteAggregations(routeAggregation []RouteAggregation) error {
	bgpConfig, err := r.GetBGPConfig()
	if err != nil {
		return err
	}
	bgpConfig.RouteAggregation = append(bgpConfig.RouteAggregation, routeAggregation...)
	return r.SetBGPConfig(bgpConfig)
}

// AddRouteAggregationFromInput : add route aggregation from raw input to existing
func (r *LogicalRouter) AddRouteAggregationFromInput(prefix string, summaryOnly bool) error {
	bgpConfig, err := r.GetBGPConfig()
	if err != nil {
		return err
	}
	routeAggregation := RouteAggregation{Prefix: prefix, SummaryOnly: summaryOnly}
	bgpConfig.RouteAggregation = append(bgpConfig.RouteAggregation, routeAggregation)
	return r.SetBGPConfig(bgpConfig)
}

// Set : set the BGP configuration
func (b *BGPConfig) Set(bgpConfig BGPConfig) error {
	// it's the same, does not need to update
	if cmp.Equal(*b, bgpConfig) {
		return nil
	}
	body, err := json.Marshal(bgpConfig)
	if err != nil {
		return err
	}
	uri := strings.Replace(uriRoutingBGP, "<logical-router-id>", b.LogicalRouterID, 1)
	_, _, _, err = b.NSX.SendRequest("PUT", uri, body)
	if err != nil {
		return err
	}
	// refresh
	newConfig, _ := b.LogicalRouter.GetBGPConfig()
	*b = newConfig
	return nil
}

// Enable : enable the BGP
func (b *BGPConfig) Enable() error {
	newBgpConfig := *b
	newBgpConfig.Enabled = true
	return b.Set(newBgpConfig)
}

// Disable : disable the BGP
func (b *BGPConfig) Disable() error {
	newBgpConfig := *b
	newBgpConfig.Enabled = false
	return b.Set(newBgpConfig)
}

// EnableECMP : enable ECMP for BGP
func (b *BGPConfig) EnableECMP() error {
	newBgpConfig := *b
	newBgpConfig.ECMP = true
	return b.Set(newBgpConfig)
}

// DisableECMP : disable ECMP for BGP
func (b *BGPConfig) DisableECMP() error {
	newBgpConfig := *b
	newBgpConfig.ECMP = false
	return b.Set(newBgpConfig)
}

// EnableGracefulRestart : enable the BGP graceful restart
func (b *BGPConfig) EnableGracefulRestart() error {
	newBgpConfig := *b
	newBgpConfig.GracefulRestart = true
	return b.Set(newBgpConfig)
}

// DisableGracefulRestart : disable the BGP graceful restart
func (b *BGPConfig) DisableGracefulRestart() error {
	newBgpConfig := *b
	newBgpConfig.GracefulRestart = false
	return b.Set(newBgpConfig)
}

// SetAS : set the as number (as string because of dot notation)
func (b *BGPConfig) SetAS(as string) error {
	newBgpConfig := *b
	newBgpConfig.AS = as
	return b.Set(newBgpConfig)
}

// SetRouteAggregation : set the routes aggregations
func (b *BGPConfig) SetRouteAggregation(routeAggregation []RouteAggregation) error {
	newBgpConfig := *b
	newBgpConfig.RouteAggregation = routeAggregation
	return b.Set(newBgpConfig)
}

// ClearRouteAggregation : clear the route aggregations
func (b *BGPConfig) ClearRouteAggregation() error {
	return b.SetRouteAggregation(nil)
}

// AddRouteAggregations : add route aggregations to existing
func (b *BGPConfig) AddRouteAggregations(routeAggregation []RouteAggregation) error {
	newBgpConfig := *b
	newBgpConfig.RouteAggregation = append(newBgpConfig.RouteAggregation, routeAggregation...)
	return b.Set(newBgpConfig)
}

// AddRouteAggregationFromInput : add route aggregation from raw input to existing
func (b *BGPConfig) AddRouteAggregationFromInput(prefix string, summaryOnly bool) error {
	newBgpConfig := *b
	newBgpConfig.RouteAggregation = append(newBgpConfig.RouteAggregation, RouteAggregation{Prefix: prefix, SummaryOnly: summaryOnly})
	return b.Set(newBgpConfig)
}
