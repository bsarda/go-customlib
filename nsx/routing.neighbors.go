package nsx

import (
	"encoding/json"
	"errors"
	"net/url"
	"regexp"
	"strconv"
	"strings"

	"github.com/google/go-cmp/cmp"
)

// BFDConfigParameters : BFD settings
type BFDConfigParameters struct {
	DeclareDeadMultiple int `json:"declare_dead_multiple,omitempty"` // min: 2, max: 16, default: 3
	ReceiveInterval     int `json:"receive_interval,omitempty"`      // min: 300, max: 60000, default: 1000
	TransmitInterval    int `json:"transmit_interval,omitempty"`     // min: 300, max: 60000, default: 1000
}

// BGPNeighborAddressFamily : neighbor address familly
type BGPNeighborAddressFamily struct {
	Enabled                 bool   `json:"enabled"`
	InFilterIPPrefixlistID  string `json:"in_filter_ipprefixlist_id,omitempty"`
	InFilterRoutemapID      string `json:"in_filter_routemap_id,omitempty"`
	OutFilterIPPrefixlistID string `json:"out_filter_ipprefixlist_id,omitempty"`
	OutFilterRoutemapID     string `json:"out_filter_routemap_id,omitempty"`
	Type                    string `json:"type"` // IPV4_UNICAST, VPNV4_UNICAST
}

// BGPNeighbor : bgp neighbors
type BGPNeighbor struct {
	ID               string                     `json:"id"`
	Name             string                     `json:"display_name,omitempty"`
	Description      string                     `json:"description,omitempty"`
	CreateTime       int64                      `json:"_create_time,omitempty"`
	CreateUser       string                     `json:"_create_user,omitempty"`
	LastModifiedTime int64                      `json:"_last_modified_time,omitempty"`
	LastModifiedUser string                     `json:"_last_modified_user,omitempty"`
	Protection       string                     `json:"_protection,omitempty"`
	Revision         int                        `json:"_revision"`
	Schema           string                     `json:"_schema,omitempty"`
	SystemOwned      bool                       `json:"_system_owned"`
	LogicalRouterID  string                     `json:"logical_router_id,omitempty"`
	ResourceType     string                     `json:"resource_type,omitempty"`
	Tags             []Tag                      `json:"tags,omitempty"`
	AddressFamilies  []BGPNeighborAddressFamily `json:"address_families,omitempty"`
	BFDConfig        BFDConfigParameters        `json:"bfd_config,omitempty"`
	BFDEnabled       bool                       `json:"enable_bfd"`
	Enabled          bool                       `json:"enabled"`
	HoldDownTimer    int                        `json:"hold_down_timer"`
	KeepAliveTimer   int                        `json:"keep_alive_timer"`
	MaximumHopLimit  int                        `json:"maximum_hop_limit,omitempty"`
	Address          string                     `json:"neighbor_address"`
	Password         string                     `json:"password,omitempty"`
	RemoteAS         string                     `json:"remote_as_num"`
	SourceAddresses  []string                   `json:"source_addresses,omitempty"`
	LogicalRouter    *LogicalRouter             `json:"-"`
	NSX              *NSX                       `json:"-"`
}

// ===== Starting funcs =====

// ListNeighborsPaged : list bgp neighbors with cursor, pagesize
func (r *LogicalRouter) ListNeighborsPaged(cursor string, pageSize int) ([]BGPNeighbor, string, error) {
	// options
	var options []string
	if cursor != "" {
		options = append(options, "cursor="+url.QueryEscape(cursor))
	}
	if pageSize > 0 {
		options = append(options, "page_size="+strconv.Itoa(pageSize))
	}
	// transcript
	optionsAsString := ""
	if len(options) > 0 {
		optionsAsString = "?" + strings.Join(options, "&")
	}
	uri := strings.Replace(uriRoutingBGPNeighbors, "<logical-router-id>", r.ID, 1)
	_, _, responseBody, err := r.NSX.SendRequest("GET", uri+optionsAsString, nil)
	if err != nil {
		return nil, "", err
	}
	responseString := strings.Replace(string(responseBody), "\n", "", -1)
	responseCursor := ""
	if regexpCursor := regexp.MustCompile("cursor\"[[:space:]]?:[[:space:]]?\"(.*?)\"").
		FindStringSubmatch(responseString); len(regexpCursor) > 0 {
		responseCursor = regexpCursor[1]
	}
	// get the sub-category result without doing json unmarshall-marshall-unmarshall.
	responseString = strings.TrimRight(regexp.
		MustCompile(".*results\".?:.?").
		ReplaceAllString(responseString, ""), "}")
	var bgpNeighbors []BGPNeighbor
	json.Unmarshal([]byte(responseString), &bgpNeighbors)
	// add the nsx parent object
	for i := range bgpNeighbors {
		bgpNeighbors[i].NSX = r.NSX
		bgpNeighbors[i].LogicalRouter = r
	}
	return bgpNeighbors, responseCursor, nil
}

// ListNeighbors : list all bgp neighbors
func (r *LogicalRouter) ListNeighbors() ([]BGPNeighbor, error) {
	// get first page - size of page defaults
	newBGPNeighbors, cursor, err := r.ListNeighborsPaged("", 0)
	bgpNeighbors := newBGPNeighbors
	if err != nil {
		return nil, err
	}
	// loop for parsing additionnal pages if exists
	for {
		// end of search
		if cursor == "" {
			break
		}
		// get next page
		newBGPNeighbors, cursor, err = r.ListNeighborsPaged(cursor, 0)
		if err != nil {
			return nil, err
		}
		// concat the restults together
		bgpNeighbors = append(bgpNeighbors, newBGPNeighbors...)
	}
	return bgpNeighbors, nil
}

// GetNeighbor : get the neighbor from its id
func (r *LogicalRouter) GetNeighbor(id string) (BGPNeighbor, error) {
	var bgpNeighbor BGPNeighbor
	uri := strings.Replace(uriRoutingBGPNeighbors, "<logical-router-id>", r.ID, 1) + "/" + id
	_, _, responseBody, err := r.NSX.SendRequest("GET", uri, nil)
	if err != nil {
		return bgpNeighbor, err
	}
	json.Unmarshal(responseBody, &bgpNeighbor)
	// adding parent object for recurse use
	bgpNeighbor.NSX = r.NSX
	bgpNeighbor.LogicalRouter = r
	return bgpNeighbor, nil
}

// GetNeighborFromIP : retrieve the neighbor that have this remote ip
func (r *LogicalRouter) GetNeighborFromIP(ip string) (BGPNeighbor, error) {
	var bgpNeighbor BGPNeighbor
	// loop for parsing additionnal pages if exists
	cursor := ""
	for {
		// get next page
		bgpNeighbors, cursor, err := r.ListNeighborsPaged(cursor, 0)
		if err != nil {
			return bgpNeighbor, err
		}
		for _, bn := range bgpNeighbors {
			if bn.Address == ip {
				return bn, nil
			}
		}
		// end of search
		if cursor == "" {
			break
		}
	}
	return bgpNeighbor, errors.New("Failed to find neighbor from ip '" + ip + "'")
}

// GetNeighborFromAS : retrieve the neighbor that have this remote AS
func (r *LogicalRouter) GetNeighborFromAS(as string) (BGPNeighbor, error) {
	var bgpNeighbor BGPNeighbor
	// loop for parsing additionnal pages if exists
	cursor := ""
	for {
		// get next page
		bgpNeighbors, cursor, err := r.ListNeighborsPaged(cursor, 0)
		if err != nil {
			return bgpNeighbor, err
		}
		for _, bn := range bgpNeighbors {
			if bn.RemoteAS == as {
				return bn, nil
			}
		}
		// end of search
		if cursor == "" {
			break
		}
	}
	return bgpNeighbor, errors.New("Failed to find neighbor from remote AS '" + as + "'")
}

// AddNeighbor : add a neighbor
func (r *LogicalRouter) AddNeighbor(bgpNeighbor BGPNeighbor) (BGPNeighbor, error) {
	var createdBgpNeighbor BGPNeighbor
	body, err := json.Marshal(bgpNeighbor)
	if err != nil {
		return createdBgpNeighbor, err
	}
	uri := strings.Replace(uriRoutingBGPNeighbors, "<logical-router-id>", r.ID, 1)
	_, _, responseBody, err := r.NSX.SendRequest("POST", uri, body)
	if err != nil {
		return createdBgpNeighbor, err
	}
	json.Unmarshal(responseBody, &createdBgpNeighbor)
	// adding parent object for recurse use
	createdBgpNeighbor.NSX = r.NSX
	createdBgpNeighbor.LogicalRouter = r
	return createdBgpNeighbor, nil
}

// AddNeighborFromInput : add a neighbor from inputs
func (r *LogicalRouter) AddNeighborFromInput(address string, remoteAS string,
	keepAliveTimer int, holdDownTimer int, name string, enabled bool) (BGPNeighbor, error) {
	if keepAliveTimer == 0 {
		keepAliveTimer = 60
	}
	if holdDownTimer == 0 {
		holdDownTimer = 180
	}
	var bgpNeighbor BGPNeighbor
	bgpNeighbor.Name = name
	bgpNeighbor.Address = address
	bgpNeighbor.RemoteAS = remoteAS
	bgpNeighbor.Enabled = enabled
	bgpNeighbor.KeepAliveTimer = keepAliveTimer
	bgpNeighbor.HoldDownTimer = holdDownTimer
	bgpNeighbor.ResourceType = "BgpNeighbor"
	return r.AddNeighbor(bgpNeighbor)
}

// ListNeighborsPaged : list bgp neighbors with cursor, pagesize
func (b *BGPConfig) ListNeighborsPaged(cursor string, pageSize int) ([]BGPNeighbor, string, error) {
	if b.NSX.Address == "" || b.NSX.Base64Login == "" {
		return nil, "", errors.New("NSX Object is not correct")
	}
	// options
	var options []string
	if cursor != "" {
		options = append(options, "cursor="+url.QueryEscape(cursor))
	}
	if pageSize > 0 {
		options = append(options, "page_size="+strconv.Itoa(pageSize))
	}
	// transcript
	optionsAsString := ""
	if len(options) > 0 {
		optionsAsString = "?" + strings.Join(options, "&")
	}
	uri := strings.Replace(uriRoutingBGPNeighbors, "<logical-router-id>", b.LogicalRouterID, 1)
	_, _, responseBody, err := b.NSX.SendRequest("GET", uri+optionsAsString, nil)
	if err != nil {
		return nil, "", err
	}
	responseString := strings.Replace(string(responseBody), "\n", "", -1)
	responseCursor := ""
	if regexpCursor := regexp.MustCompile("cursor\"[[:space:]]?:[[:space:]]?\"(.*?)\"").
		FindStringSubmatch(responseString); len(regexpCursor) > 0 {
		responseCursor = regexpCursor[1]
	}
	// get the sub-category result without doing json unmarshall-marshall-unmarshall.
	responseString = strings.TrimRight(regexp.
		MustCompile(".*results\".?:.?").
		ReplaceAllString(responseString, ""), "}")
	var bgpNeighbors []BGPNeighbor
	json.Unmarshal([]byte(responseString), &bgpNeighbors)
	// add the nsx parent object
	for i := range bgpNeighbors {
		bgpNeighbors[i].NSX = b.NSX
		bgpNeighbors[i].LogicalRouter = b.LogicalRouter
	}
	return bgpNeighbors, responseCursor, nil
}

// ListNeighbors : list all bgp neighbors
func (b *BGPConfig) ListNeighbors() ([]BGPNeighbor, error) {
	// get first page - size of page defaults
	newBGPNeighbors, cursor, err := b.ListNeighborsPaged("", 0)
	bgpNeighbors := newBGPNeighbors
	if err != nil {
		return nil, err
	}
	// loop for parsing additionnal pages if exists
	for {
		// end of search
		if cursor == "" {
			break
		}
		// get next page
		newBGPNeighbors, cursor, err = b.ListNeighborsPaged(cursor, 0)
		if err != nil {
			return nil, err
		}
		// concat the restults together
		bgpNeighbors = append(bgpNeighbors, newBGPNeighbors...)
	}
	return bgpNeighbors, nil
}

// GetNeighbor : get the neighbor from its id
func (b *BGPConfig) GetNeighbor(id string) (BGPNeighbor, error) {
	var bgpNeighbor BGPNeighbor
	uri := strings.Replace(uriRoutingBGPNeighbors, "<logical-router-id>", b.LogicalRouterID, 1) + "/" + id
	_, _, responseBody, err := b.NSX.SendRequest("GET", uri, nil)
	if err != nil {
		return bgpNeighbor, err
	}
	json.Unmarshal(responseBody, &bgpNeighbor)
	// adding parent object for recurse use
	bgpNeighbor.NSX = b.NSX
	bgpNeighbor.LogicalRouter = b.LogicalRouter
	return bgpNeighbor, nil
}

// GetNeighborFromIP : retrieve the neighbor that have this remote ip
func (b *BGPConfig) GetNeighborFromIP(ip string) (BGPNeighbor, error) {
	var bgpNeighbor BGPNeighbor
	// loop for parsing additionnal pages if exists
	cursor := ""
	for {
		// get next page
		bgpNeighbors, cursor, err := b.ListNeighborsPaged(cursor, 0)
		if err != nil {
			return bgpNeighbor, err
		}
		for _, bn := range bgpNeighbors {
			if bn.Address == ip {
				return bn, nil
			}
		}
		// end of search
		if cursor == "" {
			break
		}
	}
	return bgpNeighbor, errors.New("Failed to find neighbor from ip '" + ip + "'")
}

// GetNeighborFromAS : retrieve the neighbor that have this remote AS
func (b *BGPConfig) GetNeighborFromAS(as string) (BGPNeighbor, error) {
	var bgpNeighbor BGPNeighbor
	// loop for parsing additionnal pages if exists
	cursor := ""
	for {
		// get next page
		bgpNeighbors, cursor, err := b.ListNeighborsPaged(cursor, 0)
		if err != nil {
			return bgpNeighbor, err
		}
		for _, bn := range bgpNeighbors {
			if bn.RemoteAS == as {
				return bn, nil
			}
		}
		// end of search
		if cursor == "" {
			break
		}
	}
	return bgpNeighbor, errors.New("Failed to find neighbor from remote AS '" + as + "'")
}

// AddNeighbor : add a neighbor
func (b *BGPConfig) AddNeighbor(bgpNeighbor BGPNeighbor) (BGPNeighbor, error) {
	var createdBgpNeighbor BGPNeighbor
	body, err := json.Marshal(bgpNeighbor)
	if err != nil {
		return createdBgpNeighbor, err
	}
	uri := strings.Replace(uriRoutingBGPNeighbors, "<logical-router-id>", b.LogicalRouterID, 1)
	_, _, responseBody, err := b.NSX.SendRequest("POST", uri, body)
	if err != nil {
		return createdBgpNeighbor, err
	}
	json.Unmarshal(responseBody, &createdBgpNeighbor)
	// adding parent object for recurse use
	createdBgpNeighbor.NSX = b.NSX
	createdBgpNeighbor.LogicalRouter = b.LogicalRouter
	return createdBgpNeighbor, nil
}

// AddNeighborFromInput : add a neighbor from inputs
func (b *BGPConfig) AddNeighborFromInput(address string, remoteAS string,
	keepAliveTimer int, holdDownTimer int, name string, enabled bool) (BGPNeighbor, error) {
	if keepAliveTimer == 0 {
		keepAliveTimer = 60
	}
	if holdDownTimer == 0 {
		holdDownTimer = 180
	}
	var bgpNeighbor BGPNeighbor
	bgpNeighbor.Name = name
	bgpNeighbor.Address = address
	bgpNeighbor.RemoteAS = remoteAS
	bgpNeighbor.Enabled = enabled
	bgpNeighbor.KeepAliveTimer = keepAliveTimer
	bgpNeighbor.HoldDownTimer = holdDownTimer
	bgpNeighbor.ResourceType = "BgpNeighbor"
	return b.AddNeighbor(bgpNeighbor)
}

// Set : sets the bgp neighbor config
func (n *BGPNeighbor) Set(bgpNeighbor BGPNeighbor) error {
	if cmp.Equal(*n, bgpNeighbor) {
		return nil
	}
	body, err := json.Marshal(bgpNeighbor)
	if err != nil {
		return err
	}
	uri := strings.Replace(uriRoutingBGPNeighbors, "<logical-router-id>", n.LogicalRouterID, 1) + "/" + n.ID
	_, _, _, err = n.NSX.SendRequest("PUT", uri, body)
	if err != nil {
		return err
	}
	// refresh
	newNeighbor, _ := n.LogicalRouter.GetNeighbor(n.ID)
	*n = newNeighbor
	return nil
}

// Delete : delete this neighbor
func (n *BGPNeighbor) Delete() error {
	uri := strings.Replace(uriRoutingBGPNeighbors, "<logical-router-id>", n.LogicalRouterID, 1) + "/" + n.ID
	_, _, _, err := n.NSX.SendRequest("DELETE", uri, nil)
	return err
}

// ClearPassword : clears the
func (n *BGPNeighbor) ClearPassword() error {
	uri := strings.Replace(uriRoutingBGPNeighbors, "<logical-router-id>", n.LogicalRouterID, 1) + "/" + n.ID + "?action=clear_password"
	_, _, _, err := n.NSX.SendRequest("POST", uri, nil)
	return err
}

// SetRemoteIPAddress : sets the neighbor's ip address
func (n *BGPNeighbor) SetRemoteIPAddress(address string) error {
	bgpNeighbor := *n
	bgpNeighbor.Address = address
	return n.Set(bgpNeighbor)
}

// SetAddressFamilies : sets the address families to commit to neighbor
func (n *BGPNeighbor) SetAddressFamilies(addressFamilies []BGPNeighborAddressFamily) error {
	bgpNeighbor := *n
	bgpNeighbor.AddressFamilies = addressFamilies
	return n.Set(bgpNeighbor)
}

// ClearAddressFamilies : remove address families
func (n *BGPNeighbor) ClearAddressFamilies() error {
	return n.SetAddressFamilies(nil)
}

// AddAddressFamilies : add address families to existing
func (n *BGPNeighbor) AddAddressFamilies(addressFamilies []BGPNeighborAddressFamily) error {
	return n.SetAddressFamilies(append(n.AddressFamilies, addressFamilies...))
}

// AddAddressFamily : add (a single) address familu to existing
func (n *BGPNeighbor) AddAddressFamily(addressFamily BGPNeighborAddressFamily) error {
	return n.SetAddressFamilies(append(n.AddressFamilies, addressFamily))
}

// AddAddressFamilyFromInput : add (a single) address familu to existing from inputs
func (n *BGPNeighbor) AddAddressFamilyFromInput(enabled bool, familyType string,
	inFilterIPPrefixlistID string, inFilterRoutemapID string,
	outFilterIPPrefixlistID string, outFilterRoutemapID string) error {
	// parsing type - only 2 possible values IPV4_UNICAST, VPNV4_UNICAST
	typeToCode := "IPV4_UNICAST"
	if strings.Contains(strings.ToLower(familyType), "vpn") {
		typeToCode = "VPNV4_UNICAST"
	}
	// InFilterIPPrefixlistID		Id of the IPPrefix List to be used for IN direction filter
	// InFilterRoutemapID			Id of the RouteMap to be used for IN direction filter
	// OutFilterIPPrefixlistID		Id of the IPPrefixList to be used for OUT direction filter
	// OutFilterRoutemapID			Id of the RouteMap to be used for OUT direction filter
	addressFamily := BGPNeighborAddressFamily{Enabled: enabled, Type: typeToCode}
	if inFilterIPPrefixlistID != "" {
		addressFamily.InFilterIPPrefixlistID = inFilterIPPrefixlistID
	}
	if inFilterRoutemapID != "" {
		addressFamily.InFilterRoutemapID = inFilterRoutemapID
	}
	if outFilterIPPrefixlistID != "" {
		addressFamily.OutFilterIPPrefixlistID = outFilterIPPrefixlistID
	}
	if outFilterRoutemapID != "" {
		addressFamily.OutFilterRoutemapID = outFilterRoutemapID
	}
	// send
	return n.SetAddressFamilies(append(n.AddressFamilies, addressFamily))
}

// SetBFDConfig : sets the bfd configuration
func (n *BGPNeighbor) SetBFDConfig(bfdConfig BFDConfigParameters) error {
	bgpNeighbor := *n
	bgpNeighbor.BFDConfig = bfdConfig
	return n.Set(bgpNeighbor)
}

// SetBFDConfigFromInput : sets the bfd configuration from input
func (n *BGPNeighbor) SetBFDConfigFromInput(declareDeadMultiple int, receiveInterval int, transmitInterval int) error {
	bgpNeighbor := *n
	bgpNeighbor.BFDConfig = BFDConfigParameters{DeclareDeadMultiple: declareDeadMultiple, ReceiveInterval: receiveInterval, TransmitInterval: transmitInterval}
	return n.Set(bgpNeighbor)
}

// EnableBFD : enable the bfd
func (n *BGPNeighbor) EnableBFD() error {
	bgpNeighbor := *n
	bgpNeighbor.BFDEnabled = true
	return n.Set(bgpNeighbor)
}

// DisableBFD : disable the bfd
func (n *BGPNeighbor) DisableBFD() error {
	bgpNeighbor := *n
	bgpNeighbor.BFDEnabled = false
	return n.Set(bgpNeighbor)
}

// Enable : enable this neighbor
func (n *BGPNeighbor) Enable() error {
	bgpNeighbor := *n
	bgpNeighbor.Enabled = true
	return n.Set(bgpNeighbor)
}

// Disable : disable this neighbor
func (n *BGPNeighbor) Disable() error {
	bgpNeighbor := *n
	bgpNeighbor.Enabled = false
	return n.Set(bgpNeighbor)
}

// SetName : sets the neighbor's name
func (n *BGPNeighbor) SetName(name string) error {
	bgpNeighbor := *n
	bgpNeighbor.Name = name
	return n.Set(bgpNeighbor)
}

// SetDescription : sets the neighbor's description
func (n *BGPNeighbor) SetDescription(description string) error {
	bgpNeighbor := *n
	bgpNeighbor.Description = description
	return n.Set(bgpNeighbor)
}

// SetHoldDownTimer : sets the hold down timer
func (n *BGPNeighbor) SetHoldDownTimer(timer int) error {
	bgpNeighbor := *n
	bgpNeighbor.HoldDownTimer = timer
	return n.Set(bgpNeighbor)
}

// SetKeepAliveTimer : sets the keep alive timer
func (n *BGPNeighbor) SetKeepAliveTimer(timer int) error {
	bgpNeighbor := *n
	bgpNeighbor.KeepAliveTimer = timer
	return n.Set(bgpNeighbor)
}

// SetMaximumHopLimit : sets the max hop limit
func (n *BGPNeighbor) SetMaximumHopLimit(limit int) error {
	bgpNeighbor := *n
	bgpNeighbor.MaximumHopLimit = limit
	return n.Set(bgpNeighbor)
}

// SetRemoteAS : sets the remote AS
func (n *BGPNeighbor) SetRemoteAS(as string) error {
	bgpNeighbor := *n
	bgpNeighbor.RemoteAS = as
	return n.Set(bgpNeighbor)
}

// SetSourceAddresses : sets the uplink which could discuss with neighbor
func (n *BGPNeighbor) SetSourceAddresses(addresses []string) error {
	bgpNeighbor := *n
	bgpNeighbor.SourceAddresses = addresses
	return n.Set(bgpNeighbor)
}

// SetTags : sets the tags
func (n *BGPNeighbor) SetTags(tags []Tag) error {
	bgpNeighbor := *n
	bgpNeighbor.Tags = tags
	return n.Set(bgpNeighbor)
}
