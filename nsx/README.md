# Req:
go get -u github.com/google/go-cmp/cmp

# Usage
first, create a NSX client, for example from input parameters:   
```
n, err := nsx.CreateNSX("nsxt21-mgr-01.corp.local", 443, true, "admin", "yourPasswordH3r3!")
```

then, on that object, call methods or sub-objects   
```
allRouters, err := n.ListRouters() 
allT0Routers, err := n.ListRoutersT0() 
firstT0Router := allT0Routers[0]
routerstatus, _ := firstT0Router.GetStatus()
```
