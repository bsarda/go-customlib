package nsx

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"regexp"
	"strconv"
	"strings"

	"github.com/google/go-cmp/cmp"
)

// PrefixConfig : ip prefix config
type PrefixConfig struct {
	Action  string `json:"action"` // PERMIT, DENY
	GE      int    `json:"ge,omitempty"`
	LE      int    `json:"le,omitempty"`
	Network string `json:"network,omitempty"`
}

// IPPrefixList : ip prefix lists
type IPPrefixList struct {
	ID               string         `json:"id"`
	Name             string         `json:"display_name,omitempty"`
	Description      string         `json:"description,omitempty"`
	CreateTime       int64          `json:"_create_time,omitempty"`
	CreateUser       string         `json:"_create_user,omitempty"`
	LastModifiedTime int64          `json:"_last_modified_time,omitempty"`
	LastModifiedUser string         `json:"_last_modified_user,omitempty"`
	Protection       string         `json:"_protection,omitempty"`
	Revision         int            `json:"_revision"`
	Schema           string         `json:"_schema,omitempty"`
	SystemOwned      bool           `json:"_system_owned"`
	LogicalRouterID  string         `json:"logical_router_id,omitempty"`
	ResourceType     string         `json:"resource_type,omitempty"`
	Tags             []Tag          `json:"tags,omitempty"`
	Prefixes         []PrefixConfig `json:"prefixes,omitempty"`
	LogicalRouter    *LogicalRouter `json:"-"`
	NSX              *NSX           `json:"-"`
}

// ===== Starting funcs =====

// ListIPPrefixListsPaged : list ipprefixlist with cursor, pagesize
func (r *LogicalRouter) ListIPPrefixListsPaged(cursor string, pageSize int) ([]IPPrefixList, string, error) {
	// options
	var options []string
	if cursor != "" {
		options = append(options, "cursor="+url.QueryEscape(cursor))
	}
	if pageSize > 0 {
		options = append(options, "page_size="+strconv.Itoa(pageSize))
	}
	// transcript
	optionsAsString := ""
	if len(options) > 0 {
		optionsAsString = "?" + strings.Join(options, "&")
	}
	uri := strings.Replace(uriRoutingIPPrefixLists, "<logical-router-id>", r.ID, 1)
	_, _, responseBody, err := r.NSX.SendRequest("GET", uri+optionsAsString, nil)
	if err != nil {
		return nil, "", err
	}
	responseString := strings.Replace(string(responseBody), "\n", "", -1)
	responseCursor := ""
	if regexpCursor := regexp.MustCompile("cursor\"[[:space:]]?:[[:space:]]?\"(.*?)\"").
		FindStringSubmatch(responseString); len(regexpCursor) > 0 {
		responseCursor = regexpCursor[1]
	}
	// get the sub-category result without doing json unmarshall-marshall-unmarshall.
	responseString = strings.TrimRight(regexp.
		MustCompile(".*results\".?:.?").
		ReplaceAllString(responseString, ""), "}")
	var ipPrefixList []IPPrefixList
	json.Unmarshal([]byte(responseString), &ipPrefixList)
	// add the nsx parent object
	for i := range ipPrefixList {
		ipPrefixList[i].NSX = r.NSX
		ipPrefixList[i].LogicalRouter = r
	}
	return ipPrefixList, responseCursor, nil
}

// ListIPPrefixLists : list all ipprefixlist
func (r *LogicalRouter) ListIPPrefixLists() ([]IPPrefixList, error) {
	var ipPrefixLists []IPPrefixList
	// declare to not reinit within the loop (= and not :=)
	var newIPPrefixLists []IPPrefixList
	var err error
	// loop for parsing additionnal pages if exists
	cursor := ""
	for {
		// get next page
		newIPPrefixLists, cursor, err = r.ListIPPrefixListsPaged(cursor, 1)
		if err != nil {
			return nil, err
		}
		ipPrefixLists = append(ipPrefixLists, newIPPrefixLists...)
		// end of search
		if cursor == "" {
			break
		}
	}
	return ipPrefixLists, nil
}

// GetIPPrefixList : get the ipprefixlist from its id
func (r *LogicalRouter) GetIPPrefixList(id string) (IPPrefixList, error) {
	var ipPrefixList IPPrefixList
	uri := strings.Replace(uriRoutingIPPrefixLists, "<logical-router-id>", r.ID, 1) + "/" + id
	_, _, responseBody, err := r.NSX.SendRequest("GET", uri, nil)
	if err != nil {
		return ipPrefixList, err
	}
	json.Unmarshal(responseBody, &ipPrefixList)
	// adding parent object for recurse use
	ipPrefixList.NSX = r.NSX
	ipPrefixList.LogicalRouter = r
	return ipPrefixList, nil
}

// GetIPPrefixListFromNetwork : get ipprefixlist from network
func (r *LogicalRouter) GetIPPrefixListFromNetwork(network string) (IPPrefixList, error) {
	var ipPrefixList IPPrefixList
	if strings.ToLower(network) == "any" {
		network = ""
	}
	// declare to not reinit within the loop (= and not :=)
	var ipPrefixLists []IPPrefixList
	var err error
	// loop for parsing additionnal pages if exists
	cursor := ""
	for {
		// get next page
		ipPrefixLists, cursor, err = r.ListIPPrefixListsPaged(cursor, 0)
		if err != nil {
			return ipPrefixList, err
		}
		for i := range ipPrefixLists {
			for _, prefix := range ipPrefixLists[i].Prefixes {
				if prefix.Network == network {
					return ipPrefixLists[i], nil
				}
			}
		}
		// end of search
		if cursor == "" {
			break
		}
	}
	return ipPrefixList, errors.New("Failed to find ipprefixlist from network '" + network + "'")
}

// GetIPPrefixListFromNetworkAction : get ipprefixlist from network and action
func (r *LogicalRouter) GetIPPrefixListFromNetworkAction(network string, action string) (IPPrefixList, error) {
	var ipPrefixList IPPrefixList
	if strings.ToLower(network) == "any" {
		network = ""
	}
	if strings.Contains(strings.ToLower(action), "d") {
		action = "DENY"
	} else {
		action = "PERMIT"
	}
	// declare to not reinit within the loop (= and not :=)
	var ipPrefixLists []IPPrefixList
	var err error
	// loop for parsing additionnal pages if exists
	cursor := ""
	for {
		// get next page
		ipPrefixLists, cursor, err = r.ListIPPrefixListsPaged(cursor, 0)
		if err != nil {
			return ipPrefixList, err
		}
		for i := range ipPrefixLists {
			for _, prefix := range ipPrefixLists[i].Prefixes {
				if prefix.Network == network && prefix.Action == action {
					return ipPrefixLists[i], nil
				}
			}
		}
		// end of search
		if cursor == "" {
			break
		}
	}
	return ipPrefixList, errors.New("Failed to find ipprefixlist from network '" + network + "'")
}

// AddIPPrefixList : Add an ipprefixlist with given prefixes
func (r *LogicalRouter) AddIPPrefixList(name string, description string, prefixes []PrefixConfig) (IPPrefixList, error) {
	var ipPrefixList IPPrefixList
	ipPrefixList.ResourceType = "IPPrefixList"
	ipPrefixList.Name = name
	ipPrefixList.Description = description
	ipPrefixList.Prefixes = prefixes
	body, err := json.Marshal(ipPrefixList)
	if err != nil {
		return ipPrefixList, err
	}
	uri := strings.Replace(uriRoutingIPPrefixLists, "<logical-router-id>", r.ID, 1)
	_, _, responseBody, err := r.NSX.SendRequest("POST", uri, body)
	if err != nil {
		return ipPrefixList, err
	}
	var createdIPPrefixList IPPrefixList
	json.Unmarshal(responseBody, &createdIPPrefixList)
	// adding parent object for recurse use
	createdIPPrefixList.NSX = r.NSX
	createdIPPrefixList.LogicalRouter = r
	return createdIPPrefixList, nil
}

// ListIPPrefixListsPaged : list ipprefixlist with cursor, pagesize
func (rc *RoutingConfig) ListIPPrefixListsPaged(cursor string, pageSize int) ([]IPPrefixList, string, error) {
	// options
	var options []string
	if cursor != "" {
		options = append(options, "cursor="+url.QueryEscape(cursor))
	}
	if pageSize > 0 {
		options = append(options, "page_size="+strconv.Itoa(pageSize))
	}
	// transcript
	optionsAsString := ""
	if len(options) > 0 {
		optionsAsString = "?" + strings.Join(options, "&")
	}
	uri := strings.Replace(uriRoutingIPPrefixLists, "<logical-router-id>", rc.LogicalRouterID, 1)
	_, _, responseBody, err := rc.NSX.SendRequest("GET", uri+optionsAsString, nil)
	if err != nil {
		return nil, "", err
	}
	responseString := strings.Replace(string(responseBody), "\n", "", -1)
	responseCursor := ""
	if regexpCursor := regexp.MustCompile("cursor\"[[:space:]]?:[[:space:]]?\"(.*?)\"").
		FindStringSubmatch(responseString); len(regexpCursor) > 0 {
		responseCursor = regexpCursor[1]
	}
	// get the sub-category result without doing json unmarshall-marshall-unmarshall.
	responseString = strings.TrimRight(regexp.
		MustCompile(".*results\".?:.?").
		ReplaceAllString(responseString, ""), "}")
	var ipPrefixList []IPPrefixList
	json.Unmarshal([]byte(responseString), &ipPrefixList)
	// add the nsx parent object
	for i := range ipPrefixList {
		ipPrefixList[i].NSX = rc.NSX
		ipPrefixList[i].LogicalRouter = rc.LogicalRouter
	}
	return ipPrefixList, responseCursor, nil
}

// ListIPPrefixLists : list all ipprefixlist
func (rc *RoutingConfig) ListIPPrefixLists() ([]IPPrefixList, error) {
	var ipPrefixLists []IPPrefixList
	// declare to not reinit within the loop (= and not :=)
	var newIPPrefixLists []IPPrefixList
	var err error
	// loop for parsing additionnal pages if exists
	cursor := ""
	for {
		// get next page
		newIPPrefixLists, cursor, err = rc.ListIPPrefixListsPaged(cursor, 0)
		if err != nil {
			return nil, err
		}
		ipPrefixLists = append(ipPrefixLists, newIPPrefixLists...)
		// end of search
		if cursor == "" {
			break
		}
		fmt.Println("\tanother pass")
	}
	return ipPrefixLists, nil
}

// GetIPPrefixList : get the ipprefixlist from its id
func (rc *RoutingConfig) GetIPPrefixList(id string) (IPPrefixList, error) {
	var ipPrefixList IPPrefixList
	uri := strings.Replace(uriRoutingIPPrefixLists, "<logical-router-id>", rc.LogicalRouterID, 1) + "/" + id
	_, _, responseBody, err := rc.NSX.SendRequest("GET", uri, nil)
	if err != nil {
		return ipPrefixList, err
	}
	json.Unmarshal(responseBody, &ipPrefixList)
	// adding parent object for recurse use
	ipPrefixList.NSX = rc.NSX
	ipPrefixList.LogicalRouter = rc.LogicalRouter
	return ipPrefixList, nil
}

// GetIPPrefixListFromNetwork : get ipprefixlist from network
func (rc *RoutingConfig) GetIPPrefixListFromNetwork(network string) (IPPrefixList, error) {
	var ipPrefixList IPPrefixList
	if strings.ToLower(network) == "any" {
		network = ""
	}
	// declare to not reinit within the loop (= and not :=)
	var ipPrefixLists []IPPrefixList
	var err error
	// loop for parsing additionnal pages if exists
	cursor := ""
	for {
		// get next page
		ipPrefixLists, cursor, err = rc.ListIPPrefixListsPaged(cursor, 0)
		if err != nil {
			return ipPrefixList, err
		}
		for i := range ipPrefixLists {
			for _, prefix := range ipPrefixLists[i].Prefixes {
				if prefix.Network == network {
					return ipPrefixLists[i], nil
				}
			}
		}
		// end of search
		if cursor == "" {
			break
		}
	}
	return ipPrefixList, errors.New("Failed to find ipprefixlist from network '" + network + "'")
}

// GetIPPrefixListFromNetworkAction : get ipprefixlist from network and action
func (rc *RoutingConfig) GetIPPrefixListFromNetworkAction(network string, action string) (IPPrefixList, error) {
	var ipPrefixList IPPrefixList
	if strings.ToLower(network) == "any" {
		network = ""
	}
	if strings.Contains(strings.ToLower(action), "d") {
		action = "DENY"
	} else {
		action = "PERMIT"
	}
	// declare to not reinit within the loop (= and not :=)
	var ipPrefixLists []IPPrefixList
	var err error
	// loop for parsing additionnal pages if exists
	cursor := ""
	for {
		// get next page
		ipPrefixLists, cursor, err = rc.ListIPPrefixListsPaged(cursor, 0)
		if err != nil {
			return ipPrefixList, err
		}
		for i := range ipPrefixLists {
			for _, prefix := range ipPrefixLists[i].Prefixes {
				if prefix.Network == network && prefix.Action == action {
					return ipPrefixLists[i], nil
				}
			}
		}
		// end of search
		if cursor == "" {
			break
		}
	}
	return ipPrefixList, errors.New("Failed to find ipprefixlist from network '" + network + "'")
}

// AddIPPrefixList : Add an ipprefixlist with given prefixes
func (rc *RoutingConfig) AddIPPrefixList(name string, description string, prefixes []PrefixConfig) (IPPrefixList, error) {
	var ipPrefixList IPPrefixList
	ipPrefixList.ResourceType = "IPPrefixList"
	ipPrefixList.Name = name
	ipPrefixList.Description = description
	ipPrefixList.Prefixes = prefixes

	body, err := json.Marshal(ipPrefixList)
	if err != nil {
		return ipPrefixList, err
	}
	uri := strings.Replace(uriRoutingIPPrefixLists, "<logical-router-id>", rc.LogicalRouterID, 1)
	_, _, responseBody, err := rc.NSX.SendRequest("POST", uri, body)
	if err != nil {
		return ipPrefixList, err
	}
	var createdIPPrefixList IPPrefixList
	json.Unmarshal(responseBody, &createdIPPrefixList)
	// adding parent object for recurse use
	createdIPPrefixList.NSX = rc.NSX
	createdIPPrefixList.LogicalRouter = rc.LogicalRouter
	return createdIPPrefixList, nil
}

// Set : generic for updating
func (ippl *IPPrefixList) Set(ipPrefixList IPPrefixList) error {
	// it's the same, does not need to update
	if cmp.Equal(*ippl, ipPrefixList) {
		return nil
	}
	body, err := json.Marshal(ipPrefixList)
	if err != nil {
		return err
	}
	uri := strings.Replace(uriRoutingIPPrefixLists, "<logical-router-id>", ippl.LogicalRouterID, 1) + "/" + ippl.ID
	_, _, _, err = ippl.NSX.SendRequest("PUT", uri, body)
	if err != nil {
		return err
	}
	// refresh
	newIPPL, _ := ippl.LogicalRouter.GetIPPrefixList(ippl.ID)
	*ippl = newIPPL
	return nil
}

// SetName : set the name
func (ippl *IPPrefixList) SetName(name string) error {
	ipPrefixList := *ippl
	ipPrefixList.Name = name
	return ippl.Set(ipPrefixList)
}

// SetDescription : set the description
func (ippl *IPPrefixList) SetDescription(description string) error {
	ipPrefixList := *ippl
	ipPrefixList.Description = description
	return ippl.Set(ipPrefixList)
}

// SetTags : set the tags
func (ippl *IPPrefixList) SetTags(tags []Tag) error {
	ipPrefixList := *ippl
	ipPrefixList.Tags = tags
	return ippl.Set(ipPrefixList)
}

// AddPrefix : adding a prefix
func (ippl *IPPrefixList) AddPrefix(network string, action string, ge int, le int) error {
	if strings.Contains(strings.ToLower(action), "d") {
		action = "DENY"
	} else {
		action = "PERMIT"
	}
	ipPrefixList := *ippl
	ipPrefixList.Prefixes = append(ippl.Prefixes, PrefixConfig{Action: action, Network: network, GE: ge, LE: le})
	return ippl.Set(ipPrefixList)
}

// RemovePrefix : remove a prefix from inputs
func (ippl *IPPrefixList) RemovePrefix(network string, action string, ge int, le int) error {
	if strings.Contains(strings.ToLower(action), "d") {
		action = "DENY"
	} else {
		action = "PERMIT"
	}
	if strings.ToLower(network) == "any" {
		network = ""
	}
	var prefixesConfig []PrefixConfig
	for p := range ippl.Prefixes {
		if ippl.Prefixes[p].Action != action ||
			ippl.Prefixes[p].Network != network ||
			(ge > 0 && ippl.Prefixes[p].GE != ge) ||
			(le > 0 && ippl.Prefixes[p].LE != le) {
			// prefixes to keep - does not match all parameters
			prefixesConfig = append(prefixesConfig, ippl.Prefixes[p])
		}
	}
	ipPrefixList := *ippl
	ipPrefixList.Prefixes = prefixesConfig
	return ippl.Set(ipPrefixList)
}

// Delete : removes the ip prefix list
func (ippl *IPPrefixList) Delete() error {
	uri := strings.Replace(uriRoutingIPPrefixLists, "<logical-router-id>", ippl.LogicalRouterID, 1) + "/" + ippl.ID
	_, _, _, err := ippl.NSX.SendRequest("DELETE", uri, nil)
	return err
}
