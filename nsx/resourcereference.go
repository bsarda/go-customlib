package nsx

// ResourceReference : used for creating source, destination, services, applyto...
type ResourceReference struct {
	ID   string `json:"target_id"`
	Type string `json:"target_type"`
}
