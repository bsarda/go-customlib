package nsx

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/google/go-cmp/cmp"
)

// RedistributionRule : redist rules for Tier0
type RedistributionRule struct {
	Name           string                  `json:"display_name,omitempty"`
	Description    string                  `json:"description,omitempty"`
	Sources        []string                `json:"sources"`     // RedistributionProtocols: Enum: STATIC, NSX_CONNECTED, NSX_STATIC, TIER0_NAT, TIER1_NAT, TIER1_LB_VIP, TIER1_LB_SNAT, BGP. No BGP here.
	Destination    string                  `json:"destination"` // RedistributionProtocols: Enum: STATIC, NSX_CONNECTED, NSX_STATIC, TIER0_NAT, TIER1_NAT, TIER1_LB_VIP, TIER1_LB_SNAT, BGP. Only BGP as of now
	RouteMapID     string                  `json:"route_map_id,omitempty"`
	RedistRuleList *RedistributionRuleList `json:"-"`
}

// RedistributionConfig : routing redistribution (Tier0) configuration
type RedistributionConfig struct {
	ID               string         `json:"id"`
	Name             string         `json:"display_name,omitempty"`
	Description      string         `json:"description,omitempty"`
	CreateTime       int64          `json:"_create_time,omitempty"`
	CreateUser       string         `json:"_create_user,omitempty"`
	LastModifiedTime int64          `json:"_last_modified_time,omitempty"`
	LastModifiedUser string         `json:"_last_modified_user,omitempty"`
	Protection       string         `json:"_protection,omitempty"`
	Revision         int            `json:"_revision"`
	Schema           string         `json:"_schema,omitempty"`
	SystemOwned      bool           `json:"_system_owned"`
	LogicalRouterID  string         `json:"logical_router_id,omitempty"`
	ResourceType     string         `json:"resource_type,omitempty"`
	Tags             []Tag          `json:"tags,omitempty"`
	BGPEnabled       bool           `json:"bgp_enabled,omitempty"`
	LogicalRouter    *LogicalRouter `json:"-"`
	NSX              *NSX           `json:"-"`
}

// RedistributionRuleList : routing redistribution (Tier0) rules list
type RedistributionRuleList struct {
	ID               string               `json:"id"`
	Name             string               `json:"display_name,omitempty"`
	Description      string               `json:"description,omitempty"`
	CreateTime       int64                `json:"_create_time,omitempty"`
	CreateUser       string               `json:"_create_user,omitempty"`
	LastModifiedTime int64                `json:"_last_modified_time,omitempty"`
	LastModifiedUser string               `json:"_last_modified_user,omitempty"`
	Protection       string               `json:"_protection,omitempty"`
	Revision         int                  `json:"_revision"`
	Schema           string               `json:"_schema,omitempty"`
	SystemOwned      bool                 `json:"_system_owned"`
	LogicalRouterID  string               `json:"logical_router_id,omitempty"`
	ResourceType     string               `json:"resource_type,omitempty"`
	Tags             []Tag                `json:"tags,omitempty"`
	Rules            []RedistributionRule `json:"rules,omitempty"`
	LogicalRouter    *LogicalRouter       `json:"-"`
	NSX              *NSX                 `json:"-"`
}

// ===== Starting funcs =====

// GetRedistributionConfig : get the redistribution config - ONLY for Tier0
func (r *LogicalRouter) GetRedistributionConfig() (RedistributionConfig, error) {
	var redistributionConfig RedistributionConfig
	// only 1 parameter is interesting: bgp_enabled
	if r.RouterType == Tier1 {
		return redistributionConfig, errors.New("Not applicable on Tier1 router")
	}
	uri := strings.Replace(uriRoutingRedistribution, "<logical-router-id>", r.ID, 1)
	_, _, responseBody, err := r.NSX.SendRequest("GET", uri, nil)
	if err != nil {
		return redistributionConfig, err
	}
	json.Unmarshal(responseBody, &redistributionConfig)
	// adding parent object for recurse use
	redistributionConfig.NSX = r.NSX
	redistributionConfig.LogicalRouter = r
	return redistributionConfig, nil
}

// SetRedistributionConfig : set the redistribution config - ONLY for Tier0
func (r *LogicalRouter) SetRedistributionConfig(redistConfig RedistributionConfig) error {
	// only 1 parameter is interesting: bgp_enabled
	if r.RouterType == Tier1 {
		return errors.New("Not applicable on Tier1 router")
	}
	body, err := json.Marshal(redistConfig)
	if err != nil {
		return err
	}
	uri := strings.Replace(uriRoutingRedistribution, "<logical-router-id>", r.ID, 1)
	_, _, _, err = r.NSX.SendRequest("PUT", uri, body)
	if err != nil {
		return err
	}
	// refresh
	// newNeighbor, _ :=
	// n = &newNeighbor
	return nil
}

// IsBGPRedistributionEnabled : tells if bgp redist in on or off
func (r *LogicalRouter) IsBGPRedistributionEnabled() bool {
	redist, err := r.GetRedistributionConfig()
	if err != nil {
		fmt.Printf("error while retrieving parameters, err:%v", err)
	}
	return redist.BGPEnabled
}

// EnableBGPRedistribution : tells if bgp redist in on or off
func (r *LogicalRouter) EnableBGPRedistribution() error {
	redist, err := r.GetRedistributionConfig()
	if err != nil {
		fmt.Printf("error while retrieving parameters, err:%v", err)
	}
	if redist.BGPEnabled {
		// already on
		return nil
	}
	redist.BGPEnabled = true
	return r.SetRedistributionConfig(redist)
}

// DisableBGPRedistribution : tells if bgp redist in on or off
func (r *LogicalRouter) DisableBGPRedistribution() error {
	redist, err := r.GetRedistributionConfig()
	if err != nil {
		fmt.Printf("error while retrieving parameters, err:%v", err)
	}
	if !redist.BGPEnabled {
		// already off
		return nil
	}
	redist.BGPEnabled = false
	return r.SetRedistributionConfig(redist)
}

// Set : sets the redist config
func (rc *RedistributionConfig) Set(redistributionConfig RedistributionConfig) error {
	if cmp.Equal(*rc, redistributionConfig) {
		return nil
	}
	body, err := json.Marshal(redistributionConfig)
	if err != nil {
		return err
	}
	uri := strings.Replace(uriRoutingRedistribution, "<logical-router-id>", rc.LogicalRouterID, 1)
	_, _, _, err = rc.NSX.SendRequest("PUT", uri, body)
	if err != nil {
		return err
	}
	// refresh
	newRouter, _ := rc.NSX.GetRouter(rc.LogicalRouterID)
	newRedist, _ := newRouter.GetRedistributionConfig()
	*rc = newRedist
	return nil
}

// EnableBGPRedistribution : enables the bgp redist
func (rc *RedistributionConfig) EnableBGPRedistribution() error {
	if rc.BGPEnabled {
		// already on
		return nil
	}
	redist := *rc
	redist.BGPEnabled = true
	return rc.Set(redist)
}

// DisableBGPRedistribution : disable the bgp redist
func (rc *RedistributionConfig) DisableBGPRedistribution() error {
	if !rc.BGPEnabled {
		// already on
		return nil
	}
	redist := *rc
	redist.BGPEnabled = false
	return rc.Set(redist)
}

// GetRedistributionRuleList : gets the rule list used for redist
func (r *LogicalRouter) GetRedistributionRuleList() (RedistributionRuleList, error) {
	var redistributionRuleList RedistributionRuleList
	// only 1 parameter is interesting: bgp_enabled
	if r.RouterType == Tier1 {
		return redistributionRuleList, errors.New("Not applicable on Tier1 router")
	}
	uri := strings.Replace(uriRoutingRedistributionRules, "<logical-router-id>", r.ID, 1)
	_, _, responseBody, err := r.NSX.SendRequest("GET", uri, nil)
	if err != nil {
		return redistributionRuleList, err
	}
	json.Unmarshal(responseBody, &redistributionRuleList)
	// adding parent object for recurse use
	redistributionRuleList.NSX = r.NSX
	redistributionRuleList.LogicalRouter = r
	return redistributionRuleList, nil
}

// Set : sets the rule list
func (rl *RedistributionRuleList) Set(redistRuleList RedistributionRuleList) error {
	if cmp.Equal(*rl, redistRuleList) {
		return nil
	}
	body, err := json.Marshal(redistRuleList)
	if err != nil {
		return err
	}
	uri := strings.Replace(uriRoutingRedistributionRules, "<logical-router-id>", rl.LogicalRouterID, 1)
	_, _, _, err = rl.NSX.SendRequest("PUT", uri, body)
	if err != nil {
		return err
	}
	// refresh
	newRuleList, _ := rl.LogicalRouter.GetRedistributionRuleList()
	*rl = newRuleList
	return nil
}

// SetRules : sets the rules in the rule list
func (rl *RedistributionRuleList) SetRules(rules []RedistributionRule) error {
	var redistRuleList RedistributionRuleList
	redistRuleList.Rules = rules
	return rl.Set(redistRuleList)
}

// ClearRules : remove all rules
func (rl *RedistributionRuleList) ClearRules() error {
	redistRuleList := *rl
	redistRuleList.Rules = nil
	return rl.Set(redistRuleList)
}

// AddRule : adds a rule
func (rl *RedistributionRuleList) AddRule(rule RedistributionRule) error {
	redistRuleList := *rl
	redistRuleList.Rules = append(rl.Rules, rule)
	return rl.Set(redistRuleList)
}

// AddRuleFromInput : adds a rule from given inputs for protocols to redist
func (rl *RedistributionRuleList) AddRuleFromInput(name string, description string,
	static bool, nsxConnected bool, nsxStatic bool,
	tier0NAT bool, tier1NAT bool, tier1LBVIP bool, tier1LBSNAT bool,
	routemapID string) error {
	var protocols []string
	if static {
		protocols = append(protocols, "STATIC")
	}
	if nsxConnected {
		protocols = append(protocols, "NSX_CONNECTED")
	}
	if nsxStatic {
		protocols = append(protocols, "NSX_STATIC")
	}
	if tier0NAT {
		protocols = append(protocols, "TIER0_NAT")
	}
	if tier1NAT {
		protocols = append(protocols, "TIER1_NAT")
	}
	if tier1LBVIP {
		protocols = append(protocols, "TIER1_LB_VIP")
	}
	if tier1LBSNAT {
		protocols = append(protocols, "TIER1_LB_SNAT")
	}
	// destination is only BGP as of NSX-T 1.0 to 2.2.
	rule := RedistributionRule{Name: name, Description: description, RouteMapID: routemapID, Sources: protocols, Destination: "BGP"}
	// send
	redistRuleList := *rl
	redistRuleList.Rules = append(rl.Rules, rule)
	return rl.Set(redistRuleList)
}

// SetRule : set the rule from the redist rules list
func (rl *RedistributionRuleList) SetRule(name string, description string, sources []string, routeMapID string) error {
	flagHasChanged := false
	redistRuleList := *rl
	var newRules []RedistributionRule
	for _, rule := range redistRuleList.Rules {
		if strings.ToLower(rule.Name) == strings.ToLower(name) {
			// compare - if different
			if !cmp.Equal(sources, rule.Sources) || rule.RouteMapID != routeMapID {
				rule.Sources = sources
				rule.RouteMapID = routeMapID
				flagHasChanged = true
			}
		}
		newRules = append(newRules, rule)
	}
	if flagHasChanged {
		redistRuleList.Rules = newRules
		return rl.Set(redistRuleList)
	}
	// it does not changed
	return nil
}

// SetRuleFromInput : set the rule from the redist rules list, from input
func (rl *RedistributionRuleList) SetRuleFromInput(name string, description string,
	static bool, nsxConnected bool, nsxStatic bool,
	tier0NAT bool, tier1NAT bool, tier1LBVIP bool, tier1LBSNAT bool,
	routemapID string) error {
	flagHasChanged := false
	redistRuleList := *rl
	var newRules []RedistributionRule
	for _, rule := range redistRuleList.Rules {
		if strings.ToLower(rule.Name) == strings.ToLower(name) {
			var protocols []string
			if static {
				protocols = append(protocols, "STATIC")
			}
			if nsxConnected {
				protocols = append(protocols, "NSX_CONNECTED")
			}
			if nsxStatic {
				protocols = append(protocols, "NSX_STATIC")
			}
			if tier0NAT {
				protocols = append(protocols, "TIER0_NAT")
			}
			if tier1NAT {
				protocols = append(protocols, "TIER1_NAT")
			}
			if tier1LBVIP {
				protocols = append(protocols, "TIER1_LB_VIP")
			}
			if tier1LBSNAT {
				protocols = append(protocols, "TIER1_LB_SNAT")
			}
			// compare - if different
			if !cmp.Equal(protocols, rule.Sources) || rule.RouteMapID != routemapID {
				rule.Sources = protocols
				rule.RouteMapID = routemapID
				flagHasChanged = true
			}
		}
		newRules = append(newRules, rule)
	}
	if flagHasChanged {
		redistRuleList.Rules = newRules
		return rl.Set(redistRuleList)
	}
	// it does not changed
	return nil
}

// GetRuleFromName : get the redist rule from name
func (rl *RedistributionRuleList) GetRuleFromName(name string) (RedistributionRule, error) {
	var redistRule RedistributionRule
	for _, rule := range (*rl).Rules {
		if strings.ToLower(rule.Name) == strings.ToLower(name) {
			redistRule = rule
			redistRule.RedistRuleList = rl
			return redistRule, nil
		}
	}
	return redistRule, errors.New("Failed to get rule from name '" + name + "'")
}

// Delete : removes the rule specified by its name (non case sensitive)
func (ru *RedistributionRule) Delete() error {
	redistRuleList := *ru.RedistRuleList
	var newRules []RedistributionRule
	for r := range redistRuleList.Rules {
		if strings.ToLower(redistRuleList.Rules[r].Name) != strings.ToLower(ru.Name) {
			newRules = append(newRules, redistRuleList.Rules[r])
		}
	}
	redistRuleList.Rules = newRules
	return ru.RedistRuleList.Set(redistRuleList)
}

// Set : set the redist rule with new parameters
func (ru *RedistributionRule) Set(name string, description string, sources []string, routeMapID string) error {
	if name == "" {
		return errors.New("Invalid parameters")
	}
	err := ru.RedistRuleList.SetRule(name, description, sources, routeMapID)
	if err != nil {
		return err
	}
	// refresh
	newRedistRule, _ := ru.RedistRuleList.GetRuleFromName(ru.Name)
	*ru = newRedistRule
	return nil
}

// SetName : set name of the redist rule
func (ru *RedistributionRule) SetName(name string) error {
	if name == "" {
		return errors.New("Invalid parameters")
	}
	return ru.Set(name, ru.Description, ru.Sources, ru.RouteMapID)
}

// SetDescription : set description of the redist rule
func (ru *RedistributionRule) SetDescription(description string) error {
	return ru.Set(ru.Name, description, ru.Sources, ru.RouteMapID)
}

// SetRouteMapID : set routeMap ID of the redist rule
func (ru *RedistributionRule) SetRouteMapID(routeMapID string) error {
	return ru.Set(ru.Name, ru.Description, ru.Sources, routeMapID)
}

// SetSources : set sources of the redist rule
func (ru *RedistributionRule) SetSources(sources []string) error {
	return ru.Set(ru.Name, ru.Description, sources, ru.RouteMapID)
}

// GET /api/v1/logical-routers/<logical-router-id>/routing/route-maps
// POST /api/v1/logical-routers/<logical-router-id>/routing/route-maps
// DELETE /api/v1/logical-routers/<logical-router-id>/routing/route-maps/<id>
// GET /api/v1/logical-routers/<logical-router-id>/routing/route-maps/<id>
// PUT /api/v1/logical-routers/<logical-router-id>/routing/route-maps/<id>

// GET /api/v1/logical-routers/<logical-router-id>/routing/static-routes
// POST /api/v1/logical-routers/<logical-router-id>/routing/static-routes
// DELETE /api/v1/logical-routers/<logical-router-id>/routing/static-routes/<id>
// GET /api/v1/logical-routers/<logical-router-id>/routing/static-routes/<id>
// PUT /api/v1/logical-routers/<logical-router-id>/routing/static-routes/<id>

// 	uriRoutingRouteMaps           = "/api/v1/logical-routers/<logical-router-id>/routing/route-maps"
// 	uriRoutingRouteMap            = "/api/v1/logical-routers/<logical-router-id>/routing/route-maps/<id>"
// 	uriRoutingStaticRoutes        = "/api/v1/logical-routers/<logical-router-id>/routing/static-routes"
// 	uriRoutingStaticRoute
