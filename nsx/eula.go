package nsx

import (
	"customlib"
	"errors"
	"regexp"
	"strconv"
)

// GetEULAAcceptance : gets is the eula is accepted or not
func (n *NSX) GetEULAAcceptance() (bool, error) {
	// cursor
	if n.Address == "" || n.Base64Login == "" {
		return false, errors.New("Invalid parameters")
	}
	uri := "https://" +
		n.Address + ":" +
		strconv.Itoa(n.Port) +
		uriEULAAcceptance
	// in case of multiple pages
	headers := make(map[string]string)
	headers["Content-Type"] = customlib.APPJSONHeader
	headers["Authorization"] = "Basic " + n.Base64Login
	responseCode, responseBody := customlib.SendHTTPRequest(
		"GET",
		uri,
		nil,
		headers,
		n.InsecureSkipVerify)
	if responseCode < 200 || responseCode > 208 {
		return false, errors.New("failed the get request, responseCode=" +
			strconv.Itoa(responseCode) + "\nresponseBody=" + string(responseBody))
	}
	regex := regexp.MustCompile(`true`)
	if regex.Match(responseBody) {
		return true, nil
	}
	return false, nil
}

// AcceptEULA : accepts the EULA
func (n *NSX) AcceptEULA() error {
	// cursor
	if n.Address == "" || n.Base64Login == "" {
		return errors.New("Invalid parameters")
	}
	uri := "https://" +
		n.Address + ":" +
		strconv.Itoa(n.Port) +
		uriEULAAccept
	// in case of multiple pages
	headers := make(map[string]string)
	headers["Content-Type"] = customlib.APPJSONHeader
	headers["Authorization"] = "Basic " + n.Base64Login
	responseCode, responseBody := customlib.SendHTTPRequest(
		"POST",
		uri,
		nil,
		headers,
		n.InsecureSkipVerify)
	if responseCode < 200 || responseCode > 208 {
		return errors.New("failed the get request, responseCode=" +
			strconv.Itoa(responseCode) + "\nresponseBody=" + string(responseBody))
	}
	return nil
}
