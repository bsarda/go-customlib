package nsx

// Tag : just a NSX tag!
type Tag struct {
	Scope string `json:"scope"`
	Tag   string `json:"tag"`
}

// ===== Starting funcs =====
