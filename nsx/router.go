package nsx

import (
	"customlib/vars"
	"encoding/json"
	"errors"
	"net/url"
	"regexp"
	"strconv"
	"strings"

	"github.com/google/go-cmp/cmp"
)

// constants for enums
const (
	Tier0 string = "TIER0"
	Tier1 string = "TIER1"
)

// VIPSubnet : structure for a router vip subnet
type VIPSubnet struct {
	ActiveVIPAddresses []string `json:"active_vip_addresses,omitempty"`
	PrefixLength       int      `json:"prefix_length,omitempty"`
}

// HAVIPConfig : structure for a ha ip configuration
type HAVIPConfig struct {
	Enabled                bool        `json:"enabled,omitempty"`
	HAVIPSubnets           []VIPSubnet `json:"ha_vip_subnets"`
	RedundantUplinkPortIDs []string    `json:"redundant_uplink_port_ids"`
}

// LogicalRouterConfig : structure for configs
type LogicalRouterConfig struct {
	ExternalTransitNetworks []string      `json:"external_transit_networks,omitempty"`
	HAVIPConfigs            []HAVIPConfig `json:"ha_vip_configs,omitempty"`
	InternalTransitNetwork  string        `json:"internal_transit_network,omitempty"`
}

// LogicalRouter : structure that represents a logical router
type LogicalRouter struct {
	ID                              string              `json:"id,omitempty"`
	Name                            string              `json:"display_name,omitempty"`
	Description                     string              `json:"description,omitempty"`
	CreateTime                      int64               `json:"_create_time,omitempty"`
	CreateUser                      string              `json:"_create_user,omitempty"`
	LastModifiedTime                int64               `json:"_last_modified_time,omitempty"`
	LastModifiedUser                string              `json:"_last_modified_user,omitempty"`
	Protection                      string              `json:"_protection,omitempty"`
	Revision                        int                 `json:"_revision"`
	Schema                          string              `json:"_schema,omitempty"`
	SystemOwned                     bool                `json:"_system_owned,omitempty"`
	AdvancedConfig                  LogicalRouterConfig `json:"advanced_config,omitempty"`
	EdgeClusterID                   string              `json:"edge_cluster_id,omitempty"`
	FailoverMode                    string              `json:"failover_mode,omitempty"`
	FirewallSections                []ResourceReference `json:"firewall_sections,omitempty"`
	HAMode                          string              `json:"high_availability_mode,omitempty"`
	PreferredEdgeClusterMemberIndex int                 `json:"preferred_edge_cluster_member_index"`
	ResourceType                    string              `json:"resource_type,omitempty"`
	RouterType                      string              `json:"router_type"`
	Tags                            []Tag               `json:"tags,omitempty"`
	NSX                             *NSX                `json:"-"`
}

// LogicalRouterStatus : structure that represents the status of a logical router
type LogicalRouterStatus struct {
	LogicalRouterID     string `json:"logical_router_id"`
	LastUpdateTimestamp int64  `json:"last_update_timestamp,omitempty"`
	PerNodeStatus       []struct {
		ServiceRouterID string `json:"service_router_id,omitempty"`
		HAStatus        string `json:"high_availability_status,omitempty"` // values: ACTIVE, STANDBY, DOWN, SYNC, UNKNOWN
		TransportNodeID string `json:"transport_node_id,omitempty"`
	} `json:"per_node_status,omitempty"`
	// LogicalRouter       *LogicalRouter `json:"-"`
}

// ===== Starting funcs =====

// ListRoutersPaged : list routers with cursor, pagesize, type
func (n *NSX) ListRoutersPaged(cursor string, pageSize int, routerType string) ([]LogicalRouter, string, error) {
	if n.Address == "" || n.Base64Login == "" {
		return nil, "", errors.New("NSX Object is not correct")
	}
	// options
	var options []string
	if cursor != "" {
		options = append(options, "cursor="+url.QueryEscape(cursor))
	}
	if pageSize > 0 {
		options = append(options, "page_size="+strconv.Itoa(pageSize))
	}
	if routerType != "" {
		if strings.Contains(routerType, "0") {
			options = append(options, "router_type="+Tier0)
		} else if strings.Contains(routerType, "1") {
			options = append(options, "router_type="+Tier1)
		}
	}
	// transcript
	optionsAsString := ""
	if len(options) > 0 {
		optionsAsString = "?" + strings.Join(options, "&")
	}
	_, _, responseBody, err := n.SendRequest("GET", uriLogicalRouters+optionsAsString, nil)
	if err != nil {
		return nil, "", err
	}
	responseString := strings.Replace(string(responseBody), "\n", "", -1)
	responseCursor := ""
	if regexpCursor := regexp.MustCompile("cursor\"[[:space:]]?:[[:space:]]?\"(.*?)\"").
		FindStringSubmatch(responseString); len(regexpCursor) > 0 {
		responseCursor = regexpCursor[1]
	}
	// get the sub-category result without doing json unmarshall-marshall-unmarshall.
	responseString = strings.TrimRight(regexp.
		MustCompile(".*results\".?:.?").
		ReplaceAllString(responseString, ""), "}")
	var logicalRouters []LogicalRouter
	json.Unmarshal([]byte(responseString), &logicalRouters)
	// add the nsx parent object
	for i := range logicalRouters {
		logicalRouters[i].NSX = n
	}
	return logicalRouters, responseCursor, nil
}

// ListRouters : list all routers
func (n *NSX) ListRouters() ([]LogicalRouter, error) {
	// get first page - size of page defaults
	newLogicalRouters, cursor, err := n.ListRoutersPaged("", 0, "")
	logicalRouters := newLogicalRouters
	if err != nil {
		return nil, err
	}
	// loop for parsing additionnal pages if exists
	for {
		// end of search
		if cursor == "" {
			break
		}
		// get next page
		newLogicalRouters, cursor, err = n.ListRoutersPaged(cursor, 0, "")
		if err != nil {
			return nil, err
		}
		// concat the restults together
		logicalRouters = append(logicalRouters, newLogicalRouters...)
	}
	return logicalRouters, nil
}

// ListRoutersT0 : list all Tier-0 routers
func (n *NSX) ListRoutersT0() ([]LogicalRouter, error) {
	// get first page - size of page defaults
	newLogicalRouters, cursor, err := n.ListRoutersPaged("", 0, Tier0)
	logicalRouters := newLogicalRouters
	if err != nil {
		return nil, err
	}
	// loop for parsing additionnal pages if exists
	for {
		// end of search
		if cursor == "" {
			break
		}
		// get next page
		newLogicalRouters, cursor, err = n.ListRoutersPaged(cursor, 0, Tier0)
		if err != nil {
			return nil, err
		}
		// concat the restults together
		logicalRouters = append(logicalRouters, newLogicalRouters...)
	}
	return logicalRouters, nil
}

// ListRoutersT1 : list all Tier-1 routers
func (n *NSX) ListRoutersT1() ([]LogicalRouter, error) {
	// get first page - size of page defaults
	newLogicalRouters, cursor, err := n.ListRoutersPaged("", 0, Tier1)
	logicalRouters := newLogicalRouters
	if err != nil {
		return nil, err
	}
	// loop for parsing additionnal pages if exists
	for {
		// end of search
		if cursor == "" {
			break
		}
		// get next page
		newLogicalRouters, cursor, err = n.ListRoutersPaged(cursor, 0, Tier1)
		if err != nil {
			return nil, err
		}
		// concat the restults together
		logicalRouters = append(logicalRouters, newLogicalRouters...)
	}
	return logicalRouters, nil
}

// GetRouter : get the router from id
func (n *NSX) GetRouter(id string) (LogicalRouter, error) {
	var logicalRouter LogicalRouter
	if n.Address == "" || n.Base64Login == "" {
		return logicalRouter, errors.New("NSX Object is not correct")
	}
	uri := strings.Replace(uriLogicalRoutersGet, "<logical-router-id>", id, 1)
	_, _, responseBody, err := n.SendRequest("GET", uri, nil)
	if err != nil {
		return logicalRouter, err
	}
	json.Unmarshal(responseBody, &logicalRouter)
	// adding parent object for recurse use
	logicalRouter.NSX = n
	return logicalRouter, nil
}

// GetRouterFromName : retrieve the router from name
func (n *NSX) GetRouterFromName(name string, flags *vars.SearchFlags) (LogicalRouter, error) {
	var logicalRouter LogicalRouter
	regex, errrx := vars.CreateRegexp(name, flags)
	if errrx != nil {
		return logicalRouter, errrx
	}
	var logicalRouters []LogicalRouter
	var err error
	// loop for parsing additionnal pages if exists
	cursor := ""
	for {
		// get next page
		logicalRouters, cursor, err = n.ListRoutersPaged(cursor, 0, "")
		if err != nil {
			return logicalRouter, err
		}
		for _, lr := range logicalRouters {
			if regex.MatchString(lr.Name) {
				return lr, nil
			}
		}
		// end of search
		if cursor == "" {
			break
		}
	}
	return logicalRouter, errors.New("Failed to find router from name '" + name + "'")
}

// CreateRouter : create router from inputs
func (n *NSX) CreateRouter(routerType string, name string, description string,
	haMode string, failoverMode string,
	edgeClusterID string, preferredEdgeClusterMemberIndex int, advancedConfig LogicalRouterConfig,
	tags []Tag) (LogicalRouter, error) {
	var logicalRouter LogicalRouter
	// reparse the inputs
	if strings.Contains(routerType, "0") {
		routerType = Tier0
	} else {
		routerType = Tier1
	}
	if edgeClusterID != "" {
		haMode = "ACTIVE_STANDBY"
		// a T0 could be only preemptive, only t1 could be nonpreemptive
		if strings.Contains(failoverMode, "N") && routerType == Tier1 {
			failoverMode = "NON_PREEMPTIVE"
		} else {
			failoverMode = "PREEMPTIVE"
		}
	} else {
		// haMode = "ACTIVE_ACTIVE"
		haMode = ""
		failoverMode = ""
	}
	// create object
	var lr LogicalRouter
	lr.Name = name
	lr.Description = description
	lr.RouterType = routerType
	lr.ResourceType = "LogicalRouter"
	if edgeClusterID != "" {
		lr.EdgeClusterID = edgeClusterID
		lr.HAMode = haMode
		lr.FailoverMode = failoverMode
	}
	if routerType == Tier0 {
		lr.PreferredEdgeClusterMemberIndex = preferredEdgeClusterMemberIndex
		lr.AdvancedConfig = advancedConfig
	}
	lr.Tags = tags
	body, err := json.Marshal(lr)
	if err != nil {
		return logicalRouter, err
	}
	_, _, responseBody, err := n.SendRequest("POST", uriLogicalRouters, body)
	if err != nil {
		return logicalRouter, err
	}
	json.Unmarshal(responseBody, &logicalRouter)
	// adding parent object for recurse use
	logicalRouter.NSX = n
	return logicalRouter, nil
}

// GetStatus : get the (full) status of the router
func (r *LogicalRouter) GetStatus() (LogicalRouterStatus, error) {
	var logicalRouterStatus LogicalRouterStatus
	uri := strings.Replace(uriLogicalRoutersStatus, "<logical-router-id>", r.ID, 1)
	_, _, responseBody, err := r.NSX.SendRequest("GET", uri, nil)
	if err != nil {
		return logicalRouterStatus, err
	}
	json.Unmarshal(responseBody, &logicalRouterStatus)
	// adding parent object for recurse use
	// logicalRouterStatus.LogicalRouter = r
	return logicalRouterStatus, nil
}

// Reprocess : force publish router parameters (routing, ...)
func (r *LogicalRouter) Reprocess() error {
	uri := strings.Replace(uriLogicalRoutersReprocess, "<logical-router-id>", r.ID, 1)
	_, _, _, err := r.NSX.SendRequest("POST", uri, nil)
	if err != nil {
		return err
	}
	// refresh
	newRouter, _ := r.NSX.GetRouter(r.ID)
	*r = newRouter
	return nil
}

// Set : generic set/update for logical router
func (r *LogicalRouter) Set(lr LogicalRouter) error {
	// it's the same, does not need to update
	if cmp.Equal(*r, lr) {
		return nil
	}
	body, err := json.Marshal(lr)
	if err != nil {
		return err
	}
	uri := strings.Replace(uriLogicalRoutersUpdate, "<logical-router-id>", r.ID, 1)
	_, _, _, err = r.NSX.SendRequest("PUT", uri, body)
	if err != nil {
		return err
	}
	// refresh
	newRouter, _ := r.NSX.GetRouter(r.ID)
	*r = newRouter
	return nil
}

// UpdateName : sets the name of the router
func (r *LogicalRouter) UpdateName(name string) error {
	if name == "" {
		return errors.New("Invalid parameters")
	}
	newLogicalRouter := *r
	newLogicalRouter.Name = name
	return r.Set(newLogicalRouter)
}

// UpdateDescription : sets the description of the router
func (r *LogicalRouter) UpdateDescription(description string) error {
	newLogicalRouter := *r
	newLogicalRouter.Description = description
	return r.Set(newLogicalRouter)
}

// UpdateFailoverMode : sets the failover mode
func (r *LogicalRouter) UpdateFailoverMode(failoverMode string) error {
	if r.EdgeClusterID != "" || r.RouterType == Tier0 {
		// it's active-active, or tier0 which can't be non-preemptive
		return nil
	}
	// preemptive by default unless specfified with something with ´n' inside
	newLogicalRouter := *r
	newLogicalRouter.FailoverMode = "PREEMPTIVE"
	if strings.Contains(strings.ToLower(failoverMode), "n") {
		newLogicalRouter.FailoverMode = "NON_PREEMPTIVE"
	}
	return r.Set(newLogicalRouter)
}

// UpdateAdvancedConfig : updates the adv config (havip, internal and external transit)
func (r *LogicalRouter) UpdateAdvancedConfig(advancedConfig LogicalRouterConfig) error {
	newLogicalRouter := *r
	newLogicalRouter.AdvancedConfig = advancedConfig
	return r.Set(newLogicalRouter)
}

// RemoveHAVIP : remove a HA VIP that matches the IP address
func (r *LogicalRouter) RemoveHAVIP(ip string) error {
	if r.RouterType == Tier1 {
		return errors.New("Router is a T1 router - HA VIP is not applicable")
	}
	for i := range r.AdvancedConfig.HAVIPConfigs {
		if r.AdvancedConfig.HAVIPConfigs[i].HAVIPSubnets[0].ActiveVIPAddresses[0] == ip {
			if len(r.AdvancedConfig.HAVIPConfigs) == 1 {
				advancedConfig := r.AdvancedConfig
				advancedConfig.HAVIPConfigs = nil
				return r.UpdateAdvancedConfig(advancedConfig)
			}
			advancedConfig := r.AdvancedConfig
			// remove element and slice
			advancedConfig.HAVIPConfigs[i] = advancedConfig.HAVIPConfigs[len(advancedConfig.HAVIPConfigs)-1]
			advancedConfig.HAVIPConfigs = advancedConfig.HAVIPConfigs[:len(advancedConfig.HAVIPConfigs)-1]
			return r.UpdateAdvancedConfig(advancedConfig)
		}
	}
	return errors.New("IP not found in the HA VIP list")
}

// RemoveHAVIPs : removes all HA VIPs
func (r *LogicalRouter) RemoveHAVIPs() error {
	if r.RouterType == Tier1 {
		return errors.New("Router is a T1 router - HA VIP is not applicable")
	}
	advancedConfig := r.AdvancedConfig
	advancedConfig.HAVIPConfigs = nil
	return r.UpdateAdvancedConfig(advancedConfig)
}

// AddHAVIP : add a vip for HA on T0
func (r *LogicalRouter) AddHAVIP(ip string, prefix int, uplinksID []string) error {
	if r.RouterType == Tier1 {
		return errors.New("Router is a T1 router - HA VIP is not applicable")
	}
	advancedConfig := r.AdvancedConfig
	if advancedConfig.HAVIPConfigs == nil || len(advancedConfig.HAVIPConfigs) == 0 {
		advancedConfig.HAVIPConfigs = []HAVIPConfig{
			HAVIPConfig{
				Enabled: true,
				HAVIPSubnets: []VIPSubnet{
					VIPSubnet{
						ActiveVIPAddresses: []string{ip},
						PrefixLength:       prefix,
					},
				},
				RedundantUplinkPortIDs: uplinksID,
			},
		}
		return r.UpdateAdvancedConfig(advancedConfig)
	}
	// already have at least one, add to existing
	newVIPConfig := HAVIPConfig{
		Enabled: true,
		HAVIPSubnets: []VIPSubnet{
			VIPSubnet{
				ActiveVIPAddresses: []string{ip},
				PrefixLength:       prefix,
			},
		},
		RedundantUplinkPortIDs: uplinksID,
	}
	advancedConfig.HAVIPConfigs = append(advancedConfig.HAVIPConfigs, newVIPConfig)
	return r.UpdateAdvancedConfig(advancedConfig)
}

// ChangeEdgeCluster : updates the edge cluster assigned to router - and goes from A/S to A/S if ""
func (r *LogicalRouter) ChangeEdgeCluster(edgeClusterID string) error {
	newLogicalRouter := *r
	newLogicalRouter.EdgeClusterID = edgeClusterID
	// not specified - means A/A
	if edgeClusterID == "" {
		newLogicalRouter.FailoverMode = ""
		newLogicalRouter.HAMode = ""
		newLogicalRouter.PreferredEdgeClusterMemberIndex = 0
	}
	return r.Set(newLogicalRouter)
}

// UpdatePreferedEdgeClusterMemberIndex : update edge prefered member. should be 0 or 1 (A/S is 2...)
func (r *LogicalRouter) UpdatePreferedEdgeClusterMemberIndex(preferredEdgeClusterMemberIndex int) error {
	if preferredEdgeClusterMemberIndex < 0 || preferredEdgeClusterMemberIndex > 1 {
		return errors.New("Invalid parameters")
	}
	newLogicalRouter := *r
	newLogicalRouter.PreferredEdgeClusterMemberIndex = preferredEdgeClusterMemberIndex
	return r.Set(newLogicalRouter)
}

// SwapPreferedEdgeClusterMember : change the prefered to select the backup
func (r *LogicalRouter) SwapPreferedEdgeClusterMember() error {
	preferedMember := 0 // set to first node
	if r.PreferredEdgeClusterMemberIndex == 0 {
		preferedMember = 1
	}
	return r.UpdatePreferedEdgeClusterMemberIndex(preferedMember)
}

// ClearTags : delete all tags associated
func (r *LogicalRouter) ClearTags() error {
	newLogicalRouter := *r
	newLogicalRouter.Tags = nil
	return r.Set(newLogicalRouter)
}

// UpdateTags : update tags - will replace existing
func (r *LogicalRouter) UpdateTags(tags []Tag) error {
	newLogicalRouter := *r
	newLogicalRouter.Tags = tags
	return r.Set(newLogicalRouter)
}

// AddTags : add tags to those existing
func (r *LogicalRouter) AddTags(tags []Tag) error {
	newLogicalRouter := *r
	newLogicalRouter.Tags = append(r.Tags, tags...)
	return r.Set(newLogicalRouter)
}

// Delete : deletes the router
func (r *LogicalRouter) Delete() error {
	uri := strings.Replace(uriLogicalRoutersDelete, "<logical-router-id>", r.ID, 1)
	_, _, _, err := r.NSX.SendRequest("DELETE", uri, nil)
	if err != nil {
		return err
	}
	return nil
}

// TODO:
// GetActiveTransportNodes - could be done by getting SR active nodes
// GetLogicalRouterPorts
