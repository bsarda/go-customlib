package nsx

const (
	uriSessionCreate  = "/api/session/create"
	uriSessionDestroy = "/api/session/destroy"
	// routers
	uriLogicalRouters          = "/api/v1/logical-routers"
	uriLogicalRoutersGet       = "/api/v1/logical-routers/<logical-router-id>"
	uriLogicalRoutersUpdate    = "/api/v1/logical-routers/<logical-router-id>"
	uriLogicalRoutersDelete    = "/api/v1/logical-routers/<logical-router-id>"
	uriLogicalRoutersStatus    = "/api/v1/logical-routers/<logical-router-id>/status"
	uriLogicalRoutersReprocess = "/api/v1/logical-routers/<logical-router-id>?action=reprocess"
	// management
	uriClusterManagementNodesStatus = "/api/v1/cluster/nodes/status"
	// routing
	uriRoutingGlobal              = "/api/v1/logical-routers/<logical-router-id>/routing"
	uriRoutingAdvertisement       = "/api/v1/logical-routers/<logical-router-id>/routing/advertisement"
	uriRoutingAdvertisementRules  = "/api/v1/logical-routers/<logical-router-id>/routing/advertisement/rules"
	uriRoutingBGP                 = "/api/v1/logical-routers/<logical-router-id>/routing/bgp"
	uriRoutingBGPNeighbors        = "/api/v1/logical-routers/<logical-router-id>/routing/bgp/neighbors"
	uriRoutingIPPrefixLists       = "/api/v1/logical-routers/<logical-router-id>/routing/ip-prefix-lists"
	uriRoutingRedistribution      = "/api/v1/logical-routers/<logical-router-id>/routing/redistribution"
	uriRoutingRedistributionRules = "/api/v1/logical-routers/<logical-router-id>/routing/redistribution/rules"
	uriRoutingRouteMaps           = "/api/v1/logical-routers/<logical-router-id>/routing/route-maps"
	uriRoutingStaticRoutes        = "/api/v1/logical-routers/<logical-router-id>/routing/static-routes"
	// eula
	uriEULAAcceptance = "/api/v1/eula/acceptance"
	uriEULAAccept     = "/api/v1/eula/accept"
	// uri = "/api/v1/logical-routers/<logical-router-id>/debug-info?format=text"
)

// logical routers not done
// GET /api/v1/logical-routers/<logical-router-id>/routing/bgp/neighbors/status
// GET /api/v1/logical-routers/<logical-router-id>/routing/forwarding-table?format=csv
// GET /api/v1/logical-routers/<logical-router-id>/routing/forwarding-table
// GET /api/v1/logical-routers/<logical-router-id>/routing/route-table?format=csv
// GET /api/v1/logical-routers/<logical-router-id>/routing/route-table
// GET /api/v1/logical-routers/<logical-router-id>/routing/routing-table?format=csv
// GET /api/v1/logical-routers/<logical-router-id>/routing/routing-table
