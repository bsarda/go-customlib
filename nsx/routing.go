package nsx

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/google/go-cmp/cmp"
)

// ---- Routing config ----

// RoutingConfig : routing configuration
type RoutingConfig struct {
	ID                string         `json:"id"`
	Name              string         `json:"display_name,omitempty"`
	Description       string         `json:"description,omitempty"`
	CreateTime        int64          `json:"_create_time,omitempty"`
	CreateUser        string         `json:"_create_user,omitempty"`
	LastModifiedTime  int64          `json:"_last_modified_time,omitempty"`
	LastModifiedUser  string         `json:"_last_modified_user,omitempty"`
	Protection        string         `json:"_protection,omitempty"`
	Revision          int            `json:"_revision"`
	Schema            string         `json:"_schema,omitempty"`
	SystemOwned       bool           `json:"_system_owned"`
	ForwardingUpTimer int            `json:"forwarding_up_timer"`
	LogicalRouterID   string         `json:"logical_router_id,omitempty"`
	ResourceType      string         `json:"resource_type,omitempty"`
	Tags              *[]Tag         `json:"tags,omitempty"`
	LogicalRouter     *LogicalRouter `json:"-"`
	NSX               *NSX           `json:"-"`
}

// ---- advertisement config ----

// AdvertisementConfig : routing advertisement configuration
type AdvertisementConfig struct {
	ID                       string         `json:"id"`
	Name                     string         `json:"display_name,omitempty"`
	Description              string         `json:"description,omitempty"`
	CreateTime               int64          `json:"_create_time,omitempty"`
	CreateUser               string         `json:"_create_user,omitempty"`
	LastModifiedTime         int64          `json:"_last_modified_time,omitempty"`
	LastModifiedUser         string         `json:"_last_modified_user,omitempty"`
	Protection               string         `json:"_protection,omitempty"`
	Revision                 int            `json:"_revision"`
	Schema                   string         `json:"_schema,omitempty"`
	SystemOwned              bool           `json:"_system_owned"`
	AdvertiseLBSNATIP        bool           `json:"advertise_lb_snat_ip"`
	AdvertiseLBVIP           bool           `json:"advertise_lb_vip"`
	AdvertiseNATRoutes       bool           `json:"advertise_nat_routes"`
	AdvertiseConnectedRoutes bool           `json:"advertise_nsx_connected_routes"`
	AdvertiseStaticRoutes    bool           `json:"advertise_static_routes"`
	Enabled                  bool           `json:"enabled"`
	LogicalRouterID          string         `json:"logical_router_id,omitempty"`
	ResourceType             string         `json:"resource_type,omitempty"`
	Tags                     []Tag          `json:"tags,omitempty"`
	LogicalRouter            *LogicalRouter `json:"-"`
	NSX                      *NSX           `json:"-"`
}

// ---- advertisement rules list config ----

// RuleFilter : filter for advertising rules
// matchroutetypes values: ANY, STATIC, NSX_CONNECTED, T1_NAT, T1_LB_VIP, T1_LB_SNAT
type RuleFilter struct {
	MatchRouteTypes []string `json:"match_route_types,omitempty"` // Required. Minimum items: 1
	PrefixOperator  string   `json:"prefix_operator,omitempty"`   // Enum: GE, EQ. Default: "GE"
}

// AdvertiseRule : routing advertisement rule
type AdvertiseRule struct {
	Action        string         `json:"action"`
	Name          string         `json:"display_name,omitempty"`
	Description   string         `json:"description,omitempty"`
	Networks      []string       `json:"networks,omitempty"`
	RuleFilter    RuleFilter     `json:"rule_filter,omitempty"`
	LogicalRouter *LogicalRouter `json:"-"`
	NSX           *NSX           `json:"-"`
}

// AdvertiseRuleList : routing advertisement rules list
type AdvertiseRuleList struct {
	ID               string          `json:"id"`
	Name             string          `json:"display_name,omitempty"`
	Description      string          `json:"description,omitempty"`
	CreateTime       int64           `json:"_create_time,omitempty"`
	CreateUser       string          `json:"_create_user,omitempty"`
	LastModifiedTime int64           `json:"_last_modified_time,omitempty"`
	LastModifiedUser string          `json:"_last_modified_user,omitempty"`
	Protection       string          `json:"_protection,omitempty"`
	Revision         int             `json:"_revision"`
	Schema           string          `json:"_schema,omitempty"`
	SystemOwned      bool            `json:"_system_owned"`
	Rules            []AdvertiseRule `json:"rules,omitempty"`
	LogicalRouterID  string          `json:"logical_router_id,omitempty"`
	ResourceType     string          `json:"resource_type,omitempty"`
	Tags             []Tag           `json:"tags,omitempty"`
	LogicalRouter    *LogicalRouter  `json:"-"`
	NSX              *NSX            `json:"-"`
}

// ===== Starting funcs =====

// GetRoutingConfig : get the configuration
func (r *LogicalRouter) GetRoutingConfig() (RoutingConfig, error) {
	var routingConfig RoutingConfig
	uri := strings.Replace(uriRoutingGlobal, "<logical-router-id>", r.ID, 1)
	_, _, responseBody, err := r.NSX.SendRequest("GET", uri, nil)
	if err != nil {
		return routingConfig, err
	}
	json.Unmarshal(responseBody, &routingConfig)
	// adding parent object for recurse use
	routingConfig.NSX = r.NSX
	routingConfig.LogicalRouter = r
	return routingConfig, nil
}

// GetForwardingTimer : get the routing forwarding timer (the only parameter in routing>global)
func (r *LogicalRouter) GetForwardingTimer() (int, error) {
	routingConfig, err := r.GetRoutingConfig()
	if err != nil {
		return -1, err
	}
	return routingConfig.ForwardingUpTimer, nil
}

// GetForwardingTimer : get the routing forwarding timer (the only parameter in routing>global)
func (c *RoutingConfig) GetForwardingTimer() int {
	return c.ForwardingUpTimer
}

// SetForwardingTimer : set the routing forwarding timer (the only parameter in routing>global)
func (c *RoutingConfig) SetForwardingTimer(timer int) error {
	if c.ForwardingUpTimer == timer {
		return nil
	}
	body := []byte(fmt.Sprintf(`{"resource_type":"RoutingConfig","id":"%s","forwarding_up_timer":%d,"_revision":%d}`, c.ID, timer, c.Revision))
	uri := strings.Replace(uriRoutingGlobal, "<logical-router-id>", c.LogicalRouterID, 1)
	_, _, _, err := c.NSX.SendRequest("PUT", uri, body)
	if err != nil {
		return err
	}
	// refresh
	newConfig, _ := c.LogicalRouter.GetRoutingConfig()
	*c = newConfig
	return nil
}

// GetAdvertisementConfig : get the advertisement - ONLY for Tier1
func (r *LogicalRouter) GetAdvertisementConfig() (AdvertisementConfig, error) {
	var advertisementConfig AdvertisementConfig
	if r.RouterType == Tier0 {
		return advertisementConfig, errors.New("Not applicable on Tier0 router")
	}
	uri := strings.Replace(uriRoutingAdvertisement, "<logical-router-id>", r.ID, 1)
	_, _, responseBody, err := r.NSX.SendRequest("GET", uri, nil)
	if err != nil {
		return advertisementConfig, err
	}
	json.Unmarshal(responseBody, &advertisementConfig)
	// adding parent object for recurse use
	advertisementConfig.NSX = r.NSX
	advertisementConfig.LogicalRouter = r
	return advertisementConfig, nil
}

// Set : set the advertisement - ONLY for Tier1
func (a *AdvertisementConfig) Set(advertisementConfig AdvertisementConfig) error {
	// it's the same, does not need to update
	if cmp.Equal(*a, advertisementConfig) {
		return nil
	}
	if a.LogicalRouter.RouterType == Tier0 {
		return errors.New("Not applicable on Tier0 router")
	}
	body, err := json.Marshal(advertisementConfig)
	if err != nil {
		return err
	}
	uri := strings.Replace(uriRoutingAdvertisement, "<logical-router-id>", a.LogicalRouterID, 1)
	_, _, _, err = a.NSX.SendRequest("PUT", uri, body)
	if err != nil {
		return err
	}
	// refresh
	newConfig, _ := a.LogicalRouter.GetAdvertisementConfig()
	*a = newConfig
	return nil
}

// SetFromInputs : set the advertisement - ONLY for Tier1
func (a *AdvertisementConfig) SetFromInputs(name string, description string, enabled bool,
	advertiseConnectedRoutes bool, advertiseLBSNATIP bool, advertiseLBVIP bool,
	advertiseNATRoutes bool, advertiseStaticRoutes bool) error {
	var advertisementConfig AdvertisementConfig
	advertisementConfig.Enabled = enabled
	advertisementConfig.AdvertiseConnectedRoutes = advertiseConnectedRoutes
	advertisementConfig.AdvertiseLBSNATIP = advertiseLBSNATIP
	advertisementConfig.AdvertiseLBVIP = advertiseLBVIP
	advertisementConfig.AdvertiseNATRoutes = advertiseNATRoutes
	advertisementConfig.AdvertiseStaticRoutes = advertiseStaticRoutes
	advertisementConfig.ResourceType = a.ResourceType
	return a.Set(advertisementConfig)
}

// EnableAdvertisement : enables the advertise from T1 router
func (r *LogicalRouter) EnableAdvertisement() error {
	advertisementConfig, err := r.GetAdvertisementConfig()
	if err != nil {
		return err
	}
	newAdvertisementConfig := advertisementConfig
	newAdvertisementConfig.Enabled = true
	return advertisementConfig.Set(newAdvertisementConfig)
}

// DisableAdvertisement : disable the advertise from T1 router
func (r *LogicalRouter) DisableAdvertisement() error {
	advertisementConfig, err := r.GetAdvertisementConfig()
	if err != nil {
		return err
	}
	newAdvertisementConfig := advertisementConfig
	newAdvertisementConfig.Enabled = false
	return advertisementConfig.Set(newAdvertisementConfig)
}

// EnableAdvertiseConnectedRoutes : enable advertise of connected
func (r *LogicalRouter) EnableAdvertiseConnectedRoutes() error {
	advertisementConfig, err := r.GetAdvertisementConfig()
	if err != nil {
		return err
	}
	newAdvertisementConfig := advertisementConfig
	newAdvertisementConfig.AdvertiseConnectedRoutes = true
	return advertisementConfig.Set(newAdvertisementConfig)
}

// DisableAdvertiseConnectedRoutes : disable advertise of connected
func (r *LogicalRouter) DisableAdvertiseConnectedRoutes() error {
	advertisementConfig, err := r.GetAdvertisementConfig()
	if err != nil {
		return err
	}
	newAdvertisementConfig := advertisementConfig
	newAdvertisementConfig.AdvertiseConnectedRoutes = false
	return advertisementConfig.Set(newAdvertisementConfig)
}

// EnableAdvertiseLBSNATIP : enable advertise LB SNAT ips
func (r *LogicalRouter) EnableAdvertiseLBSNATIP() error {
	advertisementConfig, err := r.GetAdvertisementConfig()
	if err != nil {
		return err
	}
	newAdvertisementConfig := advertisementConfig
	newAdvertisementConfig.AdvertiseLBSNATIP = true
	return advertisementConfig.Set(newAdvertisementConfig)
}

// DisableAdvertiseLBSNATIP : disable advertise LB SNAT ips
func (r *LogicalRouter) DisableAdvertiseLBSNATIP() error {
	advertisementConfig, err := r.GetAdvertisementConfig()
	if err != nil {
		return err
	}
	newAdvertisementConfig := advertisementConfig
	newAdvertisementConfig.AdvertiseLBSNATIP = false
	return advertisementConfig.Set(newAdvertisementConfig)
}

// EnableAdvertiseLBVIP : enable advertise of LB virtual ips
func (r *LogicalRouter) EnableAdvertiseLBVIP() error {
	advertisementConfig, err := r.GetAdvertisementConfig()
	if err != nil {
		return err
	}
	newAdvertisementConfig := advertisementConfig
	newAdvertisementConfig.AdvertiseLBVIP = true
	return advertisementConfig.Set(newAdvertisementConfig)
}

// DisableAdvertiseLBVIP : enable advertise of LB virtual ips
func (r *LogicalRouter) DisableAdvertiseLBVIP() error {
	advertisementConfig, err := r.GetAdvertisementConfig()
	if err != nil {
		return err
	}
	newAdvertisementConfig := advertisementConfig
	newAdvertisementConfig.Enabled = false
	return advertisementConfig.Set(newAdvertisementConfig)
}

// EnableAdvertiseNATRoutes : enable advertise of NAT
func (r *LogicalRouter) EnableAdvertiseNATRoutes() error {
	advertisementConfig, err := r.GetAdvertisementConfig()
	if err != nil {
		return err
	}
	newAdvertisementConfig := advertisementConfig
	newAdvertisementConfig.AdvertiseNATRoutes = true
	return advertisementConfig.Set(newAdvertisementConfig)
}

// DisableAdvertiseNATRoutes : enable advertise of NAT
func (r *LogicalRouter) DisableAdvertiseNATRoutes() error {
	advertisementConfig, err := r.GetAdvertisementConfig()
	if err != nil {
		return err
	}
	newAdvertisementConfig := advertisementConfig
	newAdvertisementConfig.AdvertiseNATRoutes = false
	return advertisementConfig.Set(newAdvertisementConfig)
}

// EnableAdvertiseStaticRoutes : enable advertise of static
func (r *LogicalRouter) EnableAdvertiseStaticRoutes() error {
	advertisementConfig, err := r.GetAdvertisementConfig()
	if err != nil {
		return err
	}
	newAdvertisementConfig := advertisementConfig
	newAdvertisementConfig.AdvertiseStaticRoutes = true
	return advertisementConfig.Set(newAdvertisementConfig)
}

// DisableAdvertiseStaticRoutes : enable advertise of static
func (r *LogicalRouter) DisableAdvertiseStaticRoutes() error {
	advertisementConfig, err := r.GetAdvertisementConfig()
	if err != nil {
		return err
	}
	newAdvertisementConfig := advertisementConfig
	newAdvertisementConfig.AdvertiseStaticRoutes = false
	return advertisementConfig.Set(newAdvertisementConfig)
}

// GetAdvertiseRuleList : get the advertisement rules - ONLY for Tier1
func (r *LogicalRouter) GetAdvertiseRuleList() (AdvertiseRuleList, error) {
	var advertiseRuleList AdvertiseRuleList
	uri := strings.Replace(uriRoutingAdvertisementRules, "<logical-router-id>", r.ID, 1)
	_, _, responseBody, err := r.NSX.SendRequest("GET", uri, nil)
	if err != nil {
		return advertiseRuleList, err
	}
	json.Unmarshal(responseBody, &advertiseRuleList)
	// adding parent object for recurse use
	advertiseRuleList.NSX = r.NSX
	advertiseRuleList.LogicalRouter = r
	return advertiseRuleList, nil
}

// SetAdvertiseRules : set the advertisement rules - ONLY for Tier1
func (r *LogicalRouter) SetAdvertiseRules(rules []AdvertiseRule) error {
	// get actuals
	advertisedRuleList, err := r.GetAdvertiseRuleList()
	if err != nil {
		return nil
	}
	advertisedRuleList.Rules = rules
	body, err := json.Marshal(advertisedRuleList)
	if err != nil {
		return err
	}
	uri := strings.Replace(uriRoutingAdvertisementRules, "<logical-router-id>", r.ID, 1)
	_, _, _, err = r.NSX.SendRequest("PUT", uri, body)
	if err != nil {
		return err
	}
	// refresh
	newRouter, _ := r.NSX.GetRouter(r.ID)
	*r = newRouter
	return nil
}

// ClearAdvertiseRuleList : removes all the rules
func (r *LogicalRouter) ClearAdvertiseRuleList() error {
	return r.SetAdvertiseRules(nil)
}

// AddAdvertiseRules : add rules to existing (or not) advertisedRules
func (r *LogicalRouter) AddAdvertiseRules(rules []AdvertiseRule) error {
	advertiseRuleList, err := r.GetAdvertiseRuleList()
	if err != nil {
		return err
	}
	rulesToPush := append(advertiseRuleList.Rules, rules...)
	return r.SetAdvertiseRules(rulesToPush)
}

// AddAdvertiseRuleFromInput : adds a rule from plain input
func (r *LogicalRouter) AddAdvertiseRuleFromInput(action string, description string, name string,
	networks []string, ruleFilter RuleFilter) error {
	if strings.Contains(strings.ToLower(action), "y") {
		action = "DENY"
	} else {
		action = "ALLOW"
	}
	var rule = AdvertiseRule{
		Action:        action,
		Description:   description,
		LogicalRouter: r,
		Name:          name,
		Networks:      networks,
		NSX:           r.NSX,
		RuleFilter:    ruleFilter,
	}
	return r.AddAdvertiseRules([]AdvertiseRule{rule})
}

// SetRules : set the advertisement rules - ONLY for Tier1
func (a *AdvertiseRuleList) SetRules(rules []AdvertiseRule) error {
	a.Rules = rules
	body, err := json.Marshal(a)
	if err != nil {
		return err
	}
	uri := strings.Replace(uriRoutingAdvertisementRules, "<logical-router-id>", a.LogicalRouterID, 1)
	_, _, _, err = a.NSX.SendRequest("PUT", uri, body)
	if err != nil {
		return err
	}
	// refresh
	newRuleList, _ := a.LogicalRouter.GetAdvertiseRuleList()
	*a = newRuleList
	return nil
}

// ClearRules : removes all the rules
func (a *AdvertiseRuleList) ClearRules() error {
	return a.SetRules(nil)
}

// AddRules : adding the rules to the list
func (a *AdvertiseRuleList) AddRules(rules []AdvertiseRule) error {
	newRules := append(a.Rules, rules...)
	return a.SetRules(newRules)
}

// AddRuleFromInput : adds a rule from plain input
func (a *AdvertiseRuleList) AddRuleFromInput(action string, description string, name string,
	networks []string, ruleFilter RuleFilter) error {
	if strings.Contains(strings.ToLower(action), "y") {
		action = "DENY"
	} else {
		action = "ALLOW"
	}
	var rule = AdvertiseRule{
		Action:        action,
		Description:   description,
		LogicalRouter: a.LogicalRouter,
		Name:          name,
		Networks:      networks,
		NSX:           a.NSX,
		RuleFilter:    ruleFilter,
	}
	newRules := append(a.Rules, rule)
	return a.SetRules(newRules)
}

// GET /api/v1/logical-routers/<logical-router-id>/routing/static-routes
// POST /api/v1/logical-routers/<logical-router-id>/routing/static-routes
// DELETE /api/v1/logical-routers/<logical-router-id>/routing/static-routes/<id>
// GET /api/v1/logical-routers/<logical-router-id>/routing/static-routes/<id>
// PUT /api/v1/logical-routers/<logical-router-id>/routing/static-routes/<id>

// - ToDo:
// GET /api/v1/logical-routers/<logical-router-id>/debug-info?format=text
// - Not yet done:
// GET /api/v1/logical-routers/<logical-router-id>/routing/bgp/neighbors/status
// GET /api/v1/logical-routers/<logical-router-id>/routing/forwarding-table?format=csv
// GET /api/v1/logical-routers/<logical-router-id>/routing/forwarding-table
// GET /api/v1/logical-routers/<logical-router-id>/routing/route-table?format=csv
// GET /api/v1/logical-routers/<logical-router-id>/routing/route-table
// GET /api/v1/logical-routers/<logical-router-id>/routing/routing-table?format=csv
// GET /api/v1/logical-routers/<logical-router-id>/routing/routing-table
