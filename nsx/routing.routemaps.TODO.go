package nsx

import (
	"encoding/json"
	"errors"
	"fmt"
	"regexp"
	"strings"

	"github.com/google/go-cmp/cmp"
)

// RouteMapSequenceMatch : a match for a community of a sequence
type RouteMapSequenceMatch struct {
	IPPrefixLists            []string `json:"ip_prefix_lists,omitempty"`
	MatchCommunityExpression struct {
		Expression struct {
			CommunityListID   string `json:"community_list_id,omitempty"`  // ID of BGP community list. This value is not required when match_operator is MATCH_REGEX otherwise required.
			MatchOperator     string `json:"match_operator,omitempty"`     // Enum: MATCH_ANY, MATCH_ALL, MATCH_EXACT, MATCH_NONE, MATCH_REGEX
			RegularExpression string `json:"regular_expression,omitempty"` // Regular expression to match BGP communities. If match_operator is MATCH_REGEX then this value must be specified.
		} `json:"expression"`
		Operator string `json:"operator,omitempty"` // Enum: AND
	} `json:"match_community_expression,omitempty"`
	// Expression to match BGP communities. it supports conjunction operator (AND) and five operators within
	// singular community match expression (MATCH_ANY, MATCH_ALL, MATCH_EXACT, MATCH_NONE, MATCH_REGEX).
}

// RouteMapSequence : a "sequence" for routemaps, with action and match criteria
type RouteMapSequence struct {
	RouteMap      *RouteMap             `json:"-"`
	ID            int                   `json:"-"`
	Action        string                `json:"action"` // RoutingFilterAction, Enum: PERMIT, DENY
	MatchCriteria RouteMapSequenceMatch `json:"match_criteria,omitempty"`
	SetCriteria   struct {
		ASPathPrepend string `json:"as_path_prepend,omitempty"`
		Community     string `json:"community,omitempty"`
		MED           uint32 `json:"multi_exit_discriminator,omitempty"`
		Weight        uint16 `json:"weight,omitempty"`
	} `json:"set_criteria,omitempty"`
}

// RouteMap : the struct for routemap
type RouteMap struct {
	ID               string             `json:"id"`
	Name             string             `json:"display_name,omitempty"`
	Description      string             `json:"description,omitempty"`
	CreateTime       int64              `json:"_create_time,omitempty"`
	CreateUser       string             `json:"_create_user,omitempty"`
	LastModifiedTime int64              `json:"_last_modified_time,omitempty"`
	LastModifiedUser string             `json:"_last_modified_user,omitempty"`
	Protection       string             `json:"_protection,omitempty"`
	Revision         int                `json:"_revision"`
	Schema           string             `json:"_schema,omitempty"`
	SystemOwned      bool               `json:"_system_owned"`
	LogicalRouterID  string             `json:"logical_router_id,omitempty"`
	ResourceType     string             `json:"resource_type,omitempty"`
	Tags             []Tag              `json:"tags,omitempty"`
	LogicalRouter    *LogicalRouter     `json:"-"`
	NSX              *NSX               `json:"-"`
	Sequences        []RouteMapSequence `json:"sequences,omitempty"`
}

// ===== Starting funcs =====

// GetRouteMaps : get all the route maps
func (r *LogicalRouter) GetRouteMaps() ([]RouteMap, error) {
	var routeMaps []RouteMap
	// only 1 parameter is interesting: bgp_enabled
	if r.RouterType == Tier1 {
		return routeMaps, errors.New("Not applicable on Tier1 router")
	}
	uri := strings.Replace(uriRoutingRouteMaps, "<logical-router-id>", r.ID, 1)
	_, _, responseBody, err := r.NSX.SendRequest("GET", uri, nil)
	if err != nil {
		return routeMaps, err
	}
	// json.Unmarshal(responseBody, &routeMaps)
	responseString := strings.Replace(string(responseBody), "\n", "", -1)
	// get the sub-category result without doing json unmarshall-marshall-unmarshall.
	responseString = strings.TrimRight(regexp.
		MustCompile(".*results\".?:.?").
		ReplaceAllString(responseString, ""), "}")
	json.Unmarshal([]byte(responseString), &routeMaps)
	// add the nsx parent object
	for i := range routeMaps {
		routeMaps[i].NSX = r.NSX
		routeMaps[i].LogicalRouter = r
		for s := range routeMaps[i].Sequences {
			routeMaps[i].Sequences[s].RouteMap = &routeMaps[i]
		}
	}
	return routeMaps, nil
}

// GetRouteMap : get route maps from id
func (r *LogicalRouter) GetRouteMap(id string) (RouteMap, error) {
	var routeMap RouteMap
	if r.RouterType == Tier1 {
		return routeMap, errors.New("Not applicable on Tier1 router")
	}
	uri := strings.Replace(uriRoutingRouteMaps, "<logical-router-id>", r.ID, 1) + "/" + id
	_, _, responseBody, err := r.NSX.SendRequest("GET", uri, nil)
	if err != nil {
		return routeMap, err
	}
	json.Unmarshal(responseBody, &routeMap)
	// adding parent object for recurse use
	routeMap.NSX = r.NSX
	routeMap.LogicalRouter = r
	return routeMap, nil
}

// GetRouteMapFromName : get route maps from name
func (r *LogicalRouter) GetRouteMapFromName(name string) (RouteMap, error) {
	var routeMap RouteMap
	routeMaps, err := r.GetRouteMaps()
	if err != nil {
		return routeMap, err
	}
	for _, rm := range routeMaps {
		if strings.ToLower(rm.Name) == strings.ToLower(name) {
			return rm, nil
		}
	}
	return routeMap, errors.New("Failed to find routemap from name '" + name + "'")
}

// AddRouteMap : add route map
func (r *LogicalRouter) AddRouteMap(routeMap RouteMap) (RouteMap, error) {
	var createdRouteMap RouteMap
	body, err := json.Marshal(routeMap)
	if err != nil {
		return createdRouteMap, err
	}
	uri := strings.Replace(uriRoutingRouteMaps, "<logical-router-id>", r.ID, 1)
	_, _, responseBody, err := r.NSX.SendRequest("POST", uri, body)
	if err != nil {
		return createdRouteMap, err
	}
	json.Unmarshal(responseBody, &createdRouteMap)
	// adding parent object for recurse use
	createdRouteMap.NSX = r.NSX
	createdRouteMap.LogicalRouter = r
	return createdRouteMap, nil
}

// AddRouteMapFromSequences : add route map from given sequence
func (r *LogicalRouter) AddRouteMapFromSequences(name string, description string, sequences []RouteMapSequence) (RouteMap, error) {
	var routeMap RouteMap
	routeMap.ResourceType = "RouteMap"
	routeMap.Name = name
	routeMap.Description = description
	routeMap.Sequences = sequences
	return r.AddRouteMap(routeMap)
}

// AddRouteMapFromInput : add a routemap from inputs
func (r *LogicalRouter) AddRouteMapFromInput(name string, description string, action string, ipprefixListsID []string,
	aspathPrepend string, community string, med uint32, weight uint16) (RouteMap, error) {
	// create the match criteria that uses ipprefix list - not communities
	var matchCriteria RouteMapSequenceMatch
	matchCriteria.IPPrefixLists = ipprefixListsID
	// create the sequence
	var sequence RouteMapSequence
	sequence.Action = "PERMIT"
	if strings.Contains(strings.ToLower(action), "y") {
		sequence.Action = "DENY"
	}
	sequence.SetCriteria.ASPathPrepend = aspathPrepend
	sequence.SetCriteria.Community = community
	sequence.SetCriteria.MED = med
	sequence.SetCriteria.Weight = weight
	// create the routemap itself
	var routeMap RouteMap
	routeMap.ResourceType = "RouteMap"
	routeMap.Name = name
	routeMap.Description = description
	routeMap.Sequences = []RouteMapSequence{sequence}
	// launch
	return r.AddRouteMap(routeMap)
}

// Delete : delete a route map
func (rm *RouteMap) Delete() error {
	uri := strings.Replace(uriRoutingRouteMaps, "<logical-router-id>", rm.LogicalRouterID, 1) + "/" + rm.ID
	_, _, _, err := rm.NSX.SendRequest("DELETE", uri, nil)
	return err
}

// Set : sets the route map config
func (rm *RouteMap) Set(routeMap RouteMap) error {

	// var b []byte
	// json.Unmarshal(b, *rm)
	// var t []byte
	// json.Unmarshal(t, routeMap)
	b, _ := json.Marshal(rm)
	t, _ := json.Marshal(routeMap)

	fmt.Printf("routemap from src: %s\n", b)
	fmt.Printf("routemap to implm: %s\n", t)
	// fmt.Printf("routemap from src: %v\n", rm)
	// fmt.Printf("routemap to implm: %v\n", routeMap)
	fmt.Printf("====================WILL BE UPDATED !!!====================\n")
	return nil

	if cmp.Equal(*rm, routeMap) {
		return nil
	}
	body, err := json.Marshal(routeMap)
	if err != nil {
		return err
	}
	uri := strings.Replace(uriRoutingRouteMaps, "<logical-router-id>", rm.LogicalRouterID, 1) + "/" + rm.ID
	_, _, _, err = rm.NSX.SendRequest("PUT", uri, body)
	if err != nil {
		return err
	}
	// refresh
	newRouteMap, _ := rm.LogicalRouter.GetRouteMap(rm.ID)
	*rm = newRouteMap
	return nil
}

func (rm *RouteMap) GetSequence(sequenceNumber int) (RouteMapSequence, error) {
	var routemapSequence RouteMapSequence
	if sequenceNumber < 0 || sequenceNumber > len(rm.Sequences)-1 {
		return routemapSequence, errors.New("Invalid number for routemap sequence")
	}
	routemapSequence = rm.Sequences[sequenceNumber]
	routemapSequence.RouteMap = rm
	routemapSequence.ID = sequenceNumber
	return routemapSequence, nil
}

func (s *RouteMapSequence) SetSequenceAction(action string) error {
	routemap := *s.RouteMap
	routemap.Sequences[s.ID].Action = "PERMIT"
	if strings.Contains(strings.ToLower(action), "y") {
		routemap.Sequences[s.ID].Action = "DENY"
	}
	fmt.Println("WILL UPDATE THE ROUTEMAP")

	b, _ := json.Marshal(s.RouteMap)
	t, _ := json.Marshal(routemap)
	fmt.Printf("routemap from src: %s\n", b)
	fmt.Printf("routemap to implm: %s\n", t)
	fmt.Println("FINISHED ?")
	return nil
	// return s.RouteMap.Set(routemap)
}

//Sequences?
// fmt.Printf("routeMap 1 sequences routemap: %v\n", routeMaps[0].Sequences[1].RouteMap)
// fmt.Printf("routeMap 1 sequences action: %v\n", routeMaps[0].Sequences[1].Action)
// fmt.Printf("routeMap 1 sequences ippl: %v\n", routeMaps[0].Sequences[1].MatchCriteria.IPPrefixLists)
// fmt.Printf("routeMap 1 sequences: %v\n", routeMaps[0].Sequences[1].MatchCriteria.MatchCommunityExpression.Expression.CommunityListID)
// fmt.Printf("routeMap 1 sequences: %v\n", routeMaps[0].Sequences[1].MatchCriteria.MatchCommunityExpression.Expression.MatchOperator)
// fmt.Printf("routeMap 1 sequences: %v\n", routeMaps[0].Sequences[1].MatchCriteria.MatchCommunityExpression.Expression.RegularExpression)
// fmt.Printf("routeMap 1 sequences: %v\n", routeMaps[0].Sequences[1].MatchCriteria.MatchCommunityExpression.Operator)
// fmt.Printf("routeMap 1 sequences asprep: %v\n", routeMaps[0].Sequences[1].SetCriteria.ASPathPrepend)
// fmt.Printf("routeMap 1 sequences community: %v\n", routeMaps[0].Sequences[1].SetCriteria.Community)
// fmt.Printf("routeMap 1 sequences med: %v\n", routeMaps[0].Sequences[1].SetCriteria.MED)
// fmt.Printf("routeMap 1 sequences weight: %v\n", routeMaps[0].Sequences[1].SetCriteria.Weight)
