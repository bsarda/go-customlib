package vars

import (
	"sort"
)

// ReturnFirstBool : takes a number of bool in input, and will return only the first true, others will be forced to false
func ReturnFirstBool(params ...bool) []bool {
	boolArrayToReturn := make([]bool, len(params))
	var foundATrue = false
	for counter, arg := range params {
		if arg && !foundATrue {
			boolArrayToReturn[counter] = true
			foundATrue = true
		} else {
			boolArrayToReturn[counter] = false
		}
	}
	return boolArrayToReturn
}

// IsElementInArray : simple iteration over an array.
func IsElementInArray(array interface{}, element interface{}) bool {
	// iterative way
	for _, sub := range array.([]string) {
		if sub == element {
			return true
		}
	}
	return false
}

// IsElementInInterfaceArray : simple iteration over an array.
func IsElementInInterfaceArray(array []interface{}, element interface{}) bool {
	// iterative way
	for _, sub := range array {
		if sub == element {
			return true
		}
	}
	return false
}

// IsElementInListNoTrailingSlash : simple iteration over an array.
func IsElementInListNoTrailingSlash(array interface{}, el interface{}) bool {
	// iterative way
	for _, element := range array.([]string) {
		if RemoveTrailingSlash(element) == RemoveTrailingSlash(el.(string)) {
			return true
		}
	}
	return false
}

// IsStringInArrayString : used as a binary search. normally most effective on large arrays.
// seems a good idea, but the sort is so consuming that event a 1k items array is 30x more consuming than a standard iterate...
// note: int treated as string might be UNCORRECTLY sorted.
func IsStringInArrayString(array []string, str string) bool {
	// sort the array
	// in case of int as strings, it does not sort effectively
	sort.Strings(array)
	L := 0
	R := len(array) - 1
	for {
		m := (L + R) / 2
		if L == R && L == m {
			return false
		}
		if array[m] == str {
			return true
		} else if array[m] > str {
			// it's GT
			R = m - 1

		} else if array[m] < str {
			// it's lower
			L = m + 1
		}
	}
}

// RemoveTrailingSlashInArray : returns the slash at the end, if any - for an array of string
func RemoveTrailingSlashInArray(array []string) []string {
	var returnArray []string
	for _, element := range array {
		returnArray = append(returnArray, RemoveTrailingSlash(element))
	}
	return returnArray
}

// RemoveDuplicateInt : removes the duplicates from an array
func RemoveDuplicateInt(elements []int) []int {
	// Use map to record duplicates as we find them.
	encountered := map[int]bool{}
	result := []int{}
	// parse each
	for v := range elements {
		if encountered[elements[v]] == true {
			// Do not add duplicate.
		} else {
			// Record this element as an encountered element.
			encountered[elements[v]] = true
			// Append to result slice.
			result = append(result, elements[v])
		}
	}
	// Return the new slice.
	return result
}

// RemoveDuplicateStrings : removes the duplicates from an array
func RemoveDuplicateStrings(elements []string) []string {
	encountered := map[string]bool{}
	// Create a map of all unique elements.
	for v := range elements {
		encountered[elements[v]] = true
	}
	// Place all keys from the map into a slice.
	result := []string{}
	for key := range encountered {
		result = append(result, key)
	}
	return result
}
