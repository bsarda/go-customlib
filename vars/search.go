package vars

import "regexp"

// SearchFlags : used for adding tags to regexp
type SearchFlags struct {
	ExactMatch       bool
	IgnoreCase       bool
	Multiline        bool
	LinefeedMatchDot bool
	Ungreedy         bool
}

// CreateRegexp : create the regexp with flags
func CreateRegexp(query string, flags *SearchFlags) (*regexp.Regexp, error) {
	if flags != nil {
		flagsAsString := ""
		if flags.IgnoreCase {
			flagsAsString += "i"
		}
		if flags.Multiline {
			flagsAsString += "m"
		}
		if flags.LinefeedMatchDot {
			flagsAsString += "s"
		}
		if flags.Ungreedy {
			flagsAsString += "U"
		}
		// exact
		if flags.ExactMatch {
			query = "^" + query + "$"
		}
		// add to query
		if len(flagsAsString) > 0 {
			query = "(?" + flagsAsString + ")" + query
		}
	}
	return regexp.Compile(query)
}
