package vars

import (
	"fmt"
	"strings"
)

// RemoveTrailingSlash : returns the slash at the end, if any
func RemoveTrailingSlash(name string) string {
	if strings.HasSuffix(name, "/") {
		return name[:len(name)-1]
	}
	return name
}

// EscapeForRegex : escape the text to be prepared for a regexp
func EscapeForRegex(text string) string {
	// text[0] == "^"
	// if starts with ^
	startsWithCaret := false
	if text[0] == byte(94) {
		text = text[1:]
		startsWithCaret = true
		fmt.Println("STARTS WITH CARET !!")
	}
	endsWithDollar := false
	if text[0] == byte(94) {
		text = text[1:]
		endsWithDollar = true
		fmt.Println("ENDS WITH DOLLS !!")
	}
	// replace the known chars for regex
	text = strings.Replace(text, "(", "\\(", -1)
	text = strings.Replace(text, ")", "\\)", -1)
	text = strings.Replace(text, "[", "\\[", -1)
	text = strings.Replace(text, "]", "\\]", -1)
	text = strings.Replace(text, "{", "\\{", -1)
	text = strings.Replace(text, "}", "\\}", -1)
	text = strings.Replace(text, ".", "\\.", -1)
	text = strings.Replace(text, "+", "\\+", -1)
	text = strings.Replace(text, "^", "\\^", -1)
	text = strings.Replace(text, "$", "\\$", -1)
	// re-set the start/end
	if startsWithCaret {
		text = "^" + text
	}
	if endsWithDollar {
		text = text + "$"
	}
	fmt.Printf("REPLACED TEXT : %s\t", text)
	return text
}
