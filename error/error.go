package error

import (
	"customlib/logger"
	"errors"
	"fmt"
	"runtime"
	"strings"
)

// =============================================================================
// ====================== create error formatter and funcs =====================

// internal FormatError : transform inputs to a string, with the formating.
// the format is [classID] (errorID) fullDescription
func _FormatError(function string, classID int, id int, desc ...interface{}) string {
	str := ""
	println(len(desc))
	for _, i := range desc {
		println(fmt.Sprintf("%v", i))
		str += fmt.Sprintf("%v", i)
	}
	println(str)
	return fmt.Sprintf("{%v} [%v] (%v) %v", function, classID, id, str)
}

// PrintError : show the error on the screen
func PrintError(classID int, id int, desc ...interface{}) {
	// get the caller function name
	pc := make([]uintptr, 15)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	// pretty print
	println(len(desc))

	fmt.Println(_FormatError(frame.Function, classID, id, desc))
}

// FormatError : pretty transform the error to enclose the good code
func FormatError(classID int, id int, desc ...interface{}) error {
	// get the caller function name
	pc := make([]uintptr, 15)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	// return
	return errors.New(_FormatError(frame.Function, classID, id, desc))
}

// LogError : Send the error to the defined logger
func LogError(logger *logger.Logger, classID int, id int, desc string) {
	// get the caller function name
	pc := make([]uintptr, 15)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	// log to the writer collector
	logger.Error(_FormatError(frame.Function, classID, id, desc))
}

// ConvertToStringJSONCompatible : return a string from an error, without quotes
func ConvertToStringJSONCompatible(err error) string {
	return strings.Replace(err.Error(), `"`, `\"`, -1)
}

// ConvertToStringJSONResponse : return a string from an error, without quotes, for an http response
func ConvertToStringJSONResponse(err error) string {
	return `{"error":"` + strings.Replace(err.Error(), `"`, `\"`, -1) + `"}`
}

// ConvertToBytesJSONResponse : return a byte array from an error, without quotes, for an http response
func ConvertToBytesJSONResponse(err error) []byte {
	return []byte(`{"error":"` + strings.Replace(err.Error(), `"`, `\"`, -1) + `"}`)
}
