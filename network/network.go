package network

import (
	"net"
	"regexp"
	"strings"
)

// GetLocalIPAddresses : get the local IP addresses
func GetLocalIPAddresses() ([]string, []string, error) {
	ifaces, err := net.Interfaces()
	if err != nil {
		return nil, nil, err
	}
	var ips4 []string
	var ips6 []string
	for _, iface := range ifaces {
		// avoid the vmnet brides/nat, loop, docker...
		if regexp.MustCompile(`^vmnet.*|^lo|^docker`).MatchString(iface.Name) {
			continue
		}
		// get the addresses
		addresses, err := iface.Addrs()
		if err != nil {
			continue
		}
		// for each address on this interface
		for _, address := range addresses {
			switch v := address.(type) {
			case *net.IPNet:
				if v.IP.To4() != nil {
					ips4 = append(ips4, v.IP.String())
				}
				if strings.Contains(v.IP.String(), ":") {
					ips6 = append(ips6, v.IP.String())
				}
			case *net.IPAddr:
				if v.IP.To4() != nil {
					ips4 = append(ips4, v.IP.String())
				}
				if strings.Contains(v.IP.String(), ":") {
					ips6 = append(ips6, v.IP.String())
				}
			}
		}
	}
	return ips4, ips6, nil
}

// GetLocalIPv4Addresses : get the local IP addresses
func GetLocalIPv4Addresses() ([]string, error) {
	ips4, _, err := GetLocalIPAddresses()
	return ips4, err
}

// GetLocalIPv6Addresses : get the local IP addresses
func GetLocalIPv6Addresses() ([]string, error) {
	_, ips6, err := GetLocalIPAddresses()
	return ips6, err
}
