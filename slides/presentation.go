package slides

import (
	"errors"
	"strings"

	slides "google.golang.org/api/slides/v1"
)

// GetPresentation : gets the presentation object
func GetPresentation(slidesService *slides.Service, presentationId string) (*Presentation, error) {
	if slidesService == nil || presentationId == "" {
		return nil, errors.New("Invalid parameters")
	}
	presentation, err := slidesService.Presentations.Get(presentationId).Do()
	if err != nil {
		return nil, err
	}
	var tmp Presentation
	tmp.ForceSendFields = presentation.ForceSendFields
	tmp.Header = presentation.Header
	tmp.HTTPStatusCode = presentation.HTTPStatusCode
	tmp.Layouts = presentation.Layouts
	tmp.Locale = presentation.Locale
	tmp.Masters = presentation.Masters
	tmp.NotesMaster = presentation.NotesMaster
	tmp.NullFields = presentation.NullFields
	tmp.PageSize = presentation.PageSize
	tmp.PresentationId = presentation.PresentationId
	tmp.RevisionId = presentation.RevisionId
	tmp.ServerResponse = presentation.ServerResponse
	tmp.Slides = presentation.Slides
	tmp.Title = presentation.Title
	// adds
	tmp.Service = slidesService
	return &tmp, nil
}

// GetSlideAtPos : gets the slide at the index specified and returns the overloaded type
func (p *Presentation) GetSlideAtPos(index int) (*Slide, error) {
	if index > len(p.Slides) {
		return nil, errors.New("No slides in presentation")
	}
	mySlide := p.Slides[index]
	var tmp Slide
	tmp.ForceSendFields = mySlide.ForceSendFields
	tmp.Header = mySlide.Header
	tmp.HTTPStatusCode = mySlide.HTTPStatusCode
	tmp.LayoutProperties = mySlide.LayoutProperties
	tmp.MasterProperties = mySlide.MasterProperties
	tmp.NotesProperties = mySlide.NotesProperties
	tmp.NullFields = mySlide.NullFields
	tmp.ObjectId = mySlide.ObjectId
	tmp.PageElements = mySlide.PageElements
	tmp.PageProperties = mySlide.PageProperties
	tmp.PageType = mySlide.PageType
	tmp.RevisionId = mySlide.RevisionId
	tmp.ServerResponse = mySlide.ServerResponse
	tmp.SlideProperties = mySlide.SlideProperties
	// adds
	tmp.Presentation = p
	tmp.Service = p.Service
	return &tmp, nil
}

// GetSlideFromId : gets the slide with the id specified and returns the overloaded type
func (p *Presentation) GetSlideFromId(id string) (*Slide, error) {
	for _, page := range p.Slides {
		if strings.ToLower(page.ObjectId) == strings.ToLower(id) {
			var tmp Slide
			tmp.ForceSendFields = page.ForceSendFields
			tmp.Header = page.Header
			tmp.HTTPStatusCode = page.HTTPStatusCode
			tmp.LayoutProperties = page.LayoutProperties
			tmp.MasterProperties = page.MasterProperties
			tmp.NotesProperties = page.NotesProperties
			tmp.NullFields = page.NullFields
			tmp.ObjectId = page.ObjectId
			tmp.PageElements = page.PageElements
			tmp.PageProperties = page.PageProperties
			tmp.PageType = page.PageType
			tmp.RevisionId = page.RevisionId
			tmp.ServerResponse = page.ServerResponse
			tmp.SlideProperties = page.SlideProperties
			// adds
			tmp.Presentation = p
			tmp.Service = p.Service
			return &tmp, nil
		}
	}
	return nil, errors.New("Failed to found slide from given ID")
}

// GetSlideFromBackgroundColor : get the slide that matches background color
func (p *Presentation) GetSlideFromBackgroundColor(red float64, green float64, blue float64) (*Slide, error) {
	for _, slide := range p.Slides {
		if slide.PageProperties.PageBackgroundFill.SolidFill != nil &&
			slide.PageProperties.PageBackgroundFill.SolidFill.Color.RgbColor != nil {
			if slide.PageProperties.PageBackgroundFill.SolidFill.Color.RgbColor.Blue == blue &&
				slide.PageProperties.PageBackgroundFill.SolidFill.Color.RgbColor.Green == green &&
				slide.PageProperties.PageBackgroundFill.SolidFill.Color.RgbColor.Red == red {
				// create object
				var tmp Slide
				tmp.ForceSendFields = slide.ForceSendFields
				tmp.Header = slide.Header
				tmp.HTTPStatusCode = slide.HTTPStatusCode
				tmp.LayoutProperties = slide.LayoutProperties
				tmp.MasterProperties = slide.MasterProperties
				tmp.NotesProperties = slide.NotesProperties
				tmp.NullFields = slide.NullFields
				tmp.ObjectId = slide.ObjectId
				tmp.PageElements = slide.PageElements
				tmp.PageProperties = slide.PageProperties
				tmp.PageType = slide.PageType
				tmp.RevisionId = slide.RevisionId
				tmp.ServerResponse = slide.ServerResponse
				tmp.SlideProperties = slide.SlideProperties
				// adds
				tmp.Presentation = p
				tmp.Service = p.Service
				return &tmp, nil
			}
		}
	}
	return nil, errors.New("Failed to found slide that matches given colors")
}
