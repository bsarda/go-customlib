package slides

import (
	"errors"
	"regexp"
	"strings"

	vars "customlib/vars"

	slides "google.golang.org/api/slides/v1"
)

// GetSlideElementFromId : get the element from the slide with the id and returns the overloaded type
func (s *Slide) GetSlideElementFromId(id string) (*SlideElement, error) {
	for _, pageElement := range s.PageElements {
		if pageElement.ObjectId == id {
			var tmp SlideElement
			tmp.Description = pageElement.Description
			tmp.ElementGroup = pageElement.ElementGroup
			tmp.ForceSendFields = pageElement.ForceSendFields
			tmp.Image = pageElement.Image
			tmp.Line = pageElement.Line
			tmp.NullFields = pageElement.NullFields
			tmp.ObjectId = pageElement.ObjectId
			tmp.Shape = pageElement.Shape
			tmp.SheetsChart = pageElement.SheetsChart
			tmp.Size = pageElement.Size
			tmp.Table = pageElement.Table
			tmp.Title = pageElement.Title
			tmp.Transform = pageElement.Transform
			tmp.Video = pageElement.Video
			tmp.WordArt = pageElement.WordArt
			// adds
			tmp.Presentation = s.Presentation
			tmp.Service = s.Service
			tmp.Slide = s
			return &tmp, nil
		}
	}
	return nil, errors.New("Failed to get Element from given ID")
}

// SearchShapeContainsText : search a shape within the slide where the text (SINGLE line) is present
func (s *Slide) SearchShapeContainsText(text string) (*SlideElement, error) {
	for _, pageElement := range s.PageElements {
		if pageElement.Shape != nil {
			for i := range pageElement.Shape.Text.TextElements {
				// each textelelement is a line in the shape (textarea, box, cell...)
				if pageElement.Shape.Text.TextElements[i].TextRun != nil &&
					strings.TrimSpace(pageElement.Shape.Text.TextElements[i].TextRun.Content) != "" &&
					strings.Contains(strings.TrimSpace(pageElement.Shape.Text.TextElements[i].TextRun.Content), text) {
					return s.GetSlideElementFromId(pageElement.ObjectId)
				}
			}
		}
	}
	return nil, errors.New("Failed to found shape containing given text")
}

// SearchShapeEqualsText : search a shape within the slide where the text (SINGLE line) is present
func (s *Slide) SearchShapeEqualsText(text string) (*SlideElement, error) {
	for _, pageElement := range s.PageElements {
		if pageElement.Shape != nil {
			for i := range pageElement.Shape.Text.TextElements {
				// each textelelement is a line in the shape (textarea, box, cell...)
				if pageElement.Shape.Text.TextElements[i].TextRun != nil &&
					strings.TrimSpace(pageElement.Shape.Text.TextElements[i].TextRun.Content) != "" &&
					strings.TrimSpace(pageElement.Shape.Text.TextElements[i].TextRun.Content) == text {
					return s.GetSlideElementFromId(pageElement.ObjectId)
				}
			}
		}
	}
	return nil, errors.New("Failed to found shape equals to given text")
}

// SearchTableMatchesTitle : search within the tables of the slide for matching first line
func (s *Slide) SearchTableMatchesTitle(text ...string) (*SlideElement, error) {
	if len(text) < 1 {
		return nil, errors.New("Invalid parameters")
	}
	// create regex
	textConcatWithPipes := ""
	for _, sub := range text {
		textConcatWithPipes += sub + "\\|"
	}
	textConcatWithPipes = textConcatWithPipes[:len(textConcatWithPipes)-2]
	textRegexp, err := regexp.Compile("(?i)" + vars.EscapeForRegex(textConcatWithPipes))
	if err != nil {
		return nil, err
	}
	// get all elements
	for _, element := range s.PageElements {
		if element.Table != nil {
			textOfRow := ""
			for c := range element.Table.TableRows[0].TableCells {
				textOfCell := ""
				for t := range element.Table.TableRows[0].TableCells[c].Text.TextElements {
					if element.Table.TableRows[0].TableCells[c].Text.TextElements[t].TextRun != nil {
						textOfCell += strings.TrimSpace(element.Table.TableRows[0].TableCells[c].Text.TextElements[t].TextRun.Content) + " "
					}
				}
				textOfRow += strings.TrimSpace(textOfCell) + "|"
			}
			textOfRow = textOfRow[:len(textOfRow)-1]
			if textRegexp.MatchString(textOfRow) {
				return s.GetSlideElementFromId(element.ObjectId)
			}
		}
	}
	return nil, errors.New("Failed to find a table with the first row matches the given text")
}

// Duplicate : Duplicates the slide, returns the duplicated slide
func (s *Slide) Duplicate() (*Slide, error) {
	requests := []*slides.Request{{
		DuplicateObject: &slides.DuplicateObjectRequest{
			ObjectId: s.ObjectId,
		},
	}}
	// execute the request
	body := &slides.BatchUpdatePresentationRequest{Requests: requests}
	response, err := s.Service.Presentations.BatchUpdate(s.Presentation.PresentationId, body).Do()
	if err != nil {
		return nil, err
	}
	// renew the presentation item
	presentation, err := GetPresentation(s.Service, s.Presentation.PresentationId)
	if err != nil {
		return nil, err
	}
	// get the slide
	slide, err := presentation.GetSlideFromId(response.Replies[0].DuplicateObject.ObjectId)
	if err != nil {
		return nil, err
	}
	// refresh
	slide.Presentation = presentation
	return slide, nil
}

// Move : Move the slide
func (s *Slide) Move(position int64) error {
	// create array
	var objectsIds []string
	objectsIds = append(objectsIds, s.ObjectId)
	// create request
	requests := []*slides.Request{{
		UpdateSlidesPosition: &slides.UpdateSlidesPositionRequest{
			SlideObjectIds: objectsIds,
			InsertionIndex: position,
		},
	}}
	// execute the request
	body := &slides.BatchUpdatePresentationRequest{Requests: requests}
	_, err := s.Service.Presentations.BatchUpdate(s.Presentation.PresentationId, body).Do()
	if err != nil {
		return err
	}
	return nil
}

// Delete : removes the slide
func (s *Slide) Delete() error {
	requests := []*slides.Request{{
		DeleteObject: &slides.DeleteObjectRequest{
			ObjectId: s.ObjectId,
		},
	}}
	// execute the request
	body := &slides.BatchUpdatePresentationRequest{Requests: requests}
	_, err := s.Service.Presentations.BatchUpdate(s.Presentation.PresentationId, body).Do()
	if err != nil {
		return err
	}
	return nil
}

// RemoveBackgroundColor : removes the background fill of a slide
func (s *Slide) RemoveBackgroundColor() error {
	if s.PageProperties.PageBackgroundFill.SolidFill == nil {
		// already empty
		return nil
	}
	requests := []*slides.Request{{
		UpdatePageProperties: &slides.UpdatePagePropertiesRequest{
			ObjectId: s.ObjectId,
			Fields:   "pageBackgroundFill.solidFill.color",
		},
	}}
	// execute the request
	body := &slides.BatchUpdatePresentationRequest{Requests: requests}
	_, err := s.Service.Presentations.BatchUpdate(s.Presentation.PresentationId, body).Do()
	if err != nil {
		return err
	}
	return nil
}
