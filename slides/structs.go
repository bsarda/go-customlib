package slides

import (
	"google.golang.org/api/googleapi"
	slides "google.golang.org/api/slides/v1"
)

// Presentation : comes from the google slides, only added Service. implements slides.Presentation
type Presentation struct {
	Layouts                  []*slides.Page `json:"layouts,omitempty"`
	Locale                   string         `json:"locale,omitempty"`
	Masters                  []*slides.Page `json:"masters,omitempty"`
	NotesMaster              *slides.Page   `json:"notesMaster,omitempty"`
	PageSize                 *slides.Size   `json:"pageSize,omitempty"`
	PresentationId           string         `json:"presentationId,omitempty"`
	RevisionId               string         `json:"revisionId,omitempty"`
	Slides                   []*slides.Page `json:"slides,omitempty"`
	Title                    string         `json:"title,omitempty"`
	googleapi.ServerResponse `json:"-"`
	ForceSendFields          []string        `json:"-"`
	NullFields               []string        `json:"-"`
	Service                  *slides.Service `json:"-"`
}

// Slide : comes from the google slides, only added Presentation, Service. implements slides.Page
type Slide struct {
	LayoutProperties         *slides.LayoutProperties `json:"layoutProperties,omitempty"`
	MasterProperties         *slides.MasterProperties `json:"masterProperties,omitempty"`
	NotesProperties          *slides.NotesProperties  `json:"notesProperties,omitempty"`
	ObjectId                 string                   `json:"objectId,omitempty"`
	PageElements             []*slides.PageElement    `json:"pageElements,omitempty"`
	PageProperties           *slides.PageProperties   `json:"pageProperties,omitempty"`
	PageType                 string                   `json:"pageType,omitempty"`
	RevisionId               string                   `json:"revisionId,omitempty"`
	SlideProperties          *slides.SlideProperties  `json:"slideProperties,omitempty"`
	googleapi.ServerResponse `json:"-"`
	ForceSendFields          []string        `json:"-"`
	NullFields               []string        `json:"-"`
	Presentation             *Presentation   `json:"-"`
	Service                  *slides.Service `json:"-"`
}

// SlideElement : comes from the google slides, only added Presentation, Service, Slide. implements slides.PageElement
type SlideElement struct {
	Description     string                  `json:"description,omitempty"`
	ElementGroup    *slides.Group           `json:"elementGroup,omitempty"`
	Image           *slides.Image           `json:"image,omitempty"`
	Line            *slides.Line            `json:"line,omitempty"`
	ObjectId        string                  `json:"objectId,omitempty"`
	Shape           *slides.Shape           `json:"shape,omitempty"`
	SheetsChart     *slides.SheetsChart     `json:"sheetsChart,omitempty"`
	Size            *slides.Size            `json:"size,omitempty"`
	Table           *slides.Table           `json:"table,omitempty"`
	Title           string                  `json:"title,omitempty"`
	Transform       *slides.AffineTransform `json:"transform,omitempty"`
	Video           *slides.Video           `json:"video,omitempty"`
	WordArt         *slides.WordArt         `json:"wordArt,omitempty"`
	ForceSendFields []string                `json:"-"`
	NullFields      []string                `json:"-"`
	Presentation    *Presentation           `json:"-"`
	Service         *slides.Service         `json:"-"`
	Slide           *Slide                  `json:"-"`
}

// Cell : comes from the google slides, only added Slide, SlideElement. implements slides.TableCell
type Cell struct {
	ColumnSpan          int64                       `json:"columnSpan,omitempty"`
	Location            *slides.TableCellLocation   `json:"location,omitempty"`
	RowSpan             int64                       `json:"rowSpan,omitempty"`
	TableCellProperties *slides.TableCellProperties `json:"tableCellProperties,omitempty"`
	Text                *slides.TextContent         `json:"text,omitempty"`
	ForceSendFields     []string                    `json:"-"`
	NullFields          []string                    `json:"-"`
	SlideElement        *SlideElement               `json:"-"`
	Slide               *Slide                      `json:"-"`
}
