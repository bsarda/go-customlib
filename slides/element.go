package slides

import (
	"errors"
	"regexp"
	"strings"
	"unicode/utf8"

	slides "google.golang.org/api/slides/v1"
)

// GetCell : get the cell of a table at coordinates
func (s *SlideElement) GetCell(row int, column int) (*Cell, error) {
	if s.Table == nil {
		return nil, errors.New("Element is not a Table")
	}
	if s.Table.TableRows[row] != nil && s.Table.TableRows[row].TableCells[column] != nil {
		var cell Cell
		cell.ColumnSpan = s.Table.TableRows[row].TableCells[column].ColumnSpan
		cell.ForceSendFields = s.Table.TableRows[row].TableCells[column].ForceSendFields
		cell.Location = s.Table.TableRows[row].TableCells[column].Location
		cell.NullFields = s.Table.TableRows[row].TableCells[column].NullFields
		cell.RowSpan = s.Table.TableRows[row].TableCells[column].RowSpan
		cell.TableCellProperties = s.Table.TableRows[row].TableCells[column].TableCellProperties
		cell.Text = s.Table.TableRows[row].TableCells[column].Text
		// addins
		cell.SlideElement = s
		cell.Slide = s.Slide
		return &cell, nil
	}
	return nil, errors.New("Failed to find cell at given coordinates")
}

// SetText : removes and set the text of an element (generally a shape)
func (s *SlideElement) SetText(text string) error {
	requests := []*slides.Request{{
		DeleteText: &slides.DeleteTextRequest{
			ObjectId: s.ObjectId,
			TextRange: &slides.Range{
				Type: "ALL",
			},
		},
	}, {
		InsertText: &slides.InsertTextRequest{
			ObjectId:       s.ObjectId,
			InsertionIndex: 0,
			Text:           text,
		},
	}}
	// execute the request
	body := &slides.BatchUpdatePresentationRequest{Requests: requests}
	_, err := s.Service.Presentations.BatchUpdate(s.Presentation.PresentationId, body).Do()
	if err != nil {
		return err
	}
	return nil
}

// ReplaceText : replaces the text to an element
func (s *SlideElement) ReplaceText(pattern string, text string) error {
	// create regexp
	patternRegexp, err := regexp.Compile(pattern)
	if err != nil {
		return err
	}
	// create vars
	textOfShape := ""
	found := false
	indexStart := 0
	indexEnd := 0
	// options
	var textStyle slides.TextStyle
	var styleFields []string
	// for each
	elements := s.Shape.Text.TextElements
	for t := range elements {
		if elements[t].TextRun != nil {
			if patternRegexp.MatchString(strings.TrimSpace(elements[t].TextRun.Content)) {
				if elements[t].TextRun.Style.Bold {
					styleFields = append(styleFields, "bold")
					textStyle.Bold = true
				}
				if elements[t].TextRun.Style.Italic {
					styleFields = append(styleFields, "italic")
					textStyle.Italic = true
				}
				if elements[t].TextRun.Style.SmallCaps {
					styleFields = append(styleFields, "smallCaps")
					textStyle.SmallCaps = true
				}
				if elements[t].TextRun.Style.Strikethrough {
					styleFields = append(styleFields, "strikethrough")
					textStyle.Strikethrough = true
				}
				if elements[t].TextRun.Style.Underline {
					styleFields = append(styleFields, "underline")
					textStyle.Underline = true
				}
				if elements[t].TextRun.Style.Link != nil {
					styleFields = append(styleFields, "link")
					textStyle.Link = elements[t].TextRun.Style.Link
				}
				if elements[t].TextRun.Style.ForegroundColor != nil {
					styleFields = append(styleFields, "foregroundColor")
					textStyle.ForegroundColor = elements[t].TextRun.Style.ForegroundColor
				}
				if elements[t].TextRun.Style.BackgroundColor != nil {
					styleFields = append(styleFields, "backgroundColor")
					textStyle.BackgroundColor = elements[t].TextRun.Style.BackgroundColor
				}
				if elements[t].TextRun.Style.FontFamily != "" {
					styleFields = append(styleFields, "fontFamily")
					textStyle.FontFamily = elements[t].TextRun.Style.FontFamily
				}
				if elements[t].TextRun.Style.FontSize != nil {
					styleFields = append(styleFields, "fontSize")
					textStyle.FontSize.Magnitude = elements[t].TextRun.Style.FontSize.Magnitude
					textStyle.FontSize.Unit = elements[t].TextRun.Style.FontSize.Unit
				}
				loc := patternRegexp.FindStringIndex(strings.TrimSpace(elements[t].TextRun.Content))
				indexStart = loc[0] + len(textOfShape)
				indexEnd = loc[1] + len(textOfShape)
				found = true
			}
			textOfShape += strings.TrimSpace(elements[t].TextRun.Content) + "\n"
		}
	}
	if found {
		styleFieldsJoined := ""
		if len(styleFields) > 0 {
			styleFieldsJoined = strings.Join(styleFields, ",")
		}
		requests := []*slides.Request{{
			DeleteText: &slides.DeleteTextRequest{
				ObjectId: s.ObjectId,
				TextRange: &slides.Range{
					Type:       "FIXED_RANGE",
					StartIndex: int64(indexStart),
					EndIndex:   int64(indexEnd),
				},
			},
		}, {
			InsertText: &slides.InsertTextRequest{
				ObjectId:       s.ObjectId,
				InsertionIndex: int64(indexStart),
				Text:           text,
			},
		}, {
			UpdateTextStyle: &slides.UpdateTextStyleRequest{
				ObjectId: s.ObjectId,
				TextRange: &slides.Range{
					Type:       "FIXED_RANGE",
					StartIndex: int64(indexStart),
					EndIndex:   int64(indexStart + utf8.RuneCountInString(text)),
				},
				Fields: styleFieldsJoined,
				Style:  &textStyle,
			},
		}}
		// execute the request
		body := &slides.BatchUpdatePresentationRequest{Requests: requests}
		_, err := s.Service.Presentations.BatchUpdate(s.Presentation.PresentationId, body).Do()
		if err != nil {
			return err
		}
		return nil
	}
	return errors.New("Failed to found the text in the shape")
}

// ApplyStyle : replaces the text to an element
func (s *SlideElement) ApplyStyle(indexStart int64, indexEnd int64,
	bold bool, italic bool, smallcaps bool, strikethrough bool, underline bool,
	link *slides.Link, foregroundColor *slides.OptionalColor, backgroundColor *slides.OptionalColor,
	fontFamily string, fontSize *slides.Dimension) error {
	// create
	var textStyle slides.TextStyle
	var textRange slides.Range
	var styleFields []string
	// parse input
	if bold {
		styleFields = append(styleFields, "bold")
		textStyle.Bold = true
	}
	if italic {
		styleFields = append(styleFields, "italic")
		textStyle.Italic = true
	}
	if smallcaps {
		styleFields = append(styleFields, "smallCaps")
		textStyle.SmallCaps = true
	}
	if strikethrough {
		styleFields = append(styleFields, "strikethrough")
		textStyle.Strikethrough = true
	}
	if underline {
		styleFields = append(styleFields, "underline")
		textStyle.Underline = true
	}
	if link != nil {
		styleFields = append(styleFields, "link")
		textStyle.Link = link
	}
	if foregroundColor != nil {
		styleFields = append(styleFields, "foregroundColor")
		textStyle.ForegroundColor = foregroundColor
	}
	if backgroundColor != nil {
		styleFields = append(styleFields, "backgroundColor")
		textStyle.BackgroundColor = backgroundColor
	}
	if fontFamily != "" {
		styleFields = append(styleFields, "fontFamily")
		textStyle.FontFamily = fontFamily
	}
	if fontSize != nil {
		styleFields = append(styleFields, "fontSize")
		textStyle.FontSize = fontSize
	}
	// parse the range
	if indexEnd == 0 {
		textRange.Type = "ALL"
	} else if indexEnd == -1 {
		textRange.Type = "FROM_START_INDEX"
		textRange.StartIndex = indexStart
	} else {
		textRange.Type = "FIXED_RANGE"
		textRange.StartIndex = indexStart
		textRange.EndIndex = indexEnd
	}
	// join the fields
	styleFieldsJoined := ""
	if len(styleFields) > 0 {
		styleFieldsJoined = strings.Join(styleFields, ",")
	}
	// create request item
	requests := []*slides.Request{{
		UpdateTextStyle: &slides.UpdateTextStyleRequest{
			ObjectId:  s.ObjectId,
			TextRange: &textRange,
			Fields:    styleFieldsJoined,
			Style:     &textStyle,
		},
	}}
	// execute the request
	body := &slides.BatchUpdatePresentationRequest{Requests: requests}
	_, err := s.Service.Presentations.BatchUpdate(s.Presentation.PresentationId, body).Do()
	if err != nil {
		return err
	}
	return nil
}

// AppendText : adds the text to an element (generally a shape)
func (s *SlideElement) AppendText(text string, index int64) error {
	requests := []*slides.Request{{
		InsertText: &slides.InsertTextRequest{
			ObjectId:       s.ObjectId,
			InsertionIndex: index,
			Text:           text,
		},
	}}
	// execute the request
	body := &slides.BatchUpdatePresentationRequest{Requests: requests}
	_, err := s.Service.Presentations.BatchUpdate(s.Presentation.PresentationId, body).Do()
	if err != nil {
		return err
	}
	return nil
}

// ClearText : removes all the text of an element (generally a shape)
func (s *SlideElement) ClearText() error {
	requests := []*slides.Request{{
		DeleteText: &slides.DeleteTextRequest{
			ObjectId: s.ObjectId,
			TextRange: &slides.Range{
				Type: "ALL",
			},
		},
	}}
	// execute the request
	body := &slides.BatchUpdatePresentationRequest{Requests: requests}
	_, err := s.Service.Presentations.BatchUpdate(s.Presentation.PresentationId, body).Do()
	if err != nil {
		return err
	}
	return nil
}

// InsertRow : insert a row in a table
func (s *SlideElement) InsertRow(index int, number int64, insertBelow bool) error {
	requests := []*slides.Request{{
		InsertTableRows: &slides.InsertTableRowsRequest{
			TableObjectId: s.ObjectId,
			InsertBelow:   insertBelow,
			CellLocation:  s.Table.TableRows[index].TableCells[0].Location,
			Number:        number,
		},
	}}
	// execute the request
	body := &slides.BatchUpdatePresentationRequest{Requests: requests}
	_, err := s.Service.Presentations.BatchUpdate(s.Presentation.PresentationId, body).Do()
	if err != nil {
		return err
	}
	return nil
}

// InsertColumn : insert a column in a table
func (s *SlideElement) InsertColumn(index int, number int64, insertBelow bool) error {
	requests := []*slides.Request{{
		InsertTableRows: &slides.InsertTableRowsRequest{
			TableObjectId: s.ObjectId,
			InsertBelow:   insertBelow,
			CellLocation:  s.Table.TableRows[0].TableCells[index].Location,
			Number:        number,
		},
	}}
	// execute the request
	body := &slides.BatchUpdatePresentationRequest{Requests: requests}
	_, err := s.Service.Presentations.BatchUpdate(s.Presentation.PresentationId, body).Do()
	if err != nil {
		return err
	}
	return nil
}

// Delete : removes the slide element
func (s *SlideElement) Delete() error {
	requests := []*slides.Request{{
		DeleteObject: &slides.DeleteObjectRequest{
			ObjectId: s.ObjectId,
		},
	}}
	// execute the request
	body := &slides.BatchUpdatePresentationRequest{Requests: requests}
	_, err := s.Service.Presentations.BatchUpdate(s.Presentation.PresentationId, body).Do()
	if err != nil {
		return err
	}
	return nil
}
