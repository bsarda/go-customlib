package slides

import (
	"errors"
	"regexp"
	"strings"
	"unicode/utf8"

	slides "google.golang.org/api/slides/v1"
)

// SetText : removes and set the text of an element (generally a shape)
func (c *Cell) SetText(text string) error {
	requests := []*slides.Request{{
		DeleteText: &slides.DeleteTextRequest{
			ObjectId:     c.SlideElement.ObjectId,
			CellLocation: c.Location,
			TextRange: &slides.Range{
				Type: "ALL",
			},
		},
	}, {
		InsertText: &slides.InsertTextRequest{
			ObjectId:       c.SlideElement.ObjectId,
			CellLocation:   c.Location,
			InsertionIndex: 0,
			Text:           text,
		},
	}}
	// execute the request
	body := &slides.BatchUpdatePresentationRequest{Requests: requests}
	_, err := c.Slide.Service.Presentations.BatchUpdate(c.Slide.Presentation.PresentationId, body).Do()
	if err != nil {
		return err
	}
	return nil
}

// ReplaceText : replaces the text to an element
func (c *Cell) ReplaceText(pattern string, text string) error {
	// create regexp
	patternRegexp, err := regexp.Compile(pattern)
	if err != nil {
		return err
	}
	// create vars
	textOfShape := ""
	found := false
	indexStart := 0
	indexEnd := 0
	// options
	var textStyle slides.TextStyle
	var styleFields []string
	// for each
	elements := c.Text.TextElements
	for t := range elements {
		if elements[t].TextRun != nil {
			if patternRegexp.MatchString(strings.TrimSpace(elements[t].TextRun.Content)) {
				if elements[t].TextRun.Style.Bold {
					styleFields = append(styleFields, "bold")
					textStyle.Bold = true
				}
				if elements[t].TextRun.Style.Italic {
					styleFields = append(styleFields, "italic")
					textStyle.Italic = true
				}
				if elements[t].TextRun.Style.SmallCaps {
					styleFields = append(styleFields, "smallCaps")
					textStyle.SmallCaps = true
				}
				if elements[t].TextRun.Style.Strikethrough {
					styleFields = append(styleFields, "strikethrough")
					textStyle.Strikethrough = true
				}
				if elements[t].TextRun.Style.Underline {
					styleFields = append(styleFields, "underline")
					textStyle.Underline = true
				}
				if elements[t].TextRun.Style.Link != nil {
					styleFields = append(styleFields, "link")
					textStyle.Link = elements[t].TextRun.Style.Link
				}
				if elements[t].TextRun.Style.ForegroundColor != nil {
					styleFields = append(styleFields, "foregroundColor")
					textStyle.ForegroundColor = elements[t].TextRun.Style.ForegroundColor
				}
				if elements[t].TextRun.Style.BackgroundColor != nil {
					styleFields = append(styleFields, "backgroundColor")
					textStyle.BackgroundColor = elements[t].TextRun.Style.BackgroundColor
				}
				if elements[t].TextRun.Style.FontFamily != "" {
					styleFields = append(styleFields, "fontFamily")
					textStyle.FontFamily = elements[t].TextRun.Style.FontFamily
				}
				if elements[t].TextRun.Style.FontSize != nil {
					styleFields = append(styleFields, "fontSize")
					textStyle.FontSize.Magnitude = elements[t].TextRun.Style.FontSize.Magnitude
					textStyle.FontSize.Unit = elements[t].TextRun.Style.FontSize.Unit
				}
				loc := patternRegexp.FindStringIndex(strings.TrimSpace(elements[t].TextRun.Content))
				indexStart = loc[0] + len(textOfShape)
				indexEnd = loc[1] + len(textOfShape)
				found = true
			}
			textOfShape += strings.TrimSpace(elements[t].TextRun.Content) + "\n"
		}
	}
	if found {
		styleFieldsJoined := ""
		if len(styleFields) > 0 {
			styleFieldsJoined = strings.Join(styleFields, ",")
		}
		requests := []*slides.Request{{
			DeleteText: &slides.DeleteTextRequest{
				ObjectId:     c.SlideElement.ObjectId,
				CellLocation: c.Location,
				TextRange: &slides.Range{
					Type:       "FIXED_RANGE",
					StartIndex: int64(indexStart),
					EndIndex:   int64(indexEnd),
				},
			},
		}, {
			InsertText: &slides.InsertTextRequest{
				ObjectId:       c.SlideElement.ObjectId,
				CellLocation:   c.Location,
				InsertionIndex: int64(indexStart),
				Text:           text,
			},
		}, {
			UpdateTextStyle: &slides.UpdateTextStyleRequest{
				ObjectId:     c.SlideElement.ObjectId,
				CellLocation: c.Location,
				TextRange: &slides.Range{
					Type:       "FIXED_RANGE",
					StartIndex: int64(indexStart),
					EndIndex:   int64(indexStart + utf8.RuneCountInString(text)),
				},
				Fields: styleFieldsJoined,
				Style:  &textStyle,
			},
		}}
		// execute the request
		body := &slides.BatchUpdatePresentationRequest{Requests: requests}
		_, err := c.Slide.Service.Presentations.BatchUpdate(c.Slide.Presentation.PresentationId, body).Do()
		if err != nil {
			return err
		}
		return nil
	}
	return errors.New("Failed to found the text in the shape")
}

// ApplyStyle : replaces the text to an element
func (c *Cell) ApplyStyle(indexStart int64, indexEnd int64,
	bold bool, italic bool, smallcaps bool, strikethrough bool, underline bool,
	link *slides.Link, foregroundColor *slides.OptionalColor, backgroundColor *slides.OptionalColor,
	fontFamily string, fontSize *slides.Dimension) error {
	// create
	var textStyle slides.TextStyle
	var textRange slides.Range
	var styleFields []string
	// parse input
	if bold {
		styleFields = append(styleFields, "bold")
		textStyle.Bold = true
	}
	if italic {
		styleFields = append(styleFields, "italic")
		textStyle.Italic = true
	}
	if smallcaps {
		styleFields = append(styleFields, "smallCaps")
		textStyle.SmallCaps = true
	}
	if strikethrough {
		styleFields = append(styleFields, "strikethrough")
		textStyle.Strikethrough = true
	}
	if underline {
		styleFields = append(styleFields, "underline")
		textStyle.Underline = true
	}
	if link != nil {
		styleFields = append(styleFields, "link")
		textStyle.Link = link
	}
	if foregroundColor != nil {
		styleFields = append(styleFields, "foregroundColor")
		textStyle.ForegroundColor = foregroundColor
	}
	if backgroundColor != nil {
		styleFields = append(styleFields, "backgroundColor")
		textStyle.BackgroundColor = backgroundColor
	}
	if fontFamily != "" {
		styleFields = append(styleFields, "fontFamily")
		textStyle.FontFamily = fontFamily
	}
	if fontSize != nil {
		styleFields = append(styleFields, "fontSize")
		textStyle.FontSize = fontSize
	}
	// parse the range
	if indexEnd == 0 {
		textRange.Type = "ALL"
	} else if indexEnd == -1 {
		textRange.Type = "FROM_START_INDEX"
		textRange.StartIndex = indexStart
	} else {
		textRange.Type = "FIXED_RANGE"
		textRange.StartIndex = indexStart
		textRange.EndIndex = indexEnd
	}
	// join the fields
	styleFieldsJoined := ""
	if len(styleFields) > 0 {
		styleFieldsJoined = strings.Join(styleFields, ",")
	}
	// create request item
	requests := []*slides.Request{{
		UpdateTextStyle: &slides.UpdateTextStyleRequest{
			ObjectId:     c.SlideElement.ObjectId,
			CellLocation: c.Location,
			TextRange:    &textRange,
			Fields:       styleFieldsJoined,
			Style:        &textStyle,
		},
	}}
	// execute the request
	body := &slides.BatchUpdatePresentationRequest{Requests: requests}
	_, err := c.Slide.Service.Presentations.BatchUpdate(c.Slide.Presentation.PresentationId, body).Do()
	if err != nil {
		return err
	}
	return nil
}

// AppendText : adds the text to an element (generally a shape)
func (c *Cell) AppendText(text string, index int64) error {
	requests := []*slides.Request{{
		InsertText: &slides.InsertTextRequest{
			ObjectId:       c.SlideElement.ObjectId,
			CellLocation:   c.Location,
			InsertionIndex: index,
			Text:           text,
		},
	}}
	// execute the request
	body := &slides.BatchUpdatePresentationRequest{Requests: requests}
	_, err := c.Slide.Service.Presentations.BatchUpdate(c.Slide.Presentation.PresentationId, body).Do()
	if err != nil {
		return err
	}
	return nil
}

// ClearText : removes all the text of an element (generally a shape)
func (c *Cell) ClearText() error {
	requests := []*slides.Request{{
		DeleteText: &slides.DeleteTextRequest{
			ObjectId:     c.SlideElement.ObjectId,
			CellLocation: c.Location,
			TextRange: &slides.Range{
				Type: "ALL",
			},
		},
	}}
	// execute the request
	body := &slides.BatchUpdatePresentationRequest{Requests: requests}
	_, err := c.Slide.Service.Presentations.BatchUpdate(c.Slide.Presentation.PresentationId, body).Do()
	if err != nil {
		return err
	}
	return nil
}

// InsertRow : insert a row in a table
func (c *Cell) InsertRow(number int64, insertBelow bool) error {
	requests := []*slides.Request{{
		InsertTableRows: &slides.InsertTableRowsRequest{
			TableObjectId: c.SlideElement.ObjectId,
			InsertBelow:   insertBelow,
			CellLocation:  c.Location,
			Number:        number,
		},
	}}
	// execute the request
	body := &slides.BatchUpdatePresentationRequest{Requests: requests}
	_, err := c.Slide.Service.Presentations.BatchUpdate(c.Slide.Presentation.PresentationId, body).Do()
	if err != nil {
		return err
	}
	return nil
}

// InsertColumn : insert a column in a table
func (c *Cell) InsertColumn(number int64, insertBelow bool) error {
	requests := []*slides.Request{{
		InsertTableRows: &slides.InsertTableRowsRequest{
			TableObjectId: c.SlideElement.ObjectId,
			InsertBelow:   insertBelow,
			CellLocation:  c.Location,
			Number:        number,
		},
	}}
	// execute the request
	body := &slides.BatchUpdatePresentationRequest{Requests: requests}
	_, err := c.Slide.Service.Presentations.BatchUpdate(c.Slide.Presentation.PresentationId, body).Do()
	if err != nil {
		return err
	}
	return nil
}
