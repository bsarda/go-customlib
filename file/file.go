package file

import (
	"bufio"
	"errors"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

// =============================================================================
// ========================== struct for further ops ==========================

// Config : is what is read from the configuration file
type Config map[string]string

// =============================================================================
// ======================== basic operations and struct ========================

// ReadConfig reads from config file
func ReadConfig(filename string) (Config, error) {
	// init the config with blank
	config := Config{}
	// if nothing, error
	if len(filename) == 0 {
		return nil, errors.New("No file given")
	}
	// open the file
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	// close file after the function
	defer file.Close()
	// read from file
	reader := bufio.NewReader(file)
	// for each line in the file
	for {
		line, err := reader.ReadString('\n')
		if indexOfSeparator := strings.IndexAny(line, "=:"); indexOfSeparator >= 0 {
			if key := strings.TrimSpace(line[:indexOfSeparator]); len(key) > 0 {
				value := ""
				if len(line) > indexOfSeparator {
					value = strings.TrimSpace(line[indexOfSeparator+1:])
				}
				// assign the config map
				config[key] = value
			}
		}
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}
	}
	return config, nil
}

// FileOrPathExists returns true if path/file exists
func FileOrPathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}

// IsPathExists returns true if path exists
func IsPathExists(path string) (bool, error) {
	pa, err := os.Stat(path)
	if err != nil {
		return false, err
	}
	return pa.Mode().IsDir(), nil
}

// IsFileExists returns true if file exists
func IsFileExists(path string) (bool, error) {
	pa, err := os.Stat(path)
	if err != nil {
		return false, err
	}
	return pa.Mode().IsRegular(), nil
}

// ListFiles list the files and the folders in a path
func ListFiles(path string, namesOnly bool) ([]string, error) {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return nil, err
	}
	var fileslist []string
	for _, file := range files {
		if !file.IsDir() {
			if namesOnly {
				fileslist = append(fileslist, file.Name())
			} else {
				fileslist = append(fileslist, path+"/"+file.Name())
			}
		}
	}
	return fileslist, nil
}

// ListSubfolders list the files and the folders in a path
func ListSubfolders(path string, namesOnly bool) ([]string, error) {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return nil, err
	}
	var subfolders []string
	for _, file := range files {
		if file.IsDir() {
			if namesOnly {
				subfolders = append(subfolders, file.Name())
			} else {
				subfolders = append(subfolders, path+"/"+file.Name())
			}
		}
	}
	// return files, nil
	return subfolders, nil
}

// ListFilesAndSubfolders list the files and the folders in a path
func ListFilesAndSubfolders(path string, namesOnly bool) ([]string, []string, error) {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return nil, nil, err
	}
	var fileslist []string
	var subfolders []string
	for _, file := range files {
		if file.IsDir() {
			if namesOnly {
				subfolders = append(subfolders, file.Name())
			} else {
				subfolders = append(subfolders, path+"/"+file.Name())
			}
		} else {
			if namesOnly {
				fileslist = append(fileslist, file.Name())
			} else {
				fileslist = append(fileslist, path+"/"+file.Name())
			}
		}
	}
	// return files, nil
	return fileslist, subfolders, nil
}

// ListFilesInSubfolders list the files and the folders in a path - but with walk and not ioutil
func ListFilesInSubfolders(path string, namesOnly bool, filters []string) ([]string, error) {
	// init the map from filters if there is
	// filter := make(map[string]struct{}, len(filters))
	// for _, f := range filters {
	// 	filter[f] = struct{}{}
	// }
	// filter := make(map[string]interface{}, len(filters))
	// for _, f := range filters {
	// 	filter[f] = nil
	// }
	// if len(filters) < 1 {
	// 	set := nil
	// } else {
	// 	set := make(map[string]struct{}, len(slice))
	// }
	// init the return variable
	var files []string
	err := filepath.Walk(path, _listFilesInSubfolders(&files, &filters))
	if err != nil {
		return nil, err
	}
	return files, nil
}
func _listFilesInSubfolders(files *[]string, filters *[]string) filepath.WalkFunc {
	return func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}
		for _, filter := range *filters {
			if strings.Contains(path, filter) {
				return nil
			}
		}
		if err != nil {
			return err
		}
		*files = append(*files, path)
		return nil
	}
}

// RemoveFolderContents removes the content from a folder
func RemoveFolderContents(path string) error {
	d, err := os.Open(path)
	if err != nil {
		return err
	}
	defer d.Close()
	names, err := d.Readdirnames(-1)
	if err != nil {
		return err
	}
	for _, name := range names {
		err = os.RemoveAll(filepath.Join(path, name))
		if err != nil {
			return err
		}
	}
	return nil
}

// RemoveFolder removes a non-empty folder
func RemoveFolder(path string) error {
	RemoveFolderContents(path)
	return os.Remove(path)
}

// WriteToFile writes the data array of string to file
func WriteToFile(path string, data []string) error {
	f, err := os.Create(path)
	if err != nil {
		return err
	}
	defer f.Close()
	for _, line := range data {
		_, err := f.WriteString(line + "\n")
		if err != nil {
			return err
		}
	}
	f.Close()
	return nil
}
