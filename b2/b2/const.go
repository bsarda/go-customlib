package b2

// constants
const (
	B2apiEndpoint         = "https://api.backblazeb2.com/b2api/v2/"
	B2apiAuthorizeAccount = "https://api.backblazeb2.com/b2api/v2/b2_authorize_account"
	AllowInsecureFlag     = true
)
