package main

import (
	"bufio"
	"customlib"
	backblazeB2 "customlib/b2/b2"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"sync"
)

// backblazeB2 "customlib/b2/b2"

// B2 for the b2 client
type B2 struct {
	mutex              sync.Mutex
	IsValid            bool
	AccountID          string `json:"accountId"`
	APIEndpoint        string `json:"apiUrl"`
	AuthorizationToken string `json:"authorizationToken"`
	DownloadURL        string `json:"downloadUrl"`
	// Host               string
	// Auth               *b2Auth
	// Credentials
	// NoRetry bool
	// MaxIdleUploads int
	// httpClient http.Client
}

// RequestType defines the security setting for a bucket
type RequestType string

// Types of requests
const (
	Get    RequestType = "GET"
	Post   RequestType = "POST"
	Patch  RequestType = "PATCH"
	Put    RequestType = "PUT"
	Delete RequestType = "DELETE"
)

// BucketType defines the security setting for a bucket
type BucketType string

// Types of bucket
const (
	AllPublic  BucketType = "allPublic"
	AllPrivate BucketType = "allPrivate"
	Snapshot   BucketType = "snapshot"
)

// LifecycleRule instructs the B2 service to automatically hide and/or delete old files.
type LifecycleRule struct {
	DaysFromUploadingToHiding int    `json:"daysFromUploadingToHiding"`
	DaysFromHidingToDeleting  int    `json:"daysFromHidingToDeleting"`
	FileNamePrefix            string `json:"fileNamePrefix"`
}

// Bucket is the bucket representation
type Bucket struct {
	AccountID      string            `json:"accountId"`
	ID             string            `json:"bucketId"`
	Info           map[string]string `json:"bucketInfo"` // User-defined information to be stored with the bucket.
	Name           string            `json:"bucketName"` // The name to give the new bucket, 6-50 chars, unique, alphanum plus "-" and not starting with "b2-"
	BucketType     BucketType        `json:"bucketType"`
	Revision       int               `json:"revision"`
	LifecycleRules []LifecycleRule   `json:"lifecycleRules"` // The initial list of lifecycle rules for this bucket.
}

// ,omitempty
// json:"-"

// GetToken is used to retrieve token
func GetToken() {
	// source https: //www.backblaze.com/b2/docs/b2_authorize_account.html
}

// Client creates a new b2 client
func Client(applicationKeyID string, applicationKey string) (*B2, error) {
	// create empty for locking ?
	//
	//
	//
	// HAVE TO CHECK !!
	//
	//
	//
	var bt B2
	bt.mutex.Lock()
	defer bt.mutex.Unlock()
	// get the auth through api
	return apiAuth(applicationKeyID, applicationKey)
	/*
	   {
	     "absoluteMinimumPartSize": 5000000,
	     "accountId": "ba516092d603",
	     "allowed": {
	       "bucketId": null,
	       "bucketName": null,
	       "capabilities": [
	         "listKeys",
	         "writeKeys",
	         "deleteKeys",
	         "listBuckets",
	         "writeBuckets",
	         "deleteBuckets",
	         "listFiles",
	         "readFiles",
	         "shareFiles",
	         "writeFiles",
	         "deleteFiles"
	       ],
	       "namePrefix": null
	     },
	     "apiUrl": "https://api002.backblazeb2.com",
	     "authorizationToken": "4_002ba516092d6030000000001_018ccd92_7833f9_acct_Pj_PEtlaApkRl6PrYS2i3ldbEII=",
	     "downloadUrl": "https://f002.backblazeb2.com",
	     "recommendedPartSize": 100000000
	   }
	*/
}

// ListBuckets list the buckets
func (b2 *B2) ListBuckets() ([]*Bucket, error) {

	// if err := b.apiRequest("b2_list_buckets", request, response); err != nil {
	// 	return nil, err
	// }

	// creates from output
	// buckets := make([]*Bucket, len(response.Buckets))
	//
	//
	//
	//
	// create empty
	return nil, nil
}

//
//
//
//
// func FromJson(jsonSrc string) interface{} {
//     var obj person{}
//     json.Unmarshal([]byte(jsonSrc), &obj)

//     return obj
// }
//
//
//
//

func apiAuth(applicationKeyID string, applicationKey string) (*B2, error) {
	// in case of login, this specifies the headers
	headers := make(map[string]string)
	headers["Authorization"] = "Basic " + base64.StdEncoding.EncodeToString([]byte(applicationKeyID+":"+applicationKey))
	// send the request
	responseCode, _, _, responseBody := customlib.SendRESTRequest(
		"GET",
		backblazeB2.B2apiAuthorizeAccount,
		nil,
		headers,
		nil,
		backblazeB2.AllowInsecureFlag /* allow insecure?*/)
	// if not correct response
	if responseCode != 200 {
		return nil, errors.New("failed the request, responseCode=" +
			strconv.Itoa(responseCode) + "\nresponseBody=" + string(responseBody))
	}
	// convert
	var b2 B2
	err := json.Unmarshal(responseBody, &b2)
	if err != nil {
		return nil, err
	}
	b2.IsValid = true
	return &b2, nil
}

// Perform a B2 API request with the provided request and response objects
func (b2 *B2) apiRequest(path string, verb string, body []byte, response interface{}) error {
	// don't lock
	// b2.mutex.Lock()
	// defer b2.mutex.Unlock()
	if !b2.IsValid {
		return errors.New("client no longer valid, or not initialized")
	}
	// normal case
	// create the headers with that
	headers := make(map[string]string)
	// headers["Content-Type"] = "application/json"  // in case of
	headers["Authorization"] = b2.AuthorizationToken
	responseCode, _, _, responseBody := customlib.SendRESTRequest(
		verb,
		filepath.Clean(b2.APIEndpoint)+"/"+path, // the uri
		body,
		headers,
		nil,
		true /* allow insecure?*/)
	// parse it
	if responseCode == 401 {
		b2.IsValid = false
		return errors.New("No longer authorized, or don't have the permissions to do the call")
	}
	if responseCode != 200 && responseCode != 200 {
		return errors.New("Unknown error, responseCode=" +
			strconv.Itoa(responseCode) + "\nresponseBody=" + string(responseBody))
	}
	// var result interface{}
	return json.Unmarshal(responseBody, response)
}

func (b2 *B2) apiGet(path string, response interface{}) error {
	// return b2.apiRequest(path, "GET", nil, "", "", response)
	return nil
}
func (b2 *B2) apiPost(path string, body []byte, response interface{}) error {
	// return b2.apiRequest(path, "GET", nil, "", "", response)
	return nil
}

// Attempts to parse a response body into the provided result struct
func (b2 *B2) parseResponse(code int, body []byte /*, result *interface{}*/) (interface{}, error) {
	switch code {
	case 200: // Response is OK
	case 201:
		break
	case 401:
		b2.IsValid = false
		return nil, errors.New("No longer authorized, or don't have the permissions to do the call")
	default:
		return nil, errors.New("Unknown error, responseCode=" +
			strconv.Itoa(code) + "\nresponseBody=" + string(body))
	}
	var result interface{}
	err := json.Unmarshal(body, result)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func main() {
	fmt.Println("init")

	// read the key
	applicationKeyID := ""
	applicationKey := ""
	file, err := os.Open("/home/bsarda/github/notes/credentials-access/backblaze-b2-appkey.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	// reading lines
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		strKey := regexp.MustCompile(`^key: .*`).FindAllString(scanner.Text(), 1)
		strKeyID := regexp.MustCompile(`^keyId: .*`).FindAllString(scanner.Text(), 1)
		if len(strKey) > 0 {
			applicationKey = strings.Replace(scanner.Text(), "key: ", "", -1)
		}
		if len(strKeyID) > 0 {
			applicationKeyID = strings.Replace(scanner.Text(), "keyId: ", "", -1)
		}
	}

	// print it
	if applicationKey == "" || applicationKeyID == "" {
		panic(5)
	}

	fmt.Println(applicationKey)
	fmt.Println(applicationKeyID)
	// use it
	// b2, err := b2client("002ba516092d6030000000001", applicationKey)
	// if err != nil {
	// 	panic(10)
	// }
	// fmt.Println(b2.AuthorizationToken)
}
