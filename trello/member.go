package trello

// documentation:
// https://trello.readme.io/reference#member

import (
	"encoding/json"
)

// Member : struct for Member type that describes a member.
type Member struct {
	client     *Client
	ID         string `json:"id"`
	AvatarHash string `json:"avatarHash"`
	Bio        string `json:"bio"`
	BioData    struct {
		Emoji interface{} `json:"emoji,omitempty"`
	} `json:"bioData"`
	Confirmed                bool     `json:"confirmed"`
	FullName                 string   `json:"fullName"`
	IDPremOrgsAdmin          []string `json:"idPremOrgsAdmin"`
	Initials                 string   `json:"initials"`
	MemberType               string   `json:"memberType"`
	Products                 []int    `json:"products"`
	Status                   string   `json:"status"`
	URL                      string   `json:"url"`
	Username                 string   `json:"username"`
	AvatarSource             string   `json:"avatarSource"`
	Email                    string   `json:"email"`
	GravatarHash             string   `json:"gravatarHash"`
	IDBoards                 []string `json:"idBoards"`
	IDBoardsPinned           []string `json:"idBoardsPinned"`
	IDOrganizations          []string `json:"idOrganizations"`
	LoginTypes               []string `json:"loginTypes"`
	NewEmail                 string   `json:"newEmail"`
	OneTimeMessagesDismissed []string `json:"oneTimeMessagesDismissed"`
	Prefs                    struct {
		SendSummaries                 bool   `json:"sendSummaries"`
		MinutesBetweenSummaries       int    `json:"minutesBetweenSummaries"`
		MinutesBeforeDeadlineToNotify int    `json:"minutesBeforeDeadlineToNotify"`
		ColorBlind                    bool   `json:"colorBlind"`
		Locale                        string `json:"locale"`
	} `json:"prefs"`
	Trophies           []string `json:"trophies"`
	UploadedAvatarHash string   `json:"uploadedAvatarHash"`
	PremiumFeatures    []string `json:"premiumFeatures"`
}

// Member : get a member object from the member name and client
func (c *Client) Member(name string) (*Member, error) {
	_, body, err := c.get("members/" + name)
	if err != nil {
		return nil, err
	}
	var member Member
	err = json.Unmarshal(body, &member)
	if err != nil {
		return nil, err
	}
	// add client for further calls
	member.client = c
	return &member, nil
}

// Boards : get the boards of a specified member, filter with fields if needed
func (m *Member) Boards() ([]Board, error) {
	// send get
	_, body, err := m.client.get("members/" + m.ID + "/boards")
	if err != nil {
		return nil, err
	}
	var boards []Board
	err = json.Unmarshal(body, &boards)
	if err != nil {
		return nil, err
	}
	// add client for further calls
	for i := range boards {
		boards[i].client = m.client
	}
	return boards, nil
}

// TODO :
// GET savedSearches
// GET notifications
// and with that, the .go file with Search struct
