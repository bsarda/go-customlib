package trello

// documentation:
// https://trello.readme.io/reference#boards-2

import (
	"encoding/json"
)

// Board : it's the main struct that describes a board. members not included.
type Board struct {
	client   *Client
	ID       string `json:"id"`
	Name     string `json:"name"`
	Desc     string `json:"desc"`
	DescData struct {
		Emoji struct{} `json:"emoji"`
	} `json:"descData"`
	Closed         bool   `json:"closed"`
	IDOrganization string `json:"idOrganization"`
	Pinned         bool   `json:"pinned"`
	URL            string `json:"url"`
	ShortURL       string `json:"shortUrl"`
	Prefs          struct {
		PermissionLevel       string            `json:"permissionLevel"`
		Voting                string            `json:"voting"`
		Comments              string            `json:"comments"`
		Invitations           string            `json:"invitations"`
		SelfJoin              bool              `json:"selfjoin"`
		CardCovers            bool              `json:"cardCovers"`
		CardAging             string            `json:"cardAging"`
		CalendarFeedEnabled   bool              `json:"calendarFeedEnabled"`
		Background            string            `json:"background"`
		BackgroundColor       string            `json:"backgroundColor"`
		BackgroundImage       string            `json:"backgroundImage"`
		BackgroundImageScaled []BoardBackground `json:"backgroundImageScaled"`
		BackgroundTile        bool              `json:"backgroundTile"`
		BackgroundBrightness  string            `json:"backgroundBrightness"`
		CanBePublic           bool              `json:"canBePublic"`
		CanBeOrg              bool              `json:"canBeOrg"`
		CanBePrivate          bool              `json:"canBePrivate"`
		CanInvite             bool              `json:"canInvite"`
	} `json:"prefs"`
	LabelNames struct {
		Red    string `json:"red"`
		Orange string `json:"orange"`
		Yellow string `json:"yellow"`
		Green  string `json:"green"`
		Blue   string `json:"blue"`
		Purple string `json:"purple"`
	} `json:"labelNames"`
	Starred     bool           `json:"starred"`
	Memberships []BoardMembers `json:"memberships"`
}

// BoardMembers : describe the members of the board (not members objects!)
type BoardMembers struct {
	ID          string `json:"id"`
	IDMember    string `json:"idMember"`
	MemberType  string `json:"memberType"`
	Unconfirmed bool   `json:"unconfirmed"`
	Deactivated bool   `json:"deactivated"`
	// OrgMemberType string `json:"orgMemberType"`
	// Member        struct {
	// 	ID       string `json:"id"`
	// 	FullName string `json:"fullName"`
	// } `json:"member"`
}

// BoardBackground : describe the background image, w/h of a board
type BoardBackground struct {
	Width  int    `json:"width"`
	Height int    `json:"height"`
	URL    string `json:"url"`
}

// Boards : get all the boards available for the trello client
func (c *Client) Boards() ([]Board, error) {
	// get boards
	_, body, err := c.get("boards/")
	if err != nil {
		return nil, err
	}
	// json parse
	var boards []Board
	err = json.Unmarshal(body, &boards)
	if err != nil {
		return nil, err
	}
	// add client for further calls
	for i := range boards {
		boards[i].client = c
	}
	return boards, nil
}

// Board : get the detail of a board by it's Id
func (c *Client) Board(id string) (*Board, error) {
	_, body, err := c.get("boards/" + id)
	if err != nil {
		return nil, err
	}
	var board Board
	err = json.Unmarshal(body, &board)
	if err != nil {
		return nil, err
	}
	// add client for further calls
	board.client = c
	return &board, nil
}

// Lists : get the lists that are in a board
func (b *Board) Lists() ([]List, error) {
	_, body, err := b.client.get("boards/" + b.ID + "/lists")
	if err != nil {
		return nil, err
	}
	var lists []List
	err = json.Unmarshal(body, &lists)
	if err != nil {
		return nil, err
	}
	for i := range lists {
		lists[i].client = b.client
	}
	return lists, nil
}

// Members : get the members enrolled on a board
func (b *Board) Members() ([]Member, error) {
	_, body, err := b.client.get("boards/" + b.ID + "/members")
	if err != nil {
		return nil, err
	}
	var members []Member
	err = json.Unmarshal(body, &members)
	if err != nil {
		return nil, err
	}
	for i := range members {
		members[i].client = b.client
	}
	return members, nil
}

// Labels : get the Labels available for a board
func (b *Board) Labels() ([]Label, error) {
	_, body, err := b.client.get("boards/" + b.ID + "/labels")
	if err != nil {
		return nil, err
	}
	var labels []Label
	err = json.Unmarshal(body, &labels)
	if err != nil {
		return nil, err
	}
	for i := range labels {
		labels[i].client = b.client
	}
	return labels, nil
}

// TODO:
// checklists() + checklist(id)
// actions() + action(id)
// cards() + card(id)

// func (b *Board) MemberCards(IdMember string) (cards []Card, err error) {
// 	body, err := b.client.Get("/boards/" + b.ID + "/members/" + IdMember + "/cards")
// 	if err != nil {
// 		return
// 	}
//
// 	err = json.Unmarshal(body, &cards)
// 	for i := range cards {
// 		cards[i].client = b.client
// 	}
// 	return
// }
