package trello

// documentation:
// https://trello.readme.io/reference#cards-1

import (
	"encoding/json"
)

// Card : struct for Card type that describes a Card in trello.
type Card struct {
	client *Client
	ID     string `json:"id"`
	Badges struct {
		Votes              int    `json:"votes"`
		ViewingMemberVoted bool   `json:"viewingMemberVoted"`
		Subscribed         bool   `json:"subscribed"`
		Fogbugz            string `json:"fogbugz"`
		CheckItems         int    `json:"checkItems"`
		CheckItemsChecked  int    `json:"checkItemsChecked"`
		Comments           int    `json:"comments"`
		Attachments        int    `json:"attachments"`
		Description        bool   `json:"description"`
		Due                string `json:"due"`
		DueComplete        bool   `json:"dueComplete"`
	} `json:"badges"`
	CheckItemStates []struct {
		IDCheckItem string `json:"idCheckItem"`
		State       string `json:"state"`
	} `json:"checkItemStates"`
	Closed           bool   `json:"closed"`
	DateLastActivity string `json:"dateLastActivity"`
	Desc             string `json:"desc"`
	DescData         struct {
		Emoji struct{} `json:"emoji"`
	} `json:"descData"`
	Due                   string   `json:"due"`
	DueComplete           bool     `json:"dueComplete"`
	Email                 string   `json:"email"`
	IDAttachmentCover     string   `json:"idAttachmentCover"`
	IDBoard               string   `json:"idBoard"`
	IDCheckLists          []string `json:"idCheckLists"`
	IDLabels              []string `json:"idLabels"`
	IDList                string   `json:"idList"`
	IDMembers             []string `json:"idMembers"`
	IDMembersVoted        []string `json:"idMembersVoted"`
	IDShort               int      `json:"idShort"`
	Labels                []Label  `json:"labels"`
	ManualCoverAttachment bool     `json:"manualCoverAttachment"`
	Name                  string   `json:"name"`
	Pos                   float64  `json:"pos"`
	ShortLink             string   `json:"shortLink"`
	ShortURL              string   `json:"shortUrl"`
	Subscribed            bool     `json:"subscribed"`
	URL                   string   `json:"url"`
}

// Card : get card from the id
func (c *Client) Card(id string) (*Card, error) {
	_, body, err := c.get("card/" + id)
	if err != nil {
		return nil, err
	}
	var card Card
	err = json.Unmarshal(body, &card)
	if err != nil {
		return nil, err
	}
	card.client = c
	return &card, nil
}

// Members : get members on a card
func (c *Card) Members() ([]Member, error) {
	_, body, err := c.client.get("cards/" + c.ID + "/members")
	if err != nil {
		return nil, err
	}
	var members []Member
	err = json.Unmarshal(body, &members)
	if err != nil {
		return nil, err
	}
	for i := range members {
		members[i].client = c.client
	}
	return members, nil
}

// Actions : get actions/comments made on a card
func (c *Card) Actions() ([]Action, error) {
	_, body, err := c.client.get("cards/" + c.ID + "/actions")
	if err != nil {
		return nil, err
	}
	var actions []Action
	err = json.Unmarshal(body, &actions)
	if err != nil {
		return nil, err
	}
	for i := range actions {
		actions[i].client = c.client
	}
	return actions, nil
}

// TODO:
// checklist
// customFieldItems
// list
// attachment
