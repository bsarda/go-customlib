package trello

// documentation:
// https://trello.readme.io/reference#actions

// Action : struct for Action type that describes a Action in trello.
type Action struct {
	client *Client
	ID     string `json:"id"`
	Data   struct {
		DateLastEdited string `json:"dateLastEdited"`
		List           struct {
			ID   string `json:"id"`
			Name string `json:"name"`
		} `json:"list"`
		ListBefore struct {
			ID   string `json:"id"`
			Name string `json:"name"`
		} `json:"listBefore"`
		ListAfter struct {
			ID   string `json:"id"`
			Name string `json:"name"`
		} `json:"listAfter"`
		CheckItem struct {
			ID    string `json:"id"`
			State string `json:"state"`
			Name  string `json:"name"`
		} `json:"checkItem"`
		CheckList struct {
			ID   string `json:"id"`
			Name string `json:"name"`
		} `json:"checklist"`
		TextData struct {
			Emoji struct{} `json:"emoji"`
		} `json:"textData"`
		Board struct {
			ID        string `json:"id"`
			Name      string `json:"name"`
			ShortLink string `json:"shortLink"`
		} `json:"board"`
		Card struct {
			ID        string `json:"id"`
			Name      string `json:"name"`
			ShortLink string `json:"shortLink"`
			IDShort   int    `json:"idShort"`
		} `json:"card"`
		Text string `json:"text"`
	} `json:"data"`
	Date            string `json:"date"`
	IDMemberCreator string `json:"idMemberCreator"`
	Type            string `json:"type"`
	MemberCreator   struct {
		ID         string `json:"id"`
		AvatarHash string `json:"avatarHash"`
		FullName   string `json:"fullName"`
		Initials   string `json:"initials"`
		Username   string `json:"username"`
	} `json:"memberCreator"`
}
