package trello

import (
	"customlib"
	"errors"
	"regexp"
	"strings"
)

// Client : structure for creating a client
type Client struct {
	Version string
	Key     string
	Token   string
}

func (client *Client) get(uri string) (int, []byte, error) {
	if uri == "" || client.Key == "" || client.Token == "" {
		return 0, nil, errors.New("Invalid parameters")
	}
	if client.Version == "" {
		client.Version = "1"
	}
	urlBase := "https://api.trello.com/" + client.Version + "/"
	// create key/token
	keyTokenSuffix := ""
	if strings.Contains(uri, "?") {
		keyTokenSuffix = "&key=" + client.Key + "&token=" + client.Token
	} else {
		keyTokenSuffix = "?key=" + client.Key + "&token=" + client.Token
	}
	// check uri besides url
	var urlTrello string
	matched, _ := regexp.MatchString(`^https://api\.trello\.com/1/`, uri)
	if matched {
		urlTrello = uri + keyTokenSuffix
	} else {
		urlTrello = urlBase + uri + keyTokenSuffix
	}
	// fmt.Printf("\tURL => %v\n", urlTrello)
	code, response := customlib.SendHTTPRequest("GET", urlTrello, nil, nil, true)
	if code >= 200 && code < 300 {
		// fmt.Printf("\tresponse => %s\t", response)
		return code, response, nil
	}
	return code, response, errors.New("Error happened while sending the GET request")
}

// CreateClient : create the trello client
func CreateClient(key string, token string, version string) (*Client, error) {
	// check inputs
	if key == "" || token == "" {
		return nil, errors.New("Invalid inputs given")
	}
	// reformat
	ver := "1"
	if version != "" {
		ver = version
	}
	client := Client{
		Key:     key,
		Token:   token,
		Version: ver,
	}
	return &client, nil
}
