package trello

// documentation:
// https://trello.readme.io/reference#lists-1

import "encoding/json"

// List : struct for List type that describes a List in trello.
type List struct {
	client     *Client
	ID         string  `json:"id"`
	Name       string  `json:"name"`
	Closed     bool    `json:"closed"`
	IDBoard    string  `json:"idBoard"`
	Pos        float32 `json:"pos"`
	Subscribed bool    `json:"subscribed"`
}

// List : get the list details from list's ID
func (c *Client) List(id string) (*List, error) {
	_, body, err := c.get("lists/" + id)
	if err != nil {
		return nil, err
	}
	var list List
	err = json.Unmarshal(body, &list)
	if err != nil {
		return nil, err
	}
	list.client = c
	return &list, nil
}

// Cards : get the cards from a list
func (l *List) Cards() ([]Card, error) {
	_, body, err := l.client.get("lists/" + l.ID + "/cards")
	if err != nil {
		return nil, err
	}
	var cards []Card
	err = json.Unmarshal(body, &cards)
	if err != nil {
		return nil, err
	}
	for i := range cards {
		cards[i].client = l.client
	}
	return cards, nil
}

// Actions : get the actions/comments made on a list
func (l *List) Actions() ([]Action, error) {
	_, body, err := l.client.get("lists/" + l.ID + "/actions")
	if err != nil {
		return nil, err
	}
	var actions []Action
	err = json.Unmarshal(body, &actions)
	if err != nil {
		return nil, err
	}
	for i := range actions {
		actions[i].client = l.client
	}
	return actions, nil
}

// TODO :
// POST archiveAllCards
// POST moveAllCards
