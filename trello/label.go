package trello

// documentation:
// https://trello.readme.io/reference#labels

import "encoding/json"

// Label : struct for Label type that describes a Label in trello.
type Label struct {
	client  *Client
	ID      string `json:"id"`
	IDBoard string `json:"idBoard"`
	Name    string `json:"name"`
	Color   string `json:"color"`
}

// colors: yellow, purple, blue, red, green, orange, black, sky, pink,
// lime, null (null means no color, and the label will not show on the front of cards)

// Label : get the Label details from Label's ID
func (c *Client) Label(id string) (*Label, error) {
	_, body, err := c.get("labels/" + id)
	if err != nil {
		return nil, err
	}
	var label Label
	err = json.Unmarshal(body, &label)
	if err != nil {
		return nil, err
	}
	label.client = c
	return &label, nil
}
