package hashicorpvault

import (
	"customlib"
	"errors"
	"strconv"
	"strings"
)

// =============================================================================
// ======================== basic operations and strucs ========================

// Vault : vault kvstore configuration
type Vault struct {
	Address string
	Port    int
	Token   string
	Root    string
}

// =============================================================================
// ============================ creating the objets ============================

// CreateVault : creates the Vault kvstore with address/port and secret
func CreateVault(address string, port int, token string, root string) (*Vault, error) {
	// check inputs
	if address == "" || token == "" {
		return nil, errors.New("Invalid inputs given")
	}
	// reformat
	portLocal := 8200
	if port > 10 && port < 65535 {
		portLocal = port
	}
	vault := Vault{
		Address: address,
		Port:    portLocal,
		Token:   token,
		Root:    root,
	}
	return &vault, nil
}

// CreateVaultFromConfigfile : creates Vault kvstore from a config file
func CreateVaultFromConfigfile(filename string) (*Vault, error) {
	config, err := customlib.ReadConfig(filename)
	if err != nil {
		return nil, err
	}
	address := config["address"]
	port, _ := strconv.Atoi(config["port"])
	token := config["token"]
	root := config["root"]
	// add the / suffix if not exists
	if root != "" && !strings.HasSuffix(root, "/") {
		root = root + "/"
	}
	if len(address) == 0 || len(token) == 0 {
		return nil, errors.New("failed to get the parameters from config file")
	}
	return CreateVault(address, port, token, root)
}
