package hashicorpvault

import (
	"customlib"
	"encoding/json"
	"errors"
	"strconv"
	"strings"
)

// =============================================================================
// ========================= IDM: Operations on health =========================

// List : list the sub-keys, returns array of keys
func (vault *Vault) List(route string) ([]string, error) {
	responseBody, err := vault.GetAsJSONBytes(route + "?list=true")
	if err != nil {
		return nil, err
	}
	// parse the body as json
	var jsonBody map[string]interface{}
	json.Unmarshal(responseBody, &jsonBody)
	if jsonBody["data"] != nil {
		var keys []string
		keysList := jsonBody["data"].(map[string]interface{})["keys"].([]interface{})
		for _, k := range keysList {
			keys = append(keys, k.(string))
		}
		return keys, nil
	}
	return nil, errors.New("cannot find keys in the kvstore")
}

// ListAsJSON : list the sub-keys, returns the json raw
func (vault *Vault) ListAsJSON(route string) (string, error) {
	responseBody, err := vault.GetAsJSONBytes(route + "?list=true")
	if err != nil {
		return "", err
	}
	return string(responseBody), nil
}

// ListAsJSONBytes : list the sub-keys, returns the json as []byte
func (vault *Vault) ListAsJSONBytes(route string) ([]byte, error) {
	responseBody, err := vault.GetAsJSONBytes(route + "?list=true")
	if err != nil {
		return nil, err
	}
	return responseBody, nil
}

// Get : execute a get of a key, specified as arg. returns the data as map[string]interface
func (vault *Vault) Get(route string) (map[string]interface{}, error) {
	valueMap, err := vault.GetAsMap(route)
	if err != nil {
		return nil, err
	}
	valueExtracted := valueMap["data"].(map[string]interface{})
	return valueExtracted, nil
}

// GetValue : execute a get of a key, specified as arg. returns the value as interface
func (vault *Vault) GetValue(route string, value string) (interface{}, error) {
	if value == "" {
		value = "value"
	}
	valueMap, err := vault.GetAsMap(route)
	if err != nil {
		return nil, err
	}
	valueExtracted := valueMap["data"].(map[string]interface{})[value].(interface{})
	return valueExtracted, nil
}

// GetAsJSONBytes : execute a get of a key, specified as arg. returns the json []byte.
func (vault *Vault) GetAsJSONBytes(route string) ([]byte, error) {
	// set url
	uri := "https://" +
		vault.Address + ":" +
		strconv.Itoa(vault.Port) +
		secretAPI + vault.Root + route
	// create header
	headers := make(map[string]string)
	headers["Accept"] = customlib.APPJSONHeader
	headers["X-Vault-Token"] = vault.Token
	// send the request
	responseCode, responseBody := customlib.SendHTTPRequest(
		"GET",
		uri,
		nil, // no body
		headers,
		true)
	if responseCode < 200 || responseCode > 208 {
		// compare to return 404, error = [] which means not existing
		if responseCode == 404 && string(responseBody) == "{\"errors\":[]}\n" {
			return nil, errors.New("failed to send the get request on uri '" + uri + "', because it does not exists")
		}
		return nil, errors.New("failed to send the get request, responseCode=" +
			strconv.Itoa(responseCode) + "\nresponseBody=" +
			string(responseBody))
	}
	return responseBody, nil
}

// GetAsJSON : execute a get of a key, specified as arg. returns the json string.
func (vault *Vault) GetAsJSON(route string) (string, error) {
	responseBody, err := vault.GetAsJSONBytes(route)
	if err != nil {
		return "", err
	}
	return string(responseBody), nil
}

// GetAsMap : execute a get of a key, specified as arg. returns a map[string]interface
func (vault *Vault) GetAsMap(route string) (map[string]interface{}, error) {
	responseBody, err := vault.GetAsJSONBytes(route)
	if err != nil {
		return nil, err
	}
	// parse the body as json
	var jsonBody map[string]interface{}
	json.Unmarshal(responseBody, &jsonBody)
	return jsonBody, nil
}

// GetChildrensMap : gets the map of all the childrens from a node
func (vault *Vault) GetChildrensMap(route string) (map[string]interface{}, error) {
	var values = make(map[string]interface{})
	keys, err := vault.List(route)
	if err != nil {
		return nil, err
	}
	for _, key := range keys {
		if strings.HasSuffix(key, "/") {
			nestedValues, err := vault.GetChildrensMap(route + "/" + key)
			if err == nil {
				values[key[:len(key)-1]] = nestedValues
			}
		} else {
			val, err := vault.GetValue(route+"/"+key, "")
			if err == nil {
				values[key] = val
			}
		}
	}
	return values, nil
}

// SetValue : execute a post of a value in a key, specified as arg.
func (vault *Vault) SetValue(route string, value interface{}) error {
	// set url
	uri := "https://" +
		vault.Address + ":" +
		strconv.Itoa(vault.Port) +
		secretAPI + vault.Root + route
	// create header
	headers := make(map[string]string)
	headers["Accept"] = customlib.APPJSONHeader
	headers["X-Vault-Token"] = vault.Token
	// parse value
	var valueParsed string
	switch value.(type) {
	case int:
		valueParsed = `{"value":` + strconv.Itoa(value.(int)) + `}`
	case float64:
		valueParsed = `{"value":` + strconv.Itoa(int(value.(float64))) + `}`
	case bool:
		valueParsed = `{"value":` + strconv.FormatBool(value.(bool)) + `}`
	case string:
		var jsonValue interface{}
		errJSONMarshal := json.Unmarshal([]byte(value.(string)), &jsonValue)
		if errJSONMarshal != nil {
			// not a json value, will replace
			value = strings.Replace(value.(string), `"`, `\"`, -1)
			valueParsed = `{"value":"` + value.(string) + `"}`
		} else {
			switch jsonValue.(type) {
			case float64:
				valueParsed = `{"value":` + value.(string) + `}`
			default:
				valueParsed = value.(string)
			}
		}
	case []string, []interface{}:
		b, err := json.Marshal(value)
		if err != nil {
			return err
		}
		valueParsed = `{"value":` + string(b) + `}`
	}
	// send the request
	responseCode, responseBody := customlib.SendHTTPRequest(
		"POST",
		uri,
		[]byte(valueParsed),
		headers,
		true)
	if responseCode < 200 || responseCode > 208 {
		if responseCode == 404 && string(responseBody) == "{\"errors\":[]}\n" {
			return errors.New("failed to send the post request on uri '" + uri + "', because it does not exists")
		}
		return errors.New("failed to send the post request, responseCode=" +
			strconv.Itoa(responseCode) + "\nresponseBody=" +
			string(responseBody))
	}
	return nil
}

// Remove : removes the object given in the route
func (vault *Vault) Remove(route string) error {
	// set url
	uri := "https://" +
		vault.Address + ":" +
		strconv.Itoa(vault.Port) +
		secretAPI + vault.Root + route
	// create header
	headers := make(map[string]string)
	headers["Accept"] = customlib.APPJSONHeader
	headers["X-Vault-Token"] = vault.Token
	// because it returns a 2xx even if not existing, do a get first
	_, getError := vault.Get(route)
	if getError != nil {
		return getError
	}
	// send the request
	responseCode, responseBody := customlib.SendHTTPRequest(
		"DELETE",
		uri,
		nil,
		headers,
		true)
	if responseCode < 200 || responseCode > 208 {
		if responseCode == 404 && string(responseBody) == "{\"errors\":[]}\n" {
			return errors.New("failed to send the delete request on uri '" + uri + "', because it does not exists")
		}
		return errors.New("failed to send the delete request, responseCode=" +
			strconv.Itoa(responseCode) + "\nresponseBody=" +
			string(responseBody))
	}
	return nil
}

// RemoveFolder : removes the folder and sub-objects
func (vault *Vault) RemoveFolder(route string) error {
	// set url
	uri := "https://" +
		vault.Address + ":" +
		strconv.Itoa(vault.Port) +
		secretAPI + vault.Root + route
	// create header
	headers := make(map[string]string)
	headers["Accept"] = customlib.APPJSONHeader
	headers["X-Vault-Token"] = vault.Token
	// get the content
	listOfChildrens, err := vault.List(route)
	if err == nil {
		for _, child := range listOfChildrens {
			if strings.HasSuffix(child, "/") {
				if strings.HasSuffix(route, "/") {
					vault.RemoveFolder(route + child)
				} else {
					vault.RemoveFolder(route + "/" + child)
				}
			} else {
				// delete the keys that are not folder
				if !strings.HasSuffix(uri, "/") {
					uri = uri + "/"
				}
				// send the request
				responseCode, responseBody := customlib.SendHTTPRequest(
					"DELETE",
					uri+child,
					nil,
					headers,
					true)
				if responseCode < 200 || responseCode > 208 {
					if responseCode == 404 && string(responseBody) == "{\"errors\":[]}\n" {
						return errors.New("failed to send the delete request on uri '" + uri + "', because it does not exists")
					}
					return errors.New("failed to send the delete request, responseCode=" +
						strconv.Itoa(responseCode) + "\nresponseBody=" +
						string(responseBody))
				}
			}
		}
	}
	return nil
}
