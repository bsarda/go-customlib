package logger

import (
	"customlib/file"
	"errors"
	"log/syslog"
	"strconv"
	"strings"
)

// LogWriter default writer
var defaultLog *Logger
var defaultLogWriter *syslog.Writer

// =============================================================================
// ========================== create logger and funcs ==========================

// CreateLogger : creates a syslog logger with address/port/proto and tag
func CreateLogger(address string, port int, protocol string, tag string) (*Logger, error) {
	// check inputs
	if address == "" {
		return nil, errors.New("Invalid inputs given")
	}
	// reformat
	prt := 514
	if port > 10 && port < 65535 {
		prt = port
	}
	proto := "udp"
	if strings.ToLower(protocol) == "tcp" {
		proto = "tcp"
	}
	logger := Logger{
		Address:  address,
		Port:     prt,
		Protocol: proto,
		Tag:      tag,
	}
	return &logger, nil
}

// CreateLoggerFromConfigfile : creates a syslog logger
func CreateLoggerFromConfigfile(filename string) (*Logger, error) {
	config, err := file.ReadConfig(filename)
	if err != nil {
		return nil, err
	}
	address := config["address"]
	port, _ := strconv.Atoi(config["port"])
	protocol := config["protocol"]
	tag := config["tag"]
	if len(address) == 0 {
		return nil, errors.New("failed to get the parameters from config file")
	}
	return CreateLogger(address, port, protocol, tag)
}

// LoadToDefault : load to the default writer
func (l *Logger) LoadToDefault() {
	defaultLogWriter = l.Writer
	defaultLog = l
}

// Connect : connects the syslog and add writer to struct
func (l *Logger) Connect() error {
	addressAndPort := l.Address + ":" + strconv.Itoa(l.Port)
	logwriter, errSyslogDial := syslog.Dial(l.Protocol, addressAndPort, syslog.LOG_DEBUG, l.Tag)
	if errSyslogDial != nil {
		return errSyslogDial
	}
	l.Writer = logwriter
	logwriter.Debug("Started the log factory")
	return nil
}

// CreateEmbedFromConfigfile : load from config, connect and put to default
func CreateEmbedFromConfigfile(filename string) error {
	l, err := CreateLoggerFromConfigfile(filename)
	if err != nil {
		return err
	}
	errConnect := l.Connect()
	if errConnect != nil {
		return errConnect
	}
	l.LoadToDefault()
	return nil
}

// Close : closes the syslog writer
func (l *Logger) Close() {
	if l.Writer != nil {
		l.Writer.Close()
	}
}

// Close : closes the default syslog writer
func Close() {
	defaultLogWriter.Close()
}
