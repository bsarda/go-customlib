package logger

import (
	"log/syslog"
)

// Logger : Logger configuration
type Logger struct {
	Address  string
	Port     int
	Protocol string
	Tag      string
	Writer   *syslog.Writer
}
