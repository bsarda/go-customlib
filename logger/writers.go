package logger

import (
	"fmt"
	"runtime"
)

// Debug : debug message
func (l *Logger) Debug(in ...interface{}) {
	str := " [DEBUG] "
	// add the caller func name
	pc := make([]uintptr, 15)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	str += "[" + frame.Function + "] "
	// add each element
	for _, i := range in {
		str += fmt.Sprintf("%v", i)
	}
	l.Writer.Debug(str)
}

// Debug : debug message
func Debug(in ...interface{}) {
	defaultLog.Debug(in)
}

// Info : info message
func (l *Logger) Info(in ...interface{}) {
	str := " [INFO] "
	// add the caller func name
	pc := make([]uintptr, 15)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	str += "[" + frame.Function + "] "
	// add each element
	for _, i := range in {
		str += fmt.Sprintf("%v", i)
	}
	l.Writer.Info(str)
}

// Info : info message
func Info(in ...interface{}) {
	defaultLog.Info(in)
}

// Warn : Warning message
func (l *Logger) Warn(in ...interface{}) {
	str := " [WARNING] "
	// add the caller func name
	pc := make([]uintptr, 15)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	str += "[" + frame.Function + "] "
	// add each element
	for _, i := range in {
		str += fmt.Sprintf("%v", i)
	}
	l.Writer.Warning(str)
}

// Warn : Warning message
func Warn(in ...interface{}) {
	defaultLog.Warn(in)
}

// Error : Error message
func (l *Logger) Error(in ...interface{}) {
	str := " [ERROR] "
	// add the caller func name
	pc := make([]uintptr, 15)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	str += "[" + frame.Function + "] "
	// add each element
	for _, i := range in {
		str += fmt.Sprintf("%v", i)
	}
	l.Writer.Err(str)
}

// Error : Error message
func Error(in ...interface{}) {
	defaultLog.Error(in)
}

// Alert : Alert message
func (l *Logger) Alert(in ...interface{}) {
	str := " [ALERT] "
	// add the caller func name
	pc := make([]uintptr, 15)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	str += "[" + frame.Function + "] "
	// add each element
	for _, i := range in {
		str += fmt.Sprintf("%v", i)
	}
	l.Writer.Alert(str)
}

// Alert : Alert message
func Alert(in ...interface{}) {
	defaultLog.Alert(in)
}

// Critical : Critical message
func (l *Logger) Critical(in ...interface{}) {
	str := " [CRITICAL] "
	// add the caller func name
	pc := make([]uintptr, 15)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	str += "[" + frame.Function + "] "
	// add each element
	for _, i := range in {
		str += fmt.Sprintf("%v", i)
	}
	l.Writer.Crit(str)
}

// Critical : Critical message
func Critical(in ...interface{}) {
	defaultLog.Critical(in)
}
