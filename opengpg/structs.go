package opengpg

import (
	"time"

	"golang.org/x/crypto/openpgp"
)

// KeyPair is the custom override of openpgp key pair.
type KeyPair struct {
	PublicKey *openpgp.EntityList
	SecretKey *openpgp.EntityList
	Signer    *openpgp.Entity
}

// EncryptSettings is the encryption settings
type EncryptSettings struct {
	// SourceFile        string
	// TargetFile        string
	Suffix            string
	Recurse           bool
	OverwriteIfExists bool
	ContinueIfFails   bool
	FileName          string
	ModTime           time.Time
	Tag               string
}
