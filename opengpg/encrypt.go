package opengpg

import (
	"bytes"
	"customlib/file"
	"encoding/base64"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"

	"golang.org/x/crypto/openpgp"
)

// Encrypt is raw encoding from reader to writer
func (kp *KeyPair) Encrypt(reader *io.Reader, writer *io.Writer, settings *EncryptSettings) error {
	// create empty fileHints
	fileHints := &openpgp.FileHints{IsBinary: false}
	// complete if exists
	if !settings.ModTime.IsZero() {
		fileHints.IsBinary = true
		fileHints.FileName = ""
		fileHints.ModTime = settings.ModTime
	}
	if settings.FileName != "" {
		fileHints.IsBinary = true
		fileHints.FileName = settings.FileName
	}
	if settings.Tag != "" {
		fileHints.IsBinary = true
		fileHints.FileName = settings.FileName + "|" + settings.Tag
	}
	wc, err := openpgp.Encrypt(*writer, *kp.PublicKey, kp.Signer, fileHints, nil)
	if err != nil {
		return err
	}
	if _, err := io.Copy(wc, *reader); err != nil {
		return err
	}
	return wc.Close()
}

// EncryptString is used to encrypt a string.
// Reminder: use the public entitylist to encode
func (kp *KeyPair) EncryptString(secretString string) (string, error) {
	// create io.Writer buffer
	buf := new(bytes.Buffer)
	w, err := openpgp.Encrypt(buf, *kp.PublicKey, kp.Signer, nil, nil)

	defer w.Close()
	// if there was an error
	if err != nil {
		return "", err
	}
	// write to buffer the string to encrypt
	_, err = w.Write([]byte(secretString))
	if err != nil {
		return "", err
	}
	// close anyway
	err = w.Close()
	if err != nil {
		return "", err
	}
	// encode to base64
	bytes, err := ioutil.ReadAll(buf)
	if err != nil {
		return "", err
	}
	// output encrypted/encoded string
	return base64.StdEncoding.EncodeToString(bytes), nil
}

// EncryptFile is used to encrypt a file from given path to given path
func (kp *KeyPair) EncryptFile(source string, destination string, settings *EncryptSettings) (string, error) {
	if _, err := file.FileOrPathExists(source); err != nil {
		return "", err
	}
	if destination == "" {
		if settings.Suffix == "" {
			return "", fmt.Errorf("destination input is missing and no suffix set")
		}
		destination = source + settings.Suffix
	}
	if exists, _ := file.FileOrPathExists(destination); exists && !settings.OverwriteIfExists {
		return "", fmt.Errorf("destination file already exists and overwrite flag set to false")
	}

	_, sourceFileName := filepath.Split(source)
	settings.FileName = sourceFileName
	// set the filehint "tag"
	srcFileStats, err := os.Stat(source)
	if err != nil {
		return "", err
	}
	// modTime := srcFileStats.ModTime()
	settings.ModTime = srcFileStats.ModTime()
	// opening the file
	srcFile, err := os.Open(source)
	if err != nil {
		return "", err
	}
	defer srcFile.Close()
	// create destfile
	destFile, err := os.Create(destination)
	if err != nil {
		return "", err
	}
	defer destFile.Close()
	// convert
	var srcFileReader io.Reader = srcFile
	var destFileReader io.Writer = destFile
	// encore
	err = kp.Encrypt(&(srcFileReader), &(destFileReader), settings)
	if err != nil {
		return "", err
	}
	return destination, nil
}

// EncryptFileFromSettings is used to encrypt a file from given path to given path and given raw settings
func (kp *KeyPair) EncryptFileFromSettings(source string, destination string, suffix string, overwriteIfExists bool, tag string) (string, error) {
	settings := EncryptSettings{OverwriteIfExists: overwriteIfExists, Tag: tag}
	return kp.EncryptFile(source, destination, &(settings))
}

// EncryptFiles is used to encrypt multiple files specified as an array
func (kp *KeyPair) EncryptFiles(source []string, settings *EncryptSettings) ([]string, error) {
	var encryptedFiles []string
	for _, sourceFilePath := range source {
		if _, err := file.FileOrPathExists(sourceFilePath); err != nil {
			if !settings.ContinueIfFails {
				return encryptedFiles, err
			}
			continue
		}
		// err := EncryptFile(entityList, signer, sourceFilePath, sourceFilePath+suffix, overwriteIfExists, tag)
		dest, err := kp.EncryptFile(sourceFilePath, "", settings)
		if err != nil {
			if !settings.ContinueIfFails {
				return encryptedFiles, err
			}
		}
		encryptedFiles = append(encryptedFiles, dest)
	}
	return encryptedFiles, nil
}

// EncryptFilesFromSettings is used to encrypt multiple files specified as an array
func (kp *KeyPair) EncryptFilesFromSettings(source []string, suffix string, overwriteIfExists bool, continueIfFails bool, tag string) ([]string, error) {
	settings := EncryptSettings{OverwriteIfExists: overwriteIfExists, Tag: tag, Suffix: suffix, ContinueIfFails: continueIfFails}
	return kp.EncryptFiles(source, &(settings))
}

// EncryptFolder is used to encrypt multiple files within a folder
func (kp *KeyPair) EncryptFolder(folder string, settings *EncryptSettings) ([]string, error) {
	// test existence
	if exists, err := file.IsPathExists(folder); !exists {
		return nil, err
	}
	var encryptedFiles []string
	files, subfolders, err := file.ListFilesAndSubfolders(folder, false)
	if err != nil {
		return nil, err
	}
	// encrypt files in sub-folders
	if len(subfolders) > 0 && settings.Recurse {
		for _, subfolder := range subfolders {
			encryptedFilesDone, err := kp.EncryptFolder(subfolder, settings)
			if err != nil {
				if !settings.ContinueIfFails {
					return encryptedFiles, err
				}
			}
			encryptedFiles = append(encryptedFiles, encryptedFilesDone...)
		}
	}
	// encrypt local files
	encryptedFilesDone, err := kp.EncryptFiles(files, settings)
	if err != nil {
		if !settings.ContinueIfFails {
			return encryptedFiles, err
		}
	}
	encryptedFiles = append(encryptedFiles, encryptedFilesDone...)
	return encryptedFiles, nil
}

// EncryptFolderFromSettings is used to encrypt multiple files within a folder
func (kp *KeyPair) EncryptFolderFromSettings(folder string, suffix string, recurse bool, overwriteIfExists bool, continueIfFails bool, tag string) ([]string, error) {
	settings := EncryptSettings{OverwriteIfExists: overwriteIfExists, Tag: tag, Suffix: suffix, ContinueIfFails: continueIfFails, Recurse: recurse}
	return kp.EncryptFolder(folder, &(settings))
}

// EncryptFolderToNewFolder is used to encrypt multiple files within a folder, and move those encrypted files to a new folder
func (kp *KeyPair) EncryptFolderToNewFolder(folder string, target string, settings *EncryptSettings) ([]string, error) {
	// create the folder
	if target == "" {
		return nil, fmt.Errorf("no target folder specified")
	}
	err := os.MkdirAll(target, os.ModePerm)
	if err != nil {
		return nil, err
	}
	// encrypt
	files, err := kp.EncryptFolder(folder, settings)
	if err != nil {
		if !settings.ContinueIfFails {
			return nil, err
		}
	}
	// now, move
	for _, file := range files {
		// create new path as needed
		_, filename := path.Split(file)
		newpath := path.Dir(strings.Replace(file, folder, target, -1)) + "/" + filename
		err := os.MkdirAll(path.Dir(newpath), os.ModePerm)
		if err != nil {
			if !settings.ContinueIfFails {
				return nil, err
			}
		}
		// move the file
		err = os.Rename(file, newpath)
		if err != nil {
			if !settings.ContinueIfFails {
				return nil, err
			}
		}
	}
	return files, nil
}
