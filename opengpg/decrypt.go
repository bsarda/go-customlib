package opengpg

import (
	"bytes"
	"customlib/file"
	"encoding/base64"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"golang.org/x/crypto/openpgp"
)

// Decrypt is raw decrypt from reader to writer
func (kp *KeyPair) Decrypt(reader io.Reader, writer io.Writer) (*openpgp.FileHints, error) {
	// key is supposed to be decrypted
	if kp.SecretKey.DecryptionKeys()[0].PrivateKey.Encrypted {
		return nil, fmt.Errorf("secret key(s) is still encrypted")
	}
	// decrypt it with the contents of the private key
	md, err := openpgp.ReadMessage(reader, kp.SecretKey, nil, nil)
	if err != nil {
		return nil, err
	}
	// copy
	if _, err := io.Copy(writer, md.UnverifiedBody); err != nil {
		return nil, err
	}
	// if have filehints, return it
	if md.LiteralData.IsBinary {
		return &openpgp.FileHints{IsBinary: true, FileName: md.LiteralData.FileName, ModTime: time.Unix((int64)(md.LiteralData.Time), 0)}, nil
	}
	return nil, nil
}

// DecryptString is used to decode a string.
// Reminder: use the private entitylist to decode
func (kp *KeyPair) DecryptString(secretString string) (string, error) {
	// decode the base64 string
	dec, err := base64.StdEncoding.DecodeString(secretString)
	if err != nil {
		return "", err
	}
	// decrypt it with the contents of the private key
	md, err := openpgp.ReadMessage(bytes.NewBuffer(dec), kp.SecretKey, nil, nil)
	if err != nil {
		return "", err
	}
	bytes, err := ioutil.ReadAll(md.UnverifiedBody)
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}

// DecryptFile is used to decrypt a file from given path to given path.
// If path is not given, will reuse the filename recorded in the metadata (added at encrypt time)
func (kp *KeyPair) DecryptFile(source string, destination string) (string, error) {
	if _, err := file.FileOrPathExists(source); err != nil || source == "" {
		return "", err
	}
	destinationSetFlag := true
	if destination == "" {
		// not set, then export to a temp file before renaming from filehints
		destination = source + ".temporary"
		destinationSetFlag = false
	}
	srcFile, err := os.Open(source)
	if err != nil {
		return "", err
	}
	defer srcFile.Close()
	destFile, err := os.Create(destination)
	if err != nil {
		return "", err
	}
	defer destFile.Close()
	// get and set the mtime of the file - as the originally encrypted file
	fileHints, encErr := kp.Decrypt(srcFile, destFile)
	destFile.Close()
	// change the modification time if the metadata exists
	if fileHints.IsBinary && !fileHints.ModTime.IsZero() {
		err = os.Chtimes(destination, time.Now(), fileHints.ModTime)
		if err != nil {
			fmt.Println("error setting modification time to decrypted file: ", err)
		}
	}
	// if destination was empty, set the filename from metadata
	if !destinationSetFlag {
		if fileHints.FileName != "" {
			fn := strings.Split(fileHints.FileName, `|`)
			if len(fn) > 0 {
				srcFolder, _ := filepath.Split(source)
				// rename to that
				err = os.Rename(destination, srcFolder+"/"+fn[0])
				if err == nil {
					return srcFolder + "/" + fn[0], err
				}
			}
		}
		return "", fmt.Errorf("no destination given and no filename from metadata, aborting")
	}
	return destination, encErr
}

// DecryptFiles is used to decrypt multiple files from given path to given path.
// func DecryptFiles(entityList *openpgp.EntityList, passphrase string, signer *openpgp.Entity, source []string, suffixToRemove string, overwriteIfExists bool, continueIfFails bool, tag string) ([]string, error) {
func (kp *KeyPair) DecryptFiles(source []string, settings *EncryptSettings) ([]string, error) {
	var decryptedFiles []string
	for _, sourceFilePath := range source {
		if _, err := file.FileOrPathExists(sourceFilePath); err != nil {
			if !settings.ContinueIfFails {
				return decryptedFiles, err
			}
			continue
		}
		destinationFilePath := ""
		if settings != nil && settings.Suffix != "" {
			destinationFilePath = regexp.MustCompile(settings.Suffix+`$`).ReplaceAllString(sourceFilePath, "")
		}
		// decrypting the file
		fileDecrypted, err := kp.DecryptFile(sourceFilePath, destinationFilePath)
		if err != nil {
			fmt.Println(err)
			if !settings.ContinueIfFails {
				return decryptedFiles, err
			}
		}
		decryptedFiles = append(decryptedFiles, fileDecrypted)
	}
	return decryptedFiles, nil
}

// DecryptFilesFromSettings is used to decrypt multiple files from given path to given path.
func (kp *KeyPair) DecryptFilesFromSettings(source []string, suffixToRemove string, overwriteIfExists bool, continueIfFails bool) ([]string, error) {
	settings := EncryptSettings{OverwriteIfExists: overwriteIfExists, Suffix: suffixToRemove, ContinueIfFails: continueIfFails}
	return kp.DecryptFiles(source, &(settings))
}

// DecryptFolder decrypts all the content of a folder
func (kp *KeyPair) DecryptFolder(folder string, settings *EncryptSettings) ([]string, error) {
	var decryptedFiles []string
	// test existence
	if exists, err := file.IsPathExists(folder); !exists {
		return nil, err
	}
	files, subfolders, err := file.ListFilesAndSubfolders(folder, false)
	if err != nil {
		return nil, err
	}
	// decrypt files in sub-folders
	if len(subfolders) > 0 && settings.Recurse {
		for _, subfolder := range subfolders {

			decryptedFilesDone, err := kp.DecryptFolder(subfolder, settings)
			if err != nil {
				if !settings.ContinueIfFails {
					return decryptedFiles, err
				}
			}
			decryptedFiles = append(decryptedFiles, decryptedFilesDone...)
		}
	}
	// decrypt local folder files
	decryptedFilesDone, err := kp.DecryptFiles(files, settings)
	if err != nil {
		if !settings.ContinueIfFails {
			return decryptedFiles, err
		}
	}
	decryptedFiles = append(decryptedFiles, decryptedFilesDone...)
	return decryptedFiles, nil
}

// DecryptFolderFromSettings decrypts all the content of a folder
func (kp *KeyPair) DecryptFolderFromSettings(folder string, suffixToRemove string, recurse bool, overwriteIfExists bool, continueIfFails bool) ([]string, error) {
	settings := EncryptSettings{OverwriteIfExists: overwriteIfExists, Suffix: suffixToRemove, ContinueIfFails: continueIfFails, Recurse: recurse}
	return kp.DecryptFolder(folder, &(settings))
}

// DecryptFolderToNewFolder is used to decrypt multiple files within a folder, and move those decrypted files to a new folder
func (kp *KeyPair) DecryptFolderToNewFolder(folder string, target string, settings *EncryptSettings) ([]string, error) {
	// create the folder
	if target == "" {
		return nil, fmt.Errorf("no target folder specified")
	}
	err := os.MkdirAll(target, os.ModePerm)
	if err != nil {
		return nil, err
	}
	// decrypt
	files, err := kp.DecryptFolder(folder, settings)
	if err != nil {
		if !settings.ContinueIfFails {
			return nil, err
		}
	}
	// now, move
	for _, file := range files {
		// create new path as needed
		_, filename := path.Split(file)
		newpath := path.Dir(strings.Replace(file, folder, target, -1)) + "/" + filename
		err := os.MkdirAll(path.Dir(newpath), os.ModePerm)
		if err != nil {
			if !settings.ContinueIfFails {
				return nil, err
			}
		}
		// move the file
		err = os.Rename(file, newpath)
		if err != nil {
			if !settings.ContinueIfFails {
				return nil, err
			}
		}
	}
	return files, nil
}
