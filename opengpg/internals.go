package opengpg

import (
	"fmt"
	"os"

	"golang.org/x/crypto/openpgp"
)

// decryptSecretKey is used internally to decrypt the private/secret key with givent passphrase.
func (kp *KeyPair) decryptSecretKey(passphrase string) error {
	passphraseByte := []byte(passphrase)
	for i, entity := range *kp.SecretKey {
		if entity.PrivateKey != nil && entity.PrivateKey.Encrypted {
			err := entity.PrivateKey.Decrypt(passphraseByte)
			if err != nil {
				fmt.Println("failed to decrypt key #", i)
				return err
			}
		}
		for j, subkey := range entity.Subkeys {
			if subkey.PrivateKey != nil && subkey.PrivateKey.Encrypted {
				err := subkey.PrivateKey.Decrypt(passphraseByte)
				if err != nil {
					fmt.Println("failed to decrypt subkey #", j)
					return err
				}
			}
		}
	}
	return nil
}

// ReadKeyToEntityList reads a key from file to the entitylist type
// Remember first to export the GPG 2.1+ kbx to somewhat readable, example:
//   gpg -a --export mymail@free.fr > ../mymail.public-gpg.key
//   gpg -a --export-secret-keys mymail@free.fr > ../mymail.secret-gpg.key
func readKeyToEntityList(keypath string) (openpgp.EntityList, error) {
	// open public key file - os.Open returns an io.Reader
	keyringFileBuffer, _ := os.Open(keypath)
	defer keyringFileBuffer.Close()
	// read the key as an armored key
	entityList, err := openpgp.ReadArmoredKeyRing(keyringFileBuffer)
	if err != nil {
		fmt.Println("error reading keyring: ", err)
		return nil, err
	}
	return entityList, nil
}
