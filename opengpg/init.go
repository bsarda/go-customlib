package opengpg

import (
	"fmt"
)

// InitKeyPair is to be used to create a key pair. it decodes the private key
func InitKeyPair(public string, secret string, signer string, passphrase string) (*KeyPair, error) {

	publicEL, err := readKeyToEntityList(public)
	if err != nil {
		fmt.Println("failed to decrypt public key")
		return nil, err
	}
	secretEL, err := readKeyToEntityList(secret)
	if err != nil {
		fmt.Println("failed to decrypt secret key")
		return nil, err
	}
	// signerEL, err := readKeyToEntityList(signer)
	// if err != nil {
	// 	fmt.Println("failed to decrypt signer key")
	// 	return nil, err
	// }
	// create struct
	var kp = KeyPair{PublicKey: &publicEL, SecretKey: &secretEL, Signer: nil}
	// decrypt secret key with passphrase
	err = kp.decryptSecretKey(passphrase)
	if err != nil {
		fmt.Println("failed to decrypt key")
		return nil, err
	}
	return &kp, nil
}
